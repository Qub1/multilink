﻿using System;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;

namespace MultiLink.Menu {
	public class ProgressIndicator : TextureGameObject {
		/// <summary>
		/// Creates a new ProgressIndicator.
		/// </summary>
		/// <param name="position">The position of the indicator.</param>
		public ProgressIndicator(Vector2 position) : base(position, new Vector2(55, 55)) {
			Sprite.Texture = GameHandler.AssetHandler.GetTexture("6x1@progress");
			Sprite.Animated = true;
			Sprite.AnimationLength = 6;
			Sprite.AnimationFrameTime = TimeSpan.FromMilliseconds(130);
			Gui = true;
		}
	}
}