﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLink.States;

namespace MultiLink.Menu {
	public sealed class JoinMenu : MenuState {
		private readonly SoundEffect select;
		private readonly Selector selector = new Selector();
		private readonly TextBoxGameObject sessionBox;

		/// <summary>
		/// Creates a new JoinMenu.
		/// </summary>
		public JoinMenu() {
			select = GameHandler.AssetHandler.GetSoundEffect(@"Sounds/LA_Menu_Select");

			// Setup background
			Vector2 resolution = new Vector2(GameHandler.GraphicsHandler.BaseResolution.X, GameHandler.GraphicsHandler.BaseResolution.Y);
			TextureGameObject background = new TextureGameObject(Vector2.Zero, resolution) {
				Sprite = {
					Texture = GameHandler.AssetHandler.GetTexture("background_menu")
				},
				Layer = 0f,
				Gui = true
			};

			// Setup menu items
			TextGameObject codeLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "ENTER SESSION IP:") {
				Layer = .5f
			};
			codeLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - codeLabel.Size.X) / 2F, 260);
			TextGameObject cancelLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "CANCEL") {
				Layer = .5f
			};
			cancelLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - cancelLabel.Size.X) / 4F, 610);
			TextGameObject acceptLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "ACCEPT") {
				Layer = .5f
			};
			acceptLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - acceptLabel.Size.X) / 4F * 3F, 610);

			selector.Layer = .5f;

			// Setup session TextBox
			sessionBox = new TextBoxGameObject(new Vector2(resolution.X / 3, resolution.Y / 2), new Vector2(480, 44), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28")) {
				Sprite = {
					Texture = GameHandler.AssetHandler.GetTexture("background_inputbox")
				},
				Text = {
					Color = Color.White
				},
				Layer = .5f
			};

			// Add to this GameObject as children
			AddChild(background);
			AddChild(codeLabel);
			AddChild(cancelLabel);
			AddChild(acceptLabel);
			AddChild(selector);
			AddChild(sessionBox);

			// Add text items to list
			selector.AddItem(sessionBox);
			selector.AddItem(cancelLabel);
			selector.AddItem(acceptLabel);
		}

		public override void Update() {
			// Move selection up and down
			if (GameHandler.InputHandler.OnKeyDown(Keys.Up) || GameHandler.InputHandler.OnKeyDown(Keys.Left)) {
				if (selector.CurrentIndex > 0) {
					selector.CurrentIndex -= 1;
				} else {
					selector.CurrentIndex = selector.Items.Count - 1;
				}
			}

			// Move selection left and right
			if (GameHandler.InputHandler.OnKeyDown(Keys.Down) || GameHandler.InputHandler.OnKeyDown(Keys.Right)) {
				if (selector.CurrentIndex < selector.Items.Count - 1) {
					selector.CurrentIndex += 1;
				} else {
					selector.CurrentIndex = 0;
				}
			}

			// Confirm selection
			if (GameHandler.InputHandler.OnKeyDown(Keys.Enter) || GameHandler.InputHandler.OnKeyDown(Keys.Space)) {
				if (selector.CurrentIndex == 1) {
					// Cancel
					ReturnToMainMenu();
					@select.Play();
				} else if (selector.CurrentIndex == 2) {
					// Accept
					JoinGame();
					@select.Play();
				}
			}

			// Handle mouse input
			Point mousePosition = new Point((int)GameHandler.InputHandler.MousePosition.X, (int)GameHandler.InputHandler.MousePosition.Y);
			if (selector.Items[0].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 0;
			} else if (selector.Items[1].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 1;
				if (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left)) {
					ReturnToMainMenu();
					@select.Play();
				}
			} else if (selector.Items[2].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 2;
				if (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left)) {
					JoinGame();
					@select.Play();
				}
			}

			sessionBox.HasFocus = selector.CurrentIndex == 0;

			base.Update();
		}

		private void JoinGame() {
			MultiLink.MultiPlayer.Join(sessionBox.Text.Text);
			((WaitingMenu)GameHandler.StateHandler.States["Waiting"]).Hosting = false;
			GameHandler.StateHandler.States["Waiting"].Reset();
			GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["Waiting"];
		}

		private void ReturnToMainMenu() {
			GameHandler.StateHandler.States["Menu"].Reset();
			GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["Menu"];
		}
	}
}