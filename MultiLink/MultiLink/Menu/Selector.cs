﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;

namespace MultiLink.Menu {
	public class Selector : TextureGameObject {
		/// <summary>
		/// The index of the item that is currently active.
		/// </summary>
		public int CurrentIndex;

		private readonly List<GameObject> items = new List<GameObject>();

		/// <summary>
		/// The current active item.
		/// </summary>
		public GameObject CurrentItem => Items[CurrentIndex];

		/// <summary>
		/// The available items.
		/// </summary>
		public List<GameObject> Items => new List<GameObject>(items);

		/// <summary>
		/// Creates a new Selector.
		/// </summary>
		public Selector() : base(Vector2.Zero, new Vector2(55, 70)) {
			Sprite.Texture = GameHandler.AssetHandler.GetTexture("2x1@Selector");
			Sprite.Animated = true;
			Sprite.AnimationFrameTime = TimeSpan.FromMilliseconds(150);
			Sprite.AnimationLength = 2;
			Gui = true;
		}

		public override void Reset() {
			CurrentIndex = 0;

			// Set selector to first position to prevent selector 'jumping'
			Position = new Vector2(Items[0].Position.X - Size.X - 50, Items[0].Position.Y - (Size.Y - Items[0].Size.Y) / 2);

			base.Reset();
		}

		public override void Update() {
			// Move selector to selected item
			Position = new Vector2(CurrentItem.Position.X - Size.X - 50, CurrentItem.Position.Y - (Size.Y - Items[0].Size.Y) / 2);
			base.Update();
		}

		/// <summary>
		/// Adds an item to the selector.
		/// </summary>
		/// <param name="item"></param>
		public void AddItem(GameObject item) {
			items.Add(item);
		}

		/// <summary>
		/// Removes an item from the selector.
		/// </summary>
		/// <param name="item"></param>
		public void RemoveItem(GameObject item) {
			items.Remove(item);
		}
	}
}