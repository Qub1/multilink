using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLink.States;

namespace MultiLink.Menu {
	public sealed class WaitingMenu : MenuState {
		public bool Hosting = true;

		/// <summary>
		/// The seed to use.
		/// </summary>
		public int Seed;

		/// <summary>
		/// The session ID that is displayed.
		/// </summary>
		public string PublicIp = "";

		public string LocalIp = "";

		/// <summary>
		/// Whether the tutorial rooms must be added to generation
		/// </summary>
		public bool TutorialEnabled;

		private readonly TextGameObject acceptLabel;
		private readonly SoundEffect @select;
		private readonly Selector selector = new Selector();
		private readonly TextGameObject publicIpLabel;
		private readonly TextGameObject localIpLabel;
		private readonly TextGameObject statusLabel;
		private Thread t;

		/// <summary>
		/// Creates a new LoadingScreen.
		/// </summary>
		public WaitingMenu() {
			@select = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Menu_Select");

			// Setup background
			Vector2 resolution = new Vector2(GameHandler.GraphicsHandler.BaseResolution.X, GameHandler.GraphicsHandler.BaseResolution.Y);
			TextureGameObject background = new TextureGameObject(Vector2.Zero, resolution) {
				Sprite = {
					Texture = GameHandler.AssetHandler.GetTexture("background_menu")
				},
				Layer = 0f,
				Gui = true
			};

			// Setup text items
			publicIpLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28")) {
				Layer = .5f
			};
			localIpLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28")) {
				Layer = .5f
			};
			statusLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28")) {
				Layer = .5f
			};
			var cancelLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "CANCEL") {
				Layer = .5f
			};
			cancelLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - cancelLabel.Size.X) / 4F, 610);
			acceptLabel = new TextGameObject(new Vector2(GameHandler.GraphicsHandler.BaseResolution.X * 3F / 4F, 610), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "ACCEPT") {
				Layer = .5f
			};
			acceptLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - acceptLabel.Size.X) / 4F * 3F, 610);

			selector.Layer = .5f;

			// Setup progress animation
			ProgressIndicator progress = new ProgressIndicator(new Vector2(resolution.X / 2, resolution.Y / 2)) {
				Layer = .5f
			};

			TextBoxGameObject sessionBox = new TextBoxGameObject(new Vector2(resolution.X / 2, resolution.Y / 2), new Vector2(480, 44), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "", true, false) {
				Sprite = {
					Texture = GameHandler.AssetHandler.GetTexture("background_inputbox")
				},
				Text = {
					Color = Color.White
				}
			};

			// Add to this GameObject as children
			AddChild(background);
			AddChild(statusLabel);
			AddChild(publicIpLabel);
			AddChild(localIpLabel);
			AddChild(progress);
			AddChild(cancelLabel);
			AddChild(acceptLabel);
			AddChild(selector);

			//Temp
			AddChild(sessionBox);

			// Add text items to list
			selector.AddItem(cancelLabel);
			selector.AddItem(acceptLabel);
		}

		public override void Reset() {
			if (t != null) {
				t.Abort();
			}
			PublicIp = "";
			Seed = GameHandler.InfoHandler.Random.Next();

			publicIpLabel.Visible = true;
			localIpLabel.Visible = true;
			acceptLabel.Visible = false;

			publicIpLabel.Text = "PUBLIC IP: -";
			localIpLabel.Text = "LOCAL IP: -";

			// Start connection thread
			t = new Thread(Connect) {
				IsBackground = true
			};
			t.Start();
		}

		public override void Update() {
			if (GameHandler.InputHandler.OnKeyDown(Keys.Enter)) {
				if (selector.CurrentIndex == 0) {
					CancelHosting();
				} else if (acceptLabel.Visible) {
					StartGame();
				}
			}

			// Move selection left and right
			if (GameHandler.InputHandler.OnKeyDown(Keys.Down) || GameHandler.InputHandler.OnKeyDown(Keys.Right)) {
				if (selector.CurrentIndex < selector.Items.Count - 1) {
					selector.CurrentIndex += 1;
				} else {
					selector.CurrentIndex = 0;
				}
			}

			// Handle mouse input
			Point mousePosition = new Point((int)GameHandler.InputHandler.MousePosition.X, (int)GameHandler.InputHandler.MousePosition.Y);
			if (selector.Items[0].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 0;
				if (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left)) {
					CancelHosting();
					@select.Play();
				}
			} else if (selector.Items[1].BoundingBox.Contains(mousePosition) && acceptLabel.Visible) {
				selector.CurrentIndex = 1;
				if (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left)) {
					StartGame();
				}
			}

			if (PublicIp != "") {
				publicIpLabel.Text = "PUBLIC IP: " + PublicIp;
			}
			if (LocalIp != "") {
				localIpLabel.Text = "LOCAL IP: " + LocalIp;
			}

			publicIpLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - publicIpLabel.Size.X) / 2F, 450);
			localIpLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - localIpLabel.Size.X) / 2F, 500);
			statusLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - statusLabel.Size.X) / 2F, 260);

			base.Update();
		}

		private void CancelHosting() {
			// Stop current game and return to main menu
			GameHandler.StateHandler.States["Menu"].Reset();
			GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["Menu"];
		}

		private void Connect() {
			if (Hosting) {
				statusLabel.Text = "REQUESTING LOCAL IP...";
				LocalIp = GameHandler.NetworkHandler.GetLocalIpAddress().ToString();

				statusLabel.Text = "REQUESTING PUBLIC IP...";
				PublicIp = GameHandler.NetworkHandler.GetExternalIpAddress().ToString();

				statusLabel.Text = "STARTING INTERNAL SERVER...";
				MultiLink.MultiPlayer.Host();

				statusLabel.Text = "WAITING FOR PLAYER 2 TO JOIN...";
				while (MultiLink.MultiPlayer.Server != null && MultiLink.MultiPlayer.Server.Clients.Count < 2) {
				}

				acceptLabel.Visible = true;
				statusLabel.Text = "READY!";
			} else {
				publicIpLabel.Visible = false;
				localIpLabel.Visible = false;
				statusLabel.Text = "WAITING FOR HOST...";

				while (((PlayingState)GameHandler.StateHandler.States["Play"]).World == null) {
				}

				statusLabel.Text = "LOADING WORLD...";
			}

			while (!MultiLink.MultiPlayer.SelfReady) {
			}

			if (Hosting) {
				statusLabel.Text = "WAITING FOR PLAYER 2...";
			} else {
				statusLabel.Text = "WAITING FOR HOST...";
			}
			while (!MultiLink.MultiPlayer.OtherReady) {
			}

			GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["Play"];
		}

		private void StartGame() {
			if (Hosting && statusLabel.Text == "READY!") {
				acceptLabel.Visible = false;

				statusLabel.Text = "LOADING WORLD...";
				MultiLink.MultiPlayer.SendStart(Seed, TutorialEnabled);
			}
		}
	}
}