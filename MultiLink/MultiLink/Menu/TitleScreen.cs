﻿using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLink.States;

namespace MultiLink.Menu {
	public sealed class TitleScreen : MenuState {
		private readonly TextureGameObject background;

		/// <summary>
		/// Creates a new TitleScreen.
		/// </summary>
		public TitleScreen() {
			// Setup text objects
			TextGameObject continueLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"), "Press any key to start") {
				Color = Color.White,
				Layer = .5f
			};
			continueLabel.Gui = true;
			continueLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - continueLabel.Size.X) / 2F, 560);
			TextGameObject copyrightLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "(c) GameLink Studios 2015") {
				Layer = .5f
			};
			copyrightLabel.Gui = true;
			copyrightLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - copyrightLabel.Size.X) / 2F, 680);

			// Setup texture objects
			Vector2 resolution = new Vector2(GameHandler.GraphicsHandler.BaseResolution.X, GameHandler.GraphicsHandler.BaseResolution.Y);
			background = new TextureGameObject(Vector2.Zero, resolution) {
				Sprite = {
					Texture = GameHandler.AssetHandler.GetTexture("background_titlescreen")
				},
				Layer = 0f
			};
			background.Gui = true;

			// Add to this GameObject as children
			AddChild(background);
			AddChild(continueLabel);
			AddChild(copyrightLabel);

			GameHandler.AudioHandler.PlaySong(GameHandler.AssetHandler.GetSong("menumusic"), true);
		}

		public override void Update() {
			GameHandler.GraphicsHandler.Camera.CenterTarget();

			base.Update();
			if (GameHandler.InputHandler.OnAKeyDown() || (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left) && background.BoundingBox.Contains(new Point((int)GameHandler.InputHandler.MousePosition.X, (int)GameHandler.InputHandler.MousePosition.Y)))) {
				// If any key is pressed or the mouse is clicked and inside the game window, go to main menu
				GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["Menu"];
			}
		}
	}
}