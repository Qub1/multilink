﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLink.States;

namespace MultiLink.Menu {
	public sealed class HostLocalMenu : MenuState {
		public bool TutorialEnabled;
		private readonly TextBoxGameObject seedBox;
		private readonly SoundEffect select;
		private readonly Selector selector = new Selector();

		/// <summary>
		/// Creates a new HostMenu.
		/// </summary>
		public HostLocalMenu() {
			select = GameHandler.AssetHandler.GetSoundEffect(@"Sounds/LA_Menu_Select");

			// Setup background
			Vector2 resolution = new Vector2(GameHandler.GraphicsHandler.BaseResolution.X, GameHandler.GraphicsHandler.BaseResolution.Y);
			TextureGameObject background = new TextureGameObject(Vector2.Zero, resolution) {
				Sprite = {
					Texture = GameHandler.AssetHandler.GetTexture("background_menu")
				},
				Layer = 0f
			};
			background.Gui = true;

			// Setup menu items
			TextGameObject seedLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "ENTER SEED: ") {
				Layer = .5f
			};
			seedLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - seedLabel.Size.X) / 2F, 260);
			TextGameObject cancelLabel = new TextGameObject(new Vector2(GameHandler.GraphicsHandler.BaseResolution.X / 4F, 610), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "CANCEL") {
				Layer = .5f
			};
			cancelLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - cancelLabel.Size.X) / 4F, 610);
			TextGameObject acceptLabel = new TextGameObject(new Vector2(GameHandler.GraphicsHandler.BaseResolution.X * 3F / 4F, 610), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "ACCEPT") {
				Layer = .5f
			};
			acceptLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - acceptLabel.Size.X) / 4F * 3F, 610);

			selector.Layer = .5f;

			// Setup seed TextBox
			seedBox = new TextBoxGameObject(Vector2.Zero, new Vector2(480, 44), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "", true, false) {
				MaxLength = 9,
				Sprite = {
					Texture = GameHandler.AssetHandler.GetTexture("background_inputbox")
				},
				Text = {
					Color = Color.White
				},
				Layer = .5f
			};
			seedBox.Position = (resolution - seedBox.Size) / 2;

			// Add to this GameObject as children
			AddChild(background);
			AddChild(seedLabel);
			AddChild(cancelLabel);
			AddChild(acceptLabel);
			AddChild(selector);
			AddChild(seedBox);

			// Add text items to list
			selector.AddItem(seedBox);
			selector.AddItem(cancelLabel);
			selector.AddItem(acceptLabel);

			Reset();
		}

		public override void Reset() {
			seedBox.Text.Text = GameHandler.InfoHandler.Random.Next(999999999).ToString();

			base.Reset();
		}

		public override void Update() {
			// Move selection up and down
			if (GameHandler.InputHandler.OnKeyDown(Keys.Up) || GameHandler.InputHandler.OnKeyDown(Keys.Left)) {
				if (selector.CurrentIndex > 0) {
					selector.CurrentIndex -= 1;
				} else {
					selector.CurrentIndex = selector.Items.Count - 1;
				}
			}

			// Move selection left and right
			if (GameHandler.InputHandler.OnKeyDown(Keys.Down) || GameHandler.InputHandler.OnKeyDown(Keys.Right)) {
				if (selector.CurrentIndex < selector.Items.Count - 1) {
					selector.CurrentIndex += 1;
				} else {
					selector.CurrentIndex = 0;
				}
			}

			// Confirm selection
			if (GameHandler.InputHandler.OnKeyDown(Keys.Enter) || GameHandler.InputHandler.OnKeyDown(Keys.Space)) {
				if (selector.CurrentIndex == 1) {
					//Cancel
					ReturnToMainMenu();
					@select.Play();
				} else if (selector.CurrentIndex == 2) {
					//Accept
					JoinGame();
					@select.Play();
				}
			}

			// Handle mouse input
			Point mousePosition = new Point((int)GameHandler.InputHandler.MousePosition.X, (int)GameHandler.InputHandler.MousePosition.Y);
			if (selector.Items[0].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 0;
			} else if (selector.Items[1].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 1;
				if (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left)) {
					ReturnToMainMenu();
					@select.Play();
				}
			} else if (selector.Items[2].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 2;
				if (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left)) {
					JoinGame();
					@select.Play();
				}
			}

			seedBox.HasFocus = selector.CurrentIndex == 0;

			base.Update();
		}

		private void JoinGame() {
			MultiLink.MultiPlayer.Start(int.Parse(seedBox.Text.Text), TutorialEnabled);
			GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["Play"];
		}

		private void ReturnToMainMenu() {
			GameHandler.StateHandler.States["Menu"].Reset();
			GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["Menu"];
		}
	}
}