﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLink.States;

namespace MultiLink.Menu {
	public sealed class MainMenu : MenuState {
		private readonly SoundEffect @select;
		private readonly Selector selector = new Selector();
		private bool tutorialEnabled = true;
		private readonly TextGameObject tutorialLabel;
		private readonly TextGameObject tutorialSwitch;

		/// <summary>
		/// Creates a new MainMenu.
		/// </summary>
		public MainMenu() {
			selector.Gui = true;
			@select = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Menu_Select");

			// Setup background
			Vector2 resolution = new Vector2(GameHandler.GraphicsHandler.BaseResolution.X, GameHandler.GraphicsHandler.BaseResolution.Y);
			TextureGameObject background = new TextureGameObject(Vector2.Zero, resolution) {
				Sprite = {
					Texture = GameHandler.AssetHandler.GetTexture("background_menu")
				},
				Layer = 0f
			};
			background.Gui = true;

			// Setup menu items
			TextGameObject hostNewLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "HOST ONLINE GAME") {
				Layer = .5f
			};
			hostNewLabel.Gui = true;
			hostNewLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - hostNewLabel.Size.X) / 2F, 260);
			TextGameObject hostSavedLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "HOST LOCAL GAME") {
				Layer = .5f
			};
			hostSavedLabel.Gui = true;
			hostSavedLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - hostSavedLabel.Size.X) / 2F, 360);
			TextGameObject joinLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "JOIN ONLINE GAME") {
				Layer = .5f
			};
			joinLabel.Gui = true;
			joinLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - joinLabel.Size.X) / 2F, 460);

			/*TextGameObject optionsLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "OPTIONS") {
				Layer = .5f
			};
			optionsLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - optionsLabel.Size.X) / 4F * 3F, 630);*/

			tutorialLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "TUTORIAL: ENABLED") {
				Layer = .5f
			};
			tutorialLabel.Gui = true;
			tutorialLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - tutorialLabel.Size.X) / 4F * 1F, 610);

			tutorialSwitch = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "DISABLE") {
				Layer = .5f
			};
			tutorialSwitch.Gui = true;
			tutorialSwitch.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - tutorialSwitch.Size.X) / 4F * 3F, 610);

			selector.Layer = .5f;

			// Add to this GameObject as children
			AddChild(background);
			AddChild(hostNewLabel);
			AddChild(hostSavedLabel);
			AddChild(joinLabel);
			AddChild(tutorialLabel);
			AddChild(tutorialSwitch);
			AddChild(selector);

			// Add text items to list
			selector.AddItem(hostNewLabel);
			selector.AddItem(hostSavedLabel);
			selector.AddItem(joinLabel);
			selector.AddItem(tutorialSwitch);
		}

		//private void GoToOptionsMenu() {
		//	GameHandler.StateHandler.States["Options"].Reset();
		//	GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["Options"];
		//}
		public override void Reset() {
			((PlayingState)GameHandler.StateHandler.States["Play"]).Stop();

			base.Reset();
		}

		public override void Update() {
			GameHandler.GraphicsHandler.Camera.CenterTarget();

			// Move selector up / down and left / right
			if (GameHandler.InputHandler.OnKeyDown(Keys.Up) || GameHandler.InputHandler.OnKeyDown(Keys.Left)) {
				if (selector.CurrentIndex > 0) {
					selector.CurrentIndex -= 1;
				} else {
					selector.CurrentIndex = selector.Items.Count - 1;
				}
			}
			if (GameHandler.InputHandler.OnKeyDown(Keys.Down) || GameHandler.InputHandler.OnKeyDown(Keys.Right)) {
				if (selector.CurrentIndex < selector.Items.Count - 1) {
					selector.CurrentIndex += 1;
				} else {
					selector.CurrentIndex = 0;
				}
			}

			// Switch to another state is Enter or Space is pressed
			if (GameHandler.InputHandler.OnKeyDown(Keys.Enter) || GameHandler.InputHandler.OnKeyDown(Keys.Space)) {
				if (selector.CurrentIndex == 0) // Host new game
				{
					HostOnlineGame();
				} else if (selector.CurrentIndex == 1) // Host saved game
				{
					HostLocalGame();
				} else if (selector.CurrentIndex == 2) // Join game
				{
					JoinGame();
				} else if (selector.CurrentIndex == 3) // Options
				{
					SwitchTutorial();
				}
				@select.Play();
			}

			// Handle mouse input
			Point mousePosition = new Point((int)GameHandler.InputHandler.MousePosition.X, (int)GameHandler.InputHandler.MousePosition.Y);
			if (selector.Items[0].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 0;
				if (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left)) {
					HostOnlineGame();
					@select.Play();
				}
			} else if (selector.Items[1].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 1;
				if (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left)) {
					HostLocalGame();
					@select.Play();
				}
			} else if (selector.Items[2].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 2;
				if (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left)) {
					JoinGame();
					@select.Play();
				}
			} else if (selector.Items[3].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 3;
				if (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left)) {
					SwitchTutorial();
					@select.Play();
				}
			}

			base.Update();
		}

		private void HostLocalGame() {
			GameHandler.StateHandler.States["HostLocal"].Reset();
			((HostLocalMenu)GameHandler.StateHandler.States["HostLocal"]).TutorialEnabled = tutorialEnabled;
			GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["HostLocal"];
		}

		private void HostOnlineGame() {
			GameHandler.StateHandler.States["HostOnline"].Reset();
			((HostOnlineMenu)GameHandler.StateHandler.States["HostOnline"]).TutorialEnabled = tutorialEnabled;
			GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["HostOnline"];
		}

		private void JoinGame() {
			GameHandler.StateHandler.States["Join"].Reset();
			GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["Join"];
		}

		private void SwitchTutorial() {
			if (!tutorialEnabled) {
				tutorialEnabled = true;
				tutorialLabel.Text = "TUTORIAL: ENABLED";
				tutorialLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - tutorialLabel.Size.X) / 4F * 1F, 610);
				tutorialSwitch.Text = "DISABLE";
				tutorialSwitch.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - tutorialSwitch.Size.X) / 4F * 3F, 610);
			} else {
				tutorialEnabled = false;
				tutorialLabel.Text = "TUTORIAL: DISABLED";
				tutorialLabel.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - tutorialLabel.Size.X) / 4F * 1F, 610);
				tutorialSwitch.Text = "ENABLE";
				tutorialSwitch.Position = new Vector2((GameHandler.GraphicsHandler.BaseResolution.X - tutorialSwitch.Size.X) / 4F * 3F, 610);
			}
		}
	}
}