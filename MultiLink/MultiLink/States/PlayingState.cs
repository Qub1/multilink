﻿using MultiLibrary.Objects;
using MultiLibrary.Types;
using MultiLink.Gui;
using MultiLink.Objects;
using MultiLink.Worlds;

namespace MultiLink.States {
	public class PlayingState : State {
		public WorldObject World;
		public Hud hud;

		public bool LocalMultiplayer {
			get {
				return !MultiLink.MultiPlayer.IsOnline;
			}
		}

		public override void Update() {
			base.Update();

			if (World != null && (World as World).GameWon) {
				(hud as Hud).StopGame = true;
			}
		}

		/// <summary>
		/// Resets the playing state.
		/// </summary>
		/// <param name="seed">The seed to use.</param>
		/// <param name="tutorial">Whether to display the tutorial.</param>
		public void Reset(int seed, bool tutorial) {
			if (World != null) {
				RemoveChild(World);
			}
			if (hud != null) {
				RemoveChild(hud);
			}

			World = new World(seed, tutorial);
			AddChild(World);

			hud = new Hud();
			((Hud)hud).Player = World.Player;
			((Hud)hud).Player2 = World.Player2;
			AddChild(hud);

			base.Reset();
		}

		public void Stop() {
			if (World != null) {
				RemoveChild(World);
			}
			if (hud != null) {
				RemoveChild(hud);
			}
		}
	}
}