﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Rooms;

namespace MultiLink.Objects.Tiles {
	public class GrassTile : Tile {
		private bool cutDown;
		private bool cutDownTimer;
		public override Rectangle BoundingBox => new Rectangle((int)Position.X, (int)Position.Y, (int)UnitX, (int)UnitY);

		public GrassTile(Vector2 position) : base(position, new Sprite(GameHandler.AssetHandler.GetTexture("grass")), true) {
			Sprite.CurrentIndex = Point.Zero;
		}

		public override void Update() {
			if (!cutDown) {
				Dictionary<Rectangle, DamagePair> damageDictionary = ((Room)Parent).DamageList;
				foreach (KeyValuePair<Rectangle, DamagePair> p in damageDictionary.Where(p => BoundingBox.Intersects(p.Key))) {
					cutDown = true;
					Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("floor")) {
						CurrentIndex = Point.Zero
					};
					isSolid = false;
				}
			}

			base.Update();
		}
	}
}