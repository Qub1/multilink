using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Rooms;

namespace MultiLink.Objects.Tiles {
	public class KeyLockedDoor : Tile {
		public bool locked = true;
		private readonly SoundEffect Open;
		private Point startIndex;

		private bool Locked {
			get {
				return locked;
			}
			set {
				locked = value;

				//MultiLink.MultiPlayer.SendSetLocked(this, Locked);
			}
		}

		public KeyLockedDoor(Vector2 position) : base(position, new Sprite(GameHandler.AssetHandler.GetTexture("tilesheet")), true) {
			Open = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Dungeon_DoorSlam");
		}

		public override void Reload() {
			Locked = true;
			Sprite.CurrentIndex = startIndex;
		}

		public override void Update() {
			if (Locked) {
				Rectangle bb = BoundingBox;
				bb.Inflate(5, 5);

				foreach (Entity e in (Parent as Room).EntitiesInclPlayer) {
					if (e as PlayerCharacter != null) {
						if (e.BoundingBox.Intersects(bb)) {
							if ((e as PlayerCharacter).Key > 0) {
								Locked = false;
								isSolid = false;
								startIndex = Sprite.CurrentIndex;
								Sprite.CurrentIndex = new Point(2, 0);
								(e as PlayerCharacter).AddKeys(-1);
								Open.Play();
							}
						}
					}
				}
			}

			base.Update();
		}
	}
}