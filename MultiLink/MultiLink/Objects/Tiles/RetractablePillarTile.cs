﻿using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Rooms;

namespace MultiLink.Objects.Tiles {
	internal class RetractablePillarTile : Tile {
		/// <summary>
		/// The types of retractablePillarTile
		/// </summary>
		public enum Types {
			Circle,
			Square,
			Triangle
		}

		public bool Inversed {
			get;
		}

		public Types Type {
			get;
		}

		public RetractablePillarTile(Vector2 position, Types type, bool inversed) : base(position, new Sprite(GameHandler.AssetHandler.GetTexture("dungeon")), false) {
			Type = type;
			Inversed = inversed;

			GraphicsAndSolidUpdate();
		}

		public override void Update() {
			LinkValueCheck();
			GraphicsAndSolidUpdate();
			base.Update();
		}

		private void GraphicsAndSolidUpdate() {
			switch (Type) {
				case Types.Circle:
					Sprite.Texture = GameHandler.AssetHandler.GetTexture("retractablepillar_circle");
					if (activated == Inversed) {
						Sprite.CurrentIndex = new Point(0, 0);
						isSolid = true;
					} else {
						Sprite.CurrentIndex = new Point(1, 0);
						isSolid = false;
					}
					break;
				case Types.Square:
					Sprite.Texture = GameHandler.AssetHandler.GetTexture("retractablepillar_square");
					if (activated == Inversed) {
						Sprite.CurrentIndex = new Point(0, 0);
						isSolid = true;
					} else {
						Sprite.CurrentIndex = new Point(1, 0);
						isSolid = false;
					}
					break;
				case Types.Triangle:
					Sprite.Texture = GameHandler.AssetHandler.GetTexture("retractablepillar_triangle");
					if (activated == Inversed) {
						Sprite.CurrentIndex = new Point(0, 0);
						isSolid = true;
					} else {
						Sprite.CurrentIndex = new Point(1, 0);
						isSolid = false;
					}
					break;
			}
		}

		private void LinkValueCheck() {
			bool value = false;
			switch (Type) {
				case Types.Circle:
					value = ((Room)Parent).LinkValues[0];
					break;
				case Types.Square:
					value = ((Room)Parent).LinkValues[1];
					break;
				case Types.Triangle:
					value = ((Room)Parent).LinkValues[2];
					break;
			}
			activated = value;
		}
	}
}