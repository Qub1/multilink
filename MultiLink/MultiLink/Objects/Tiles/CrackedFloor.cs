﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Characters;

namespace MultiLink.Objects.Tiles {
	internal class CrackedFloor : Tile {
		public enum Types {
			Instant,
			Once
		}

		private bool oldSteppedOn;
		private readonly SoundEffect Shatter;
		private bool steppedOn;

		public bool PlayerOnly {
			get;
		}

		public Types Type {
			get;
		}

		public CrackedFloor(Vector2 position, Types type, bool playerOnly = false) : base(position, new Sprite(GameHandler.AssetHandler.GetTexture("tilesheet")), false, SpecialTypes.None, true) {
			Sprite.CurrentIndex = new Point(3, 0);
			Type = type;
			PlayerOnly = playerOnly;
			GraphicsUpdate();
			Shatter = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Rock_Shatter");
		}

		public override void EntityOnTop(Entity e) {
			if (PlayerOnly) {
				if (e is CharacterObject) {
					steppedOn = true;
				}
			} else {
				steppedOn = true;
			}
		}

		public override void Reload() {
			isSolidFloor = true;
			GraphicsUpdate();
		}

		public override void Update() {
			steppedOn = false;
			base.Update();

			if ((Type == Types.Instant && steppedOn) || (Type == Types.Once && !steppedOn && oldSteppedOn)) {
				isSolidFloor = false;
				Shatter.Play();
			}

			oldSteppedOn = steppedOn;
			GraphicsUpdate();
		}

		private void GraphicsUpdate() {
			if (isSolidFloor) {
				Sprite.CurrentIndex = new Point(3, 0);
			} else {
				Sprite.CurrentIndex = new Point(14, 1);
			}
		}
	}
}