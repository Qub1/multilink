﻿using System;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities;
using MultiLink.Objects.Rooms;

namespace MultiLink.Objects.Tiles {
	internal class ArrowShooterTile : Tile {
		public enum Modes {
			Interval
		}

		public enum Orientations {
			Down,
			Left,
			Up,
			Right
		}

		public float ArrowSpeed = 10f;
		public float IntervalCounter;
		public float ShootInterval = 1000f;

		public Modes Mode {
			get;
		}

		public Orientations Orientation {
			get;
			private set;
		}

		public ArrowShooterTile(Vector2 position, Orientations orientation, Modes mode = Modes.Interval) : base(position, new Sprite(GameHandler.AssetHandler.GetTexture("arrowshooter")), true) {
			Orientation = orientation;
			Mode = mode;

			GraphicsUpdate();
		}

		public override void Rotate(int i = 1) {
			switch (Orientation) {
				case Orientations.Down:
					Orientation = Orientations.Right;
					break;
				case Orientations.Right:
					Orientation = Orientations.Up;
					break;
				case Orientations.Up:
					Orientation = Orientations.Left;
					break;
				case Orientations.Left:
					Orientation = Orientations.Down;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			base.Rotate(i);
		}

		public override void Update() {
			if (Mode == Modes.Interval) {
				IntervalCounter += GameHandler.InfoHandler.GameTime.ElapsedGameTime.Milliseconds;
				if (IntervalCounter >= ShootInterval) {
					SpawnArrow();
					IntervalCounter -= ShootInterval;
				}
			}
		}

		public static Vector2 OrientationToVector2(Orientations o) {
			switch (o) {
				case Orientations.Up:
					return -Vector2.UnitY;
				case Orientations.Down:
					return Vector2.UnitY;
				case Orientations.Left:
					return -Vector2.UnitX;
				case Orientations.Right:
					return Vector2.UnitX;
				default:
					return Vector2.Zero;
			}
		}

		private void GraphicsUpdate() {
			switch (Orientation) {
				case Orientations.Down:
					Sprite.CurrentIndex = new Point(0, 0);
					break;
				case Orientations.Left:
					Sprite.CurrentIndex = new Point(1, 0);
					break;
				case Orientations.Up:
					Sprite.CurrentIndex = new Point(2, 0);
					break;
				case Orientations.Right:
					Sprite.CurrentIndex = new Point(3, 0);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void SpawnArrow() {
			Room parentRoom = Parent as Room;
			if (parentRoom == null) {
				return;
			}
			Entity arrow = new ArrowEntity(Position + UnitSize * OrientationToVector2(Orientation), OrientationToVector2(Orientation), ArrowSpeed);
			parentRoom.AddEntity(arrow);
		}
	}
}