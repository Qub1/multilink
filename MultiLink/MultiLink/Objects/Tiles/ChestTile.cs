﻿using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Items;
using MultiLink.Objects.Items.DropTable;

namespace MultiLink.Objects.Tiles {
	/// <summary>
	/// A chest that can store items.
	/// </summary>
	public class ChestTile : Tile {
		public enum States {
			Open,
			Closed
		}

		/// <summary>
		/// The types of chest.
		/// </summary>
		public enum Types {
			Standard,
			Key
		}

		private States actualState = States.Closed;
		private readonly Item content;

		public States currentState {
			get {
				return actualState;
			}
			set {
				actualState = value;
				switch (Type) {
					case Types.Standard:
						if (value == States.Closed) {
							Sprite.CurrentIndex = new Point(0, 3);
						} else if (value == States.Open) {
							Sprite.CurrentIndex = new Point(1, 3);
						}
						break;
					case Types.Key:
						if (value == States.Closed) {
							Sprite.CurrentIndex = new Point(2, 3);
						} else if (value == States.Open) {
							Sprite.CurrentIndex = new Point(3, 3);
						}
						break;
				}
			}
		}

		/// <summary>
		/// Current state of the chest (open or closed)
		/// </summary>
		public States CurrentState {
			get {
				return currentState;
			}
			set {
				currentState = value;
				//MultiLink.MultiPlayer.SendSetChestState(this, CurrentState);
			}
		}

		public bool IsClosed => currentState == States.Closed;
		public bool IsOpen => !IsClosed;

		/// <summary>
		/// The current type.
		/// </summary>
		public Types Type {
			get;
			set;
		}

		/// <summary>
		/// Creates a new ContainerObject.
		/// </summary>
		/// <param name="position">The position.</param>
		/// <param name="type">The type of tile.</param>
		public ChestTile(Vector2 position, Types type = Types.Standard) : base(position, new Sprite(GameHandler.AssetHandler.GetTexture("tilesheet")), true) {
			Type = type;
			CurrentState = States.Closed;
			//Determine content
			if (type == Types.Standard) {
				DropTable table = DropTable.Chest;
				content = table.GetRandomItem();
			} else {
				content = new Item(Item.ItemTypes.Key);
			}
		}

		public override void Reload() {
			CurrentState = States.Closed;
		}

		public override void Update() {
			if (currentState == States.Closed) {
				Rectangle r = BoundingBox;
				r.X--;
				r.Y--;
				r.Width += 2;
				r.Height += 2;

				/*foreach (Entity e in (Parent as Room).EntitiesInclPlayer) {
                    if ((e as PlayerCharacter) == null)
                        continue;

                    if (e.BoundingBox.Intersects(r)) {
                        this.OnOpen(e as PlayerCharacter);
                    }
                }*/
			}

			base.Update();
		}

		public Sprite OnOpen(PlayerCharacter player) {
			content.OnPickUp(player);
			CurrentState = States.Open;
			return content.ItemSprite;
		}
	}
}