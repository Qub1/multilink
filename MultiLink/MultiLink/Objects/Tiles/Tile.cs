﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using MultiLibrary.Objects;
using MultiLibrary.Types;
using MultiLink.Objects.Rooms;
using MultiLink.Worlds;

namespace MultiLink.Objects.Tiles {
	public class Tile : DiscoverableObject {
		public enum SpecialTypes {
			None,
			Ice,
			Damage
		}

		public static readonly Vector2 UnitSize = new Vector2(40f, 40f);
		public const float UnitX = 40f;
		public const float UnitY = 40f;
		public bool staticRendered = false;
		protected bool activated;
		protected bool isSolid;
		protected bool isSolidFloor;
		public bool Activated => activated;

		/// <summary>
		/// The position of the voxelobject on the grid
		/// </summary>
		public Vector2 GridPosition => Position / new Vector2(UnitX, UnitY);

		/// <summary>
		/// If the tile is a solid
		/// </summary>
		public bool IsSolid => isSolid;

		/// <summary>
		/// If the tile is a solid floor
		/// </summary>
		public bool IsSolidFloor => isSolidFloor;

		public SpecialTypes SpecialType {
			get;
		}

		public Tile(Vector2 position, Sprite sprite, bool isSolid = false, SpecialTypes specialType = SpecialTypes.None, bool isSolidFloor = true) : base(position * UnitSize, UnitSize) {
			Sprite = sprite;
			this.isSolid = isSolid;
			this.isSolidFloor = isSolidFloor;
			Layer = 0f;
			SpecialType = specialType;
		}

		public override void Update() {
			Room room = Parent as Room;
			if (room != null) {
				Rectangle rect = BoundingBox;

				foreach (Entity e in room.EntitiesInclPlayer.Where(e => rect.Intersects(e.BoundingBox))) {
					EntityOnTop(e);
				}
			}
			if (activated) {
				IsActivate();
			}

			base.Update();
		}

		/// <summary>
		/// Activate the voxelobject
		/// </summary>
		public void Activate() {
			activated = true;
			IsActivated();
		}

		/// <summary>
		/// Deactive the tile
		/// </summary>
		public void Deactivate() {
			activated = false;
			IsDeactivated();
		}

		public virtual void EntityOnTop(Entity e) {
		}

		public virtual void IsActivate() {
		}

		public virtual void IsActivated() {
		}

		public virtual void IsDeactivated() {
		}

		public virtual void Reload() {
		}

		public virtual void Rotate(int i = 1) {
			if (i - 1 >= 0) {
				Rotate(i - 1);
			}
		}

		/// <summary>
		/// Set the activation of the voxelobject
		/// </summary>
		public void SetActivated(bool value) {
			if (value) {
				Activate();
			} else {
				Deactivate();
			}
		}

		public void StaticDraw() {
			StaticRenderDraw();
		}

		/// <summary>
		/// Toggle between value and deactivated
		/// </summary>
		public void ToggleActivated() {
			if (activated) {
				Deactivate();
			} else {
				Activate();
			}
		}
	}
}