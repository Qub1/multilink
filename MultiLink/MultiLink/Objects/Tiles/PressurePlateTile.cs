﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Rooms;

namespace MultiLink.Objects.Tiles {
	public class PressurePlateTile : Tile {
		/// <summary>
		/// The types of pressure plates there are
		/// </summary>
		public enum Types {
			Circle,
			Square,
			Triangle
		}

		public List<Entity> TargetEntities = new List<Entity>();

		//Targets of the pressure plates
		public List<Tile> TargetTiles = new List<Tile>();
		private bool entityOnTop;
		private readonly SoundEffect Lever;
		private bool oldActivated;
		private bool playerOnTop;
		private readonly Types type;

		/// <summary>
		/// Whether the Pressure Plate can only be pressed once
		/// </summary>
		public bool Once {
			get;
		}

		/// <summary>
		/// Whether the Pressure Plate can only be activated by a player
		/// </summary>
		public bool PlayerOnly {
			get;
		}

		public PressurePlateTile(Vector2 position, Types type, bool once = false, bool playerOnly = false) : base(position, new Sprite(GameHandler.AssetHandler.GetTexture("tilesheet")), false) {
			Once = once;
			isSolid = false;
			PlayerOnly = playerOnly;
			this.type = type;
			GraphicsUpdate();
			Lever = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_FloorTiles");
		}

		public override void EntityOnTop(Entity e) {
			entityOnTop = true;
			if (e is PlayerCharacter || e is PushableBlockEntity) {
				playerOnTop = true;
			}
		}

		public override void IsActivated() {
			switch (type) {
				case Types.Circle:
					((Room)Parent).LinkValues[0] = true;
					return;
				case Types.Square:
					((Room)Parent).LinkValues[1] = true;
					return;
				case Types.Triangle:
					((Room)Parent).LinkValues[2] = true;
					return;
			}
		}

		public override void IsDeactivated() {
			switch (type) {
				case Types.Circle:
					((Room)Parent).LinkValues[0] = false;
					return;
				case Types.Square:
					((Room)Parent).LinkValues[1] = false;
					return;
				case Types.Triangle:
					((Room)Parent).LinkValues[2] = false;
					return;
			}
		}

		public override void Reload() {
			oldActivated = false;
			entityOnTop = false;
			playerOnTop = false;
			GraphicsUpdate();
		}

		public override void Update() {
			entityOnTop = false;
			playerOnTop = false;
			base.Update();
			if (Once && oldActivated) {
				entityOnTop = true;
				playerOnTop = true;
				Activate();
			}

			if (PlayerOnly) {
				if (playerOnTop && entityOnTop && !activated && !oldActivated) {
					Activate();
					Lever.Play();
				} else if (!playerOnTop && oldActivated) {
					Deactivate();
					Lever.Play();
				}
			} else {
				if (entityOnTop && !activated && !oldActivated) {
					Activate();
					Lever.Play();
				} else if (!entityOnTop) {
					Deactivate();
					Lever.Play();
				}
			}

			oldActivated = activated;

			GraphicsUpdate();
		}

		public void AddTargetTile(Tile t) {
			TargetTiles.Add(t);
		}

		public void GraphicsUpdate() {
			if (activated) {
				Sprite.CurrentIndex = new Point(1, 0);
				switch (type) {
					case Types.Circle:
						Sprite.CurrentIndex = new Point(5, 3);
						return;
					case Types.Square:
						Sprite.CurrentIndex = new Point(7, 3);
						return;
					case Types.Triangle:
						Sprite.CurrentIndex = new Point(9, 3);
						return;
				}
			} else {
				Sprite.CurrentIndex = new Point(0, 0);
				switch (type) {
					case Types.Circle:
						Sprite.CurrentIndex = new Point(4, 3);
						return;
					case Types.Square:
						Sprite.CurrentIndex = new Point(6, 3);
						return;
					case Types.Triangle:
						Sprite.CurrentIndex = new Point(8, 3);
						return;
				}
			}
		}
	}
}