﻿using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Rooms;
using MultiLink.Worlds;

namespace MultiLink.Objects.Tiles {
	internal class EndStairsTile : Tile {
		public EndStairsTile(Vector2 position) : base(position, new Sprite(GameHandler.AssetHandler.GetTexture("tilesheet")), false) {
			Sprite.CurrentIndex = new Point(0, 15);
		}

		public override void EntityOnTop(Entity e) {
			if (e as PlayerCharacter != null) {
				((Parent as Room).Parent as World).WinGame();
			}
		}
	}
}