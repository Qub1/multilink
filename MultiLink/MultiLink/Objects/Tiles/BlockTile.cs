﻿using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;

namespace MultiLink.Objects.Tiles {
	internal class BlockTile : Tile {
		/// <summary>
		/// Creates a new unpushable block
		/// </summary>
		public BlockTile(Vector2 position) : base(position, new Sprite(GameHandler.AssetHandler.GetTexture("tilesheet")), true) {
			Sprite.CurrentIndex = new Point(6, 0);
		}
	}
}