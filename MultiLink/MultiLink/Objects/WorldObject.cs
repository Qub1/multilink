﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Rooms;

namespace MultiLink.Objects {
	/// <summary>
	/// A 3D world state.
	/// </summary>
	public class WorldObject : GameObject {
		/// <summary>
		/// The actual player.
		/// </summary>
		public PlayerCharacter Player,
							   Player2;

		private bool playMusic;
		public List<Room> Rooms => Children.OfType<Room>().ToList();

		/// <summary>
		/// Creates a new PlayingState.
		/// </summary>
		public WorldObject() : base(Vector2.Zero, Vector2.Zero) {
			// Create player

			if (MultiLink.MultiPlayer.IsOnline) {
				InputMapping p1input = new InputMapping(Keys.W, Keys.S, Keys.A, Keys.D, Keys.J, Keys.I, Keys.U, Keys.K);
				Player = new PlayerCharacter(new Vector2(800F, 800F), p1input, SpriteMapping.BluePlayerMap);
				AddChild(Player);
				InputMapping p2input = new InputMapping(Keys.W, Keys.S, Keys.A, Keys.D, Keys.J, Keys.I, Keys.U, Keys.K);
				Player2 = new PlayerCharacter(new Vector2(850f, 800f), p2input, SpriteMapping.RedPlayerMap);
				AddChild(Player2);
			} else {
				InputMapping p1input = new InputMapping(Keys.W, Keys.S, Keys.A, Keys.D, Keys.F, Keys.T, Keys.R, Keys.G);
				Player = new PlayerCharacter(new Vector2(800F, 800F), p1input, SpriteMapping.BluePlayerMap);
				AddChild(Player);
				InputMapping p2input = new InputMapping(Keys.Up, Keys.Down, Keys.Left, Keys.Right, Keys.NumPad5, Keys.NumPad9, Keys.NumPad8, Keys.NumPad6);
				Player2 = new PlayerCharacter(new Vector2(850f, 800f), p2input, SpriteMapping.RedPlayerMap);
				AddChild(Player2);
			}

			if (MultiLink.MultiPlayer.IsOnline) {
				Player.CanControl = MultiLink.MultiPlayer.IsHost;
				Player2.CanControl = !MultiLink.MultiPlayer.IsHost;
			} else {
				Player.CanControl = true;
				Player2.CanControl = true;
			}
		}

		public override void Update() {
			if (!playMusic) {
				MediaPlayer.Stop();
				MediaPlayer.Play(GameHandler.AssetHandler.GetSong("gamemusic"));
				MediaPlayer.IsRepeating = true;
				playMusic = true;
			}

			if (MultiLink.MultiPlayer.IsOnline) {
				if (Player.CanControl) {
					GameHandler.GraphicsHandler.Camera.Target = Player.Position;
					GameHandler.GraphicsHandler.Camera.SecondTarget = Player.Position;
				} else {
					GameHandler.GraphicsHandler.Camera.Target = Player2.Position;
					GameHandler.GraphicsHandler.Camera.SecondTarget = Player2.Position;
				}
			} else {
				GameHandler.GraphicsHandler.Camera.Target = Player.Position;
				GameHandler.GraphicsHandler.Camera.SecondTarget = Player2.Position;
			}
			base.Update();
		}
	}
}