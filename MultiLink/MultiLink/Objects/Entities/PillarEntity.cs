﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using MultiLibrary.Handlers;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Entities {
	internal class PillarEntity : Entity {
		/// <summary>
		/// The types of retractablePillarTile
		/// </summary>
		public enum Types {
			Circle,
			Square,
			Triangle
		}

		private bool activated;
		private SoundEffect Lever;

		public bool Inversed {
			get;
		}

		public Types Type {
			get;
		}

		public PillarEntity(Vector2 position, Types type, bool inversed) : base(position, Tile.UnitSize) {
			Type = type;
			Inversed = inversed;
			useNormalPhysics = false;
			GraphicsAndSolidUpdate();
			Lever = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_FloorTiles");
		}

		public override void Update() {
			LinkValueCheck();
			GraphicsAndSolidUpdate();
			base.Update();
		}

		private void GraphicsAndSolidUpdate() {
			Sprite.Texture = GameHandler.AssetHandler.GetTexture("tilesheet");
			switch (Type) {
				case Types.Circle:
					if (activated == Inversed) {
						Sprite.CurrentIndex = new Point(2, 5);
						isSolid = true;
					} else {
						Sprite.CurrentIndex = new Point(3, 5);
						isSolid = false;
					}
					break;
				case Types.Square:
					if (activated == Inversed) {
						Sprite.CurrentIndex = new Point(0, 5);
						isSolid = true;
					} else {
						Sprite.CurrentIndex = new Point(1, 5);
						isSolid = false;
					}
					break;
				case Types.Triangle:
					if (activated == Inversed) {
						Sprite.CurrentIndex = new Point(4, 5);
						isSolid = true;
					} else {
						Sprite.CurrentIndex = new Point(5, 5);
						isSolid = false;
					}
					break;
			}
		}

		private void LinkValueCheck() {
			bool value = false;
			switch (Type) {
				case Types.Circle:
					value = ParentRoom.LinkValues[0];
					break;
				case Types.Square:
					value = ParentRoom.LinkValues[1];
					break;
				case Types.Triangle:
					value = ParentRoom.LinkValues[2];
					break;
			}
			activated = value;
		}
	}
}