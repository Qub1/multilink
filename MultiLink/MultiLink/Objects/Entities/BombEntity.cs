﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Entities {
	internal class BombEntity : Entity {
		private const int damage = 4;
		private readonly SoundEffect Bomb;
		private readonly Dictionary<Rectangle, DamagePair> damageDictionary = new Dictionary<Rectangle, DamagePair>();
		private bool exploded;
		private float explosiontimer;
		private float timer;

		public BombEntity(Vector2 position, float timer, Vector2 velocity) : base(position, new Vector2(90, 90)) {
			this.timer = timer;
			Velocity = velocity;
			MovementSpeed = Velocity.Length();
			isSolid = false;

			BoundingBoxSizeOffset = new Vector2(-60);
			RenderOffset = new Vector2(-30, -30);

			Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("explosion"));
			Sprite.AnimationStartIndex = Point.Zero;
			Sprite.AnimationLength = 7;
			Sprite.AnimationFrameTime = TimeSpan.FromMilliseconds(100f / 7f);
			Sprite.Animated = false;

			Bomb = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Bomb_Explode");
			networkedPosition = false;
		}

		public override Dictionary<Rectangle, DamagePair> GetDamageRectangles() {
			return damageDictionary;
		}

		public override void Update() {
			if (!new Rectangle(0, 0, (int)(ParentRoom.Size.X * Tile.UnitX), (int)(ParentRoom.Size.Y * Tile.UnitY)).Intersects(BoundingBox)) {
				ParentRoom.RemoveEntity(this);
			}

			//Slow the bomb down
			Damping();

			if (!exploded) {
				//Of time is out
				timer -= GameHandler.InfoHandler.GameTime.ElapsedGameTime.Milliseconds;
				if (timer <= 0f) {
					Explode();
					Bomb.Play();
				}
			} else {
				explosiontimer -= GameHandler.InfoHandler.GameTime.ElapsedGameTime.Milliseconds;
				if (explosiontimer <= 0f) {
					ParentRoom.RemoveEntity(this);
				}
			}
			base.Update();
		}

		private void Damping() {
			Velocity = Velocity * .9f;
			if (Velocity.Length() <= .2f) {
				Velocity = Vector2.Zero;
			}
		}

		private void Explode() {
			exploded = true;
			explosiontimer = 100f;
			Sprite.Animated = true;
			damageDictionary.Add(new Rectangle((int)Position.X, (int)Position.Y, (int)Size.X, (int)Size.Y), new DamagePair(this, damage));
		}
	}
}