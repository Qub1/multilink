﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Rooms;
using MultiLink.Objects.Tiles;
using MultiLink.Worlds;

namespace MultiLink.Objects {
	public class Entity : DiscoverableObject {
		public static bool inMenu = false;

		protected bool networkedPosition = true;

		/// <summary>
		/// The movement speed of the entity.
		/// </summary>
		public float MovementSpeed {
			get {
				if (movementSpeed == -1f) {
					if (HighestVelocity != Vector2.Zero) {
						return HighestVelocity.Length();
					}
					return 3f;
				}
				return movementSpeed;
			}
			set {
				movementSpeed = value;
			}
		}

		private float movementSpeed = -1f;

		/// <summary>
		/// The room the entity is in
		/// </summary>
		public Room ParentRoom;

		public Vector2? RemotePosition;
		protected bool collided;
		protected bool isSolid = true;
		protected bool useNormalPhysics = true;
		private int interpolationTries;

		/// <summary>
		/// The current position of the entity on the grid
		/// </summary>
		public Vector2 GridPosition {
			get {
				return new Vector2((float)Math.Floor(Position.X / Tile.UnitX - ParentRoom.Position.X / Tile.UnitX), (float)Math.Floor((Position.Y + 50f) / Tile.UnitY - ParentRoom.Position.Y / Tile.UnitY));
			}
		}

		public bool IsSolid {
			get {
				return isSolid;
			}
		}

		public Vector2 PositionRelativeToRoom {
			get {
				return Position - ParentRoom.Position + BoundingBoxOffset;
			}
		}

		/// <summary>
		/// Create a new entity
		/// </summary>
		public Entity(Vector2 position, Vector2 size) : base(position, size) {
			Layer = .5f;

			PropertyChanged += Networking;
		}

		public override void Update() {
			// Move towards remote position
			if (RemotePosition != null && networkedPosition) {
				float distance = Vector2.Distance(Position, (Vector2)RemotePosition);
				if (distance <= MovementSpeed || (!MultiLink.MultiPlayer.IsHost && interpolationTries >= 60)) {
					position = (Vector2)RemotePosition;
					RemotePosition = null;
					Velocity = Vector2.Zero;
					interpolationTries = 0;
				} else if (MultiLink.MultiPlayer.IsHost && interpolationTries >= 60) {
					// If host and the tries exceed the count, reset the remote player
					MultiLink.MultiPlayer.SendSetPosition(this, Position, false);
					RemotePosition = null;
					interpolationTries = 0;
				} else {
					Vector2 direction = Vector2.Normalize((Vector2)RemotePosition - Position);
					float interpolationSpeed = MovementSpeed;
					if (!MultiLink.MultiPlayer.IsHost) {
						// If not host, catch up faster
						interpolationSpeed = distance / MovementSpeed / 2;
					}
					Velocity = direction * interpolationSpeed;
					interpolationTries++;
				}
			}

			if (inMenu) {
				return;
			}

			if (useNormalPhysics) {
				collided = PhysicsCheck();
			}

			base.Update();
		}

		public virtual Dictionary<Rectangle, DamagePair> GetDamageRectangles() {
			return new Dictionary<Rectangle, DamagePair>();
		}

		public bool PhysicsCheck() {
			bool collides = false;

			Rectangle bb = BoundingBox;

			Rectangle hr = new Rectangle(bb.X + (int)Velocity.X, bb.Y, bb.Width, bb.Height);
			Rectangle vr = new Rectangle(bb.X, bb.Y + (int)Velocity.Y, bb.Width, bb.Height);

			Vector2 pr;
			if (this is PlayerCharacter) {
				pr = PositionRelativeToRoom;
			} else {
				pr = Position;
			}

			bool hb = false,
				 vb = false;
			
			foreach (Rectangle r in ParentRoom.CollisionList) {
				if (r == BoundingBox) {
					continue;
				}

				if (Math.Abs(pr.X - r.X) + Math.Abs(pr.Y - r.Y) > 150) {
					continue;
				}

				World world = ((World)ParentRoom.Parent);

				if (this is PlayerCharacter && (r == world.Player.BoundingBox || r == world.Player2.BoundingBox)) {
					continue;
				}

				Vector2 horizontalMove = Vector2.Zero;
				Vector2 verticalMove = Vector2.Zero;

				//Horizontal check
				if (hr.Intersects(r)) {
					if (Velocity.X > 0) {
						Velocity = new Vector2(0f, Velocity.Y);
						horizontalMove = new Vector2(-(bb.Right - r.Left), 0f);
						collides = true;
						hb = true;
					} else if (Velocity.X < 0) {
						Velocity = new Vector2(0f, Velocity.Y);
						horizontalMove = new Vector2(-(bb.Left - r.Right), 0f);
						collides = true;
						hb = true;
					}
				}

				//Vertical check
				if (vr.Intersects(r)) {
					if (Velocity.Y > 0) {
						Velocity = new Vector2(Velocity.X, 0f);
						verticalMove = new Vector2(0f, -(bb.Bottom - r.Top));
						collides = true;
						vb = true;
					} else if (Velocity.Y < 0) {
						Velocity = new Vector2(Velocity.X, 0f);
						verticalMove = new Vector2(0f, -(bb.Top - r.Bottom));
						collides = true;
						vb = true;
					}
				}

				// Only move the one with the smallest collision
				if (horizontalMove.Length() < verticalMove.Length()) {
					//Position = Position - horizontalMove;
					//Velocity = MovementSpeed * Vector2.Normalize(horizontalMove);
				} else if(verticalMove.Length() <= horizontalMove.Length()) {
					//Position = Position - verticalMove;
					//Velocity = MovementSpeed * Vector2.Normalize(verticalMove);
				}

				if (hb && vb) {
					return true;
				}
			}
			
			return collides;
		}

		private void Networking(object sender, PropertyChangedEventArgs e) {
			// Networking code:
			if (e.PropertyName == "Position" && RemotePosition == null) {
				MultiLink.MultiPlayer.SendSetPosition(this, Position);
			}
		}
	}
}