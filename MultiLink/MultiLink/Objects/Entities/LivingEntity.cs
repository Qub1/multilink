﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Enemies;
using MultiLink.Objects.Items.DropTable;
using MultiLink.Worlds;

namespace MultiLink.Objects.Entities {
	public class LivingEntity : Entity {
		/// <summary>
		/// If the LivingEntity is allowed to collect PickUps
		/// </summary>
		public bool AllowPickUps;

		/// <summary>
		/// The current DropTable
		/// </summary>
		public DropTable CurrentDropTable;

		public float DefenceFactor = 1f;
		public int health;
		protected float invincibilityCounter = -2f;
		protected bool isInvincible = false;
		protected int maxHealth;
		protected float maxInvincibilityCounter = 2000f;
		private bool allowPickUps; // TODO: Use variable
		private readonly SoundEffect Fall;
		private readonly SoundEffect Hit;
		private readonly SoundEffect Hurt;

		/// <summary>
		/// Current health of the Living Entity
		/// </summary>
		public int Health {
			get {
				return health;
			}
			set {
				if (Health != value) {
					health = value;

					MultiLink.MultiPlayer.SendSetHealth(this, Health);
				}
			}
		}

		/// <summary>
		/// The maximal Health of the living entity
		/// </summary>
		public int MaxHealth => maxHealth;

		public LivingEntity(Vector2 position, Vector2 size, int maxHealth, bool allowPickUps = false) : base(position, size) {
			this.maxHealth = maxHealth;
			Health = maxHealth;
			this.allowPickUps = allowPickUps;
			Hurt = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Link_Hurt");
			Fall = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Link_Fall");
			Hit = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Enemy_Hit");
		}

		public override void Draw() {
			base.Draw();

			if (Health < maxHealth && Health >= 0) {
				float totalBar = Size.X;
				float greenBar = totalBar * ((float)Health / maxHealth);
				float redBar = totalBar - greenBar;

				// Draw bar
				//World world = ParentRoom.Parent as World;
				Vector2 greenPosition = new Vector2(GlobalPosition.X, GlobalPosition.Y - 20) * GlobalScale;
				Vector2 greenSize = new Vector2(greenBar, 5) * GlobalScale;
				Vector2 redPosition = new Vector2(GlobalPosition.X + greenBar, GlobalPosition.Y - 20) * GlobalScale;
				Vector2 redSize = new Vector2(redBar, 5) * GlobalScale;
				GameHandler.GraphicsHandler.DrawRectangle(greenPosition, greenSize, Color.Green * Alpha, 1f); // Green
				GameHandler.GraphicsHandler.DrawRectangle(redPosition, redSize, Color.Red * Alpha, 1f); // Red
			}
		}

		public override void Update() {
			foreach (Rectangle r in ParentRoom.EmptyList.Where(r => BoundingBox.Intersects(r))) {
				OnFallOutOfRoom();
				Fall.Play();
			}

			invincibilityCounter -= GameHandler.InfoHandler.GameTime.ElapsedGameTime.Milliseconds;

			if (invincibilityCounter < 0f) {
				//HighestDamage
				int hDam = 0;
				foreach (KeyValuePair<Rectangle, DamagePair> p in ParentRoom.DamageList.Where(p => p.Value.Object != this)) {
					if (this is Enemy) {
						if (p.Value.Object is Enemy) {
							continue;
						}
					}
					if (p.Key.Intersects(BoundingBox)) {
						if (hDam < p.Value.Damage) {
							hDam = p.Value.Damage;
							Hit.Play();
						}
					}
				}
				if (hDam != 0) {
					Damage(hDam);
					invincibilityCounter = maxInvincibilityCounter;
					Hurt.Play();
				}
			}

			base.Update();
		}

		/// <summary>
		/// Damage the LivingEntity
		/// </summary>
		/// <param name="damage">amount of damage</param>
		public void Damage(float damage) {
			GameHandler.InfoHandler.Output(ToString() + " Damaged");
			if (damage <= 0f) {
				return;
			}
			Health -= (int)(damage * DefenceFactor);
			if (Health <= 0f && !isInvincible) {
				OnDie();
			}
		}

		/// <summary>
		/// Heal the LivingEntity
		/// </summary>
		/// <param name="heal">amount to heal</param>
		public void Heal(int heal) {
			GameHandler.InfoHandler.Output("Healed");
			Health += heal;
			if (Health > maxHealth) {
				Health = maxHealth;
			}
		}

		/// <summary>
		/// What happens when the entity dies
		/// </summary>
		public virtual void OnDie() {
			if (CurrentDropTable != null) {
				ParentRoom.AddEntity(CurrentDropTable.GetRandomItem().AsPickUpEntity(Position, 10000));
			}
			ParentRoom.RemoveEntity(this);
		}

		/// <summary>
		/// What happens when the entity falls out of the room
		/// </summary>
		public virtual void OnFallOutOfRoom() {
			OnDie();
		}
	}
}