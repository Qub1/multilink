﻿using Microsoft.Xna.Framework;
using MultiLibrary.Types;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Entities {
	public class BlockEntity : Entity {
		public BlockEntity(Vector2 position, Sprite sprite) : base(position, Tile.UnitSize) {
			Sprite = sprite;
			Sprite.CurrentIndex = new Point(7, 0);
			isSolid = true;
			useNormalPhysics = false;
		}
	}
}