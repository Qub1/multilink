﻿using System.Linq;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Items;

namespace MultiLink.Objects.Entities {
	public class PickUpEntity : Entity {
		private int activeTime;
		private readonly Item item;

		public PickUpEntity(Vector2 position, Item item, int activeTime) : base(position, new Vector2(20, 20)) {
			this.item = item;
			this.activeTime = activeTime;
			Sprite = item.ItemSprite;

			useNormalPhysics = false;
			isSolid = false;
			Layer = .55f;
		}

		public override void Update() {
			activeTime -= GameHandler.InfoHandler.GameTime.ElapsedGameTime.Milliseconds;
			if (activeTime <= 0) {
				Parent.RemoveChild(this);
			}

			PickUpCheck();
			base.Update();
		}

		private void OnPickUp(LivingEntity e) {
			item.OnPickUp(e as PlayerCharacter);
			ParentRoom.RemoveEntity(this);
		}

		private void PickUpCheck() {
			foreach (PlayerCharacter le in ParentRoom.EntitiesInclPlayer.OfType<PlayerCharacter>().Where(le => BoundingBox.Intersects(le.BoundingBox))) {
				OnPickUp(le);
			}
		}
	}
}