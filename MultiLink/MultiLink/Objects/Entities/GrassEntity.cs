﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Entities {
	internal class GrassEntity : LivingEntity {
		private float cutDownTimer = -1f;
		private readonly SoundEffect Grass;

		public object TimeSpawn {
			get;
			private set;
		}

		public GrassEntity(Vector2 position) : base(position, Tile.UnitSize, 1, false) {
			Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("tilesheet"));
			Sprite.CurrentIndex = new Point(8, 0);
			useNormalPhysics = false;
			isSolid = true;

			Grass = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Bush_Cut");
		}

		public override void OnDie() {
			cutDownTimer = 200f;
			Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("grass_cut"));
			Sprite.AnimationStartIndex = Point.Zero;
			Sprite.AnimationLength = 2;
			Sprite.AnimationFrameTime = TimeSpan.FromMilliseconds(200f / 2f);
			Sprite.Animated = true;
		}

		public override void Update() {
			if (cutDownTimer > 0) {
				cutDownTimer -= GameHandler.InfoHandler.GameTime.ElapsedGameTime.Milliseconds;

				if (cutDownTimer <= 0) {
					ParentRoom.RemoveEntity(this);
					Grass.Play();
				}
			}
			base.Update();
		}
	}
}