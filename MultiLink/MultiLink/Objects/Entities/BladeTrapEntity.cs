﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Entities {
	internal class BladeTrapEntity : Entity {
		private const float attackSpeed = 8f;
		private const int damage = 2;
		private const float returnSpeed = 4f;
		private Vector2 direction;
		private bool rotated;
		private Vector2 startPosition;
		private State state = State.Inactive;

		/// <summary>
		/// Creates a new BladeTrapEntity.
		/// </summary>
		public BladeTrapEntity(Vector2 position) : base(position, Tile.UnitSize) {
			Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("BladeTrap"));
			isSolid = true;
			startPosition = position;
			direction = Vector2.Zero;
		}

		public override Dictionary<Rectangle, DamagePair> GetDamageRectangles() {
			Rectangle bb = BoundingBox;
			bb.Inflate(5, 5);
			Dictionary<Rectangle, DamagePair> dic = new Dictionary<Rectangle, DamagePair> {
				{
					bb, new DamagePair(this, damage)
				}
			};
			return dic;
		}

		public override void Update() {
			base.Update();
			if (!rotated) {
				startPosition = Position; // Rotation fix
				rotated = true;
			}

			//Change behaviour based on current state
			if (state == State.Inactive) {
				CheckPlayerInRange();
			} else if (state == State.Active) {
				if (Velocity == Vector2.Zero) {
					state = State.Cooldown;
					Velocity = direction * -returnSpeed;
				}
			} else if (state == State.Cooldown) {
				if ((Position.X < startPosition.X + 5) && (Position.X > startPosition.X - 5) && (Position.Y < startPosition.Y + 5) && (Position.X > startPosition.X - 5)) {
					Velocity = Vector2.Zero;
					Position = startPosition;
					state = State.Inactive;
					if (Velocity == Vector2.Zero) {
						Position = startPosition;
					}
				}
			}
			//Check for collisions
			BladeTrapCollisionCheck();
		}

		/// <summary>
		/// Check for collision with other BladeTrapEntities
		/// </summary>
		private void BladeTrapCollisionCheck() {
			if (ParentRoom.EntitiesInclPlayer.OfType<BladeTrapEntity>().Where(e => e != this && e.BoundingBox.Intersects(BoundingBox)).ToList().Count > 0) {
				state = State.Cooldown;
				Velocity = direction * -4f;
			}
		}

		/// <summary>
		/// Check whether the player is close enough to attack
		/// </summary>
		private void CheckPlayerInRange() {
			foreach (PlayerCharacter player in ParentRoom.EntitiesInclPlayer.OfType<PlayerCharacter>()) {
				direction = Vector2.Zero;
				if (player.PositionRelativeToRoom.X < Position.X + 40 && player.PositionRelativeToRoom.X > Position.X - 0) {
					direction = player.PositionRelativeToRoom.Y < Position.Y ? new Vector2(0, -1) : new Vector2(0, 1);
					state = State.Active;
					Velocity = direction * attackSpeed;
					break;
				}
				if (player.PositionRelativeToRoom.Y < Position.Y + 40 && player.PositionRelativeToRoom.Y > Position.Y - 0) {
					direction = player.PositionRelativeToRoom.X < Position.X ? new Vector2(-1, 0) : new Vector2(1, 0);
					state = State.Active;
					Velocity = direction * attackSpeed;
					break;
				}
			}
		}

		/// <summary>
		/// Possible states for this entity
		/// </summary>
		private enum State {
			Inactive,
			Active,
			Cooldown
		}
	}
}