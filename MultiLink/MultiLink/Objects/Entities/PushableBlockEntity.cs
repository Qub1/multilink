﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Entities {
	public class PushableBlockEntity : Entity {
		public enum Directions {
			Up,
			Down,
			Left,
			Right,
			None
		}

		public enum MovementStyles {
			Up,
			Down,
			Left,
			Right,
			Horizontal,
			Vertical,
			Omni
		}

		public enum Pushabilities {
			Infinite,
			Once
		}

		private const float transitionTime = 30f;
		private bool onIce;
		private bool pushedOnce;
		private readonly bool showMovementStyle;
		private Vector2 startPosition; // TODO: Use variable
		private float transitionTimer = -1;

		public Directions CurrentDirection {
			get;
			private set;
		} = Directions.None;

		public MovementStyles MovementStyle {
			get;
		}

		public Pushabilities Pushability {
			get;
		}

		public PushableBlockEntity(Vector2 position, Pushabilities pushability, MovementStyles movementStyle, bool showMovementStyle = false) : base(position, Tile.UnitSize) {
			Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("tilesheet"));

			Pushability = pushability;
			MovementStyle = movementStyle;
			this.showMovementStyle = showMovementStyle;

			Sprite.CurrentIndex = new Point(7, 0);

			useNormalPhysics = false;
		}

		public override void Update() {
			base.Update();

			if (CurrentDirection != Directions.None) {
				MoveUpdate();
			}
		}

		public bool AllowedToMove(Directions dir) {
			Rectangle bb = new Rectangle((int)(BoundingBox.X + DirectionToVector(dir).X * Tile.UnitX), (int)(BoundingBox.Y + DirectionToVector(dir).Y * Tile.UnitY), BoundingBox.Width, BoundingBox.Height);
			return ParentRoom.CollisionList.All(r => !bb.Intersects(r));
		}

		public void CheckPush() {
			foreach (Entity e in ParentRoom.EntitiesInclPlayer) {
				if (e == this) {
					continue;
				}
				if (!(e is PlayerCharacter)) {
					continue;
				}

				int buf = 1;
				//left check
				if (e.BoundingBox.Intersects(new Rectangle((int)Position.X - buf, (int)Position.Y, buf, (int)Size.Y))) {
					PushDirection(Directions.Right);
				}
				//right check
				if (e.BoundingBox.Intersects(new Rectangle((int)Position.X + (int)Size.X, (int)Position.Y, buf, (int)Size.Y))) {
					PushDirection(Directions.Left);
				}
				//up check
				if (e.BoundingBox.Intersects(new Rectangle((int)Position.X, (int)Position.Y - buf, (int)Size.X, buf))) {
					PushDirection(Directions.Down);
				}
				//down check
				if (e.BoundingBox.Intersects(new Rectangle((int)Position.X, (int)Position.Y + (int)Size.Y, (int)Size.X, buf))) {
					PushDirection(Directions.Up);
				}
			}
		}

		public bool DirectionAllowed(Directions dir) {
			switch (dir) {
				case Directions.None:
					return true;
				case Directions.Left:
					if ((MovementStyle == MovementStyles.Left) || (MovementStyle == MovementStyles.Horizontal) || (MovementStyle == MovementStyles.Omni)) {
						return true;
					}
					return false;
				case Directions.Right:
					if ((MovementStyle == MovementStyles.Right) || (MovementStyle == MovementStyles.Horizontal) || (MovementStyle == MovementStyles.Omni)) {
						return true;
					}
					return false;
				case Directions.Up:
					if ((MovementStyle == MovementStyles.Up) || (MovementStyle == MovementStyles.Vertical) || (MovementStyle == MovementStyles.Omni)) {
						return true;
					}
					return false;
				case Directions.Down:
					if ((MovementStyle == MovementStyles.Down) || (MovementStyle == MovementStyles.Vertical) || (MovementStyle == MovementStyles.Omni)) {
						return true;
					}
					return false;
				default:
					return false;
			}
		}

		public static Vector2 DirectionToVector(Directions dir) {
			switch (dir) {
				case Directions.None:
					return Vector2.Zero;
				case Directions.Up:
					return new Vector2(0, -1);
				case Directions.Down:
					return new Vector2(0, 1);
				case Directions.Left:
					return new Vector2(-1, 0);
				case Directions.Right:
					return new Vector2(1, 0);
				default:
					return Vector2.Zero;
			}
		}

		public void MoveUpdate() {
			if (!OnIce()) {
				transitionTimer += 1 / transitionTime;
				if (onIce) {
					StopMovement();
				}
			} else if (PhysicsCheck()) {
				StopMovement();
			}

			if (transitionTimer >= 1f) {
				StopMovement();
			}
		}

		public bool OnIce() {
			Rectangle bb = BoundingBox;
			if (ParentRoom.SpecialFloors.Where(t => t.SpecialType == Tile.SpecialTypes.Ice).Any(t => bb.Intersects(t.BoundingBox))) {
				onIce = true;
				return true;
			}
			return false;
		}

		public void PushDirection(Directions direction) {
			PushDirectionNetworked(direction);
			MultiLink.MultiPlayer.SendPushBlock(this, direction);
		}

		public void PushDirectionNetworked(Directions direction) {
			if (CurrentDirection != Directions.None) {
				return;
			}
			if (!DirectionAllowed(direction)) {
				return;
			}
			if ((Pushability == Pushabilities.Once) && pushedOnce) {
				return;
			}
			if (!AllowedToMove(direction)) {
				return;
			}
			CurrentDirection = direction;
			transitionTimer = 0f;
			startPosition = new Vector2(Position.X, Position.Y);
			Velocity = DirectionToVector(direction) * Tile.UnitSize / transitionTime;
			MovementSpeed = Velocity.Length();
			pushedOnce = true;
			onIce = false;
		}

		public void SetGraphics() {
			if (!showMovementStyle) {
				Sprite.CurrentIndex = Point.Zero;
				return;
			}
			switch (MovementStyle) {
				case MovementStyles.Up:
					Sprite.CurrentIndex = Point.Zero;
					break;
				case MovementStyles.Down:
					Sprite.CurrentIndex = Point.Zero;
					break;
				case MovementStyles.Left:
					Sprite.CurrentIndex = Point.Zero;
					break;
				case MovementStyles.Right:
					Sprite.CurrentIndex = Point.Zero;
					break;
				case MovementStyles.Horizontal:
					Sprite.CurrentIndex = Point.Zero;
					break;
				case MovementStyles.Vertical:
					Sprite.CurrentIndex = Point.Zero;
					break;
				case MovementStyles.Omni:
					Sprite.CurrentIndex = Point.Zero;
					break;
			}
		}

		public void StopMovement() {
			Position = Tile.UnitSize * new Vector2((float)Math.Floor((Position.X + 10f) / Tile.UnitX), (float)Math.Floor((Position.Y + 10f) / Tile.UnitY));
			transitionTimer = -1f;
			CurrentDirection = Directions.None;
			Velocity = Vector2.Zero;

			//Remove the block if it 'hovers over an empty tile'
			foreach (Rectangle r in ParentRoom.EmptyList.Where(r => r.Intersects(BoundingBox))) {
				ParentRoom.RemoveEntity(this);
			}
		}
	}
}