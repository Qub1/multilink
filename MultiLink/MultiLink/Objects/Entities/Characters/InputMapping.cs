﻿using Microsoft.Xna.Framework.Input;

namespace MultiLink.Objects.Entities.Characters {
	public class InputMapping {
		public Keys UpKey,
					DownKey,
					LeftKey,
					RightKey,
					SwordKey,
					ArrowKey,
					BombKey,
					InteractKey;

		public InputMapping(Keys upKey, Keys downKey, Keys leftKey, Keys rightKey, Keys swordKey, Keys arrowKey, Keys bombKey, Keys interactKey) {
			UpKey = upKey;
			DownKey = downKey;
			LeftKey = leftKey;
			RightKey = rightKey;
			SwordKey = swordKey;
			ArrowKey = arrowKey;
			BombKey = bombKey;
			InteractKey = interactKey;
		}
	}
}