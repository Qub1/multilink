﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using MultiLibrary.Types;

namespace MultiLink.Objects.Entities.Characters {
	/// <summary>
	/// A regular animated character.
	/// </summary>
	public class CharacterObject : LivingEntity {
		public enum Direction {
			Left,
			Right,
			Up,
			Down
		}

		public Direction currentDirection = Direction.Down;
		public Vector2 RespawnPosition;
		protected Dictionary<Rectangle, DamagePair> damageDictionary = new Dictionary<Rectangle, DamagePair>();
		protected SpriteMapping SpriteMap;
		protected int swordDamage = 1;
		protected bool use = false;
		protected bool useSword = false;
		protected float useSwordTimer = -1f;
		protected float useTimer = -1f;

		public Direction CurrentDirection {
			get {
				return currentDirection;
			}
			set {
				if (value != currentDirection && RemotePosition == null) {
					currentDirection = value;

					MultiLink.MultiPlayer.SendSetDirection(this, CurrentDirection);
				}
			}
		}

		/// <summary>
		/// Creates a new CharacterObject.
		/// </summary>
		/// <param name="position">The position.</param>
		protected CharacterObject(Vector2 position, SpriteMapping spriteMap) : base(position, Vector2.Zero, 10, true) {
			SpriteMap = spriteMap;
			Sprite = SpriteMap.MoveForwardSprite;
		}

		public override Dictionary<Rectangle, DamagePair> GetDamageRectangles() {
			return damageDictionary;
		}

		public override void OnDie() {
			Health = maxHealth;
			Respawn();
		}

		public override void OnFallOutOfRoom() {
			Position = RespawnPosition;
			Damage(1);
		}

		public override void Update() {
			RenderOffset = Vector2.Zero;
			RenderSizeOffset = Vector2.Zero;

			if (!useSword && !use) {
				// Set sprite based on CurrentDirection
				if (Velocity.X < 0) {
					CurrentDirection = Direction.Left;
					Sprite = SpriteMap.MoveLeftSprite;
					Sprite.Animated = true;
				} else if (Velocity.X > 0) {
					CurrentDirection = Direction.Right;
					Sprite = SpriteMap.MoveRightSprite;
					Sprite.Animated = true;
				} else if (Velocity.Y < 0) {
					CurrentDirection = Direction.Up;
					Sprite = SpriteMap.MoveForwardSprite;
					Sprite.Animated = true;
				} else if (Velocity.Y > 0) {
					CurrentDirection = Direction.Down;
					Sprite = SpriteMap.MoveBackSprite;
					Sprite.Animated = true;
				} else {
					switch (CurrentDirection) {
						case Direction.Up:
							Sprite = SpriteMap.MoveForwardSprite;
							break;
						case Direction.Down:
							Sprite = SpriteMap.MoveBackSprite;
							break;
						case Direction.Left:
							Sprite = SpriteMap.MoveLeftSprite;
							break;
						case Direction.Right:
							Sprite = SpriteMap.MoveRightSprite;
							break;
					}
					Sprite.Animated = false;
				}
			} else if (useSword) {
				Sprite = SpriteMap.SwordSprite;
				RenderSizeOffset = new Vector2(50);
				switch (CurrentDirection) {
					case Direction.Down:
						Sprite.CurrentIndex = new Point(6, 0);
						RenderOffset = new Vector2(-50, 0);
						break;
					case Direction.Up:
						Sprite.CurrentIndex = new Point(1, 0);
						RenderOffset = new Vector2(-50);
						break;
					case Direction.Left:
						Sprite.CurrentIndex = new Point(3, 0);
						RenderOffset = new Vector2(-50);
						break;
					case Direction.Right:
						Sprite.CurrentIndex = new Point(4, 0);
						RenderOffset = new Vector2(0, -50);
						break;
				}
			} else if (use) {
				// Set sprite based on CurrentDirection
				if (Velocity.X < 0) {
					CurrentDirection = Direction.Left;
					Sprite = SpriteMap.UseLeftSprite;
				} else if (Velocity.X > 0) {
					CurrentDirection = Direction.Right;
					Sprite = SpriteMap.UseRightSprite;
				} else if (Velocity.Y < 0) {
					CurrentDirection = Direction.Up;
					Sprite = SpriteMap.UseUpSprite;
				} else if (Velocity.Y > 0) {
					CurrentDirection = Direction.Down;
					Sprite = SpriteMap.UseDownSprite;
				}
				Sprite.Animated = false;
			}
            base.Update();
        }

		public void Respawn() {
			position = RespawnPosition;
			MultiLink.MultiPlayer.SendSetPosition(this, Position, false);
		}
	}
}