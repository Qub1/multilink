﻿using System;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;

namespace MultiLink.Objects.Entities.Characters {
	public class SpriteMapping {
		public Sprite IdleSprite = new Sprite(),
					  MoveBackSprite = new Sprite(),
					  MoveForwardSprite = new Sprite(),
					  MoveLeftSprite = new Sprite(),
					  MoveRightSprite = new Sprite(),
					  UseDownSprite = new Sprite(),
					  UseLeftSprite = new Sprite(),
					  UseRightSprite = new Sprite(),
					  UseUpSprite = new Sprite(),
					  SwordSprite = new Sprite();

		//PreMadeProperties
		public static SpriteMapping BluePlayerMap {
			get {
				SpriteMapping m = new SpriteMapping();

				m.IdleSprite.Texture = GameHandler.AssetHandler.GetTexture("player_blue");

				m.MoveForwardSprite.Texture = GameHandler.AssetHandler.GetTexture("player_blue");
				m.MoveForwardSprite.Animated = true;
				m.MoveForwardSprite.AnimationStartIndex = new Point(0, 1);
				m.MoveForwardSprite.AnimationFrameTime = TimeSpan.FromMilliseconds(100);
				m.MoveForwardSprite.AnimationLength = 2;

				m.MoveBackSprite.Texture = GameHandler.AssetHandler.GetTexture("player_blue");
				m.MoveBackSprite.Animated = true;
				m.MoveBackSprite.AnimationStartIndex = new Point(0, 0);
				m.MoveBackSprite.AnimationFrameTime = TimeSpan.FromMilliseconds(100);
				m.MoveBackSprite.AnimationLength = 2;

				m.MoveLeftSprite.Texture = GameHandler.AssetHandler.GetTexture("player_blue");
				m.MoveLeftSprite.Animated = true;
				m.MoveLeftSprite.AnimationStartIndex = new Point(0, 3);
				m.MoveLeftSprite.AnimationFrameTime = TimeSpan.FromMilliseconds(100);
				m.MoveLeftSprite.AnimationLength = 2;

				m.MoveRightSprite.Texture = GameHandler.AssetHandler.GetTexture("player_blue");
				m.MoveRightSprite.Animated = true;
				m.MoveRightSprite.AnimationStartIndex = new Point(0, 2);
				m.MoveRightSprite.AnimationFrameTime = TimeSpan.FromMilliseconds(100);
				m.MoveRightSprite.AnimationLength = 2;

				m.UseRightSprite.Texture = GameHandler.AssetHandler.GetTexture("player_use_blue");
				m.UseRightSprite.CurrentIndex = new Point(3, 0);

				m.UseLeftSprite.Texture = GameHandler.AssetHandler.GetTexture("player_use_blue");
				m.UseLeftSprite.CurrentIndex = new Point(1, 0);

				m.UseUpSprite.Texture = GameHandler.AssetHandler.GetTexture("player_use_blue");
				m.UseUpSprite.CurrentIndex = new Point(2, 0);

				m.UseDownSprite.Texture = GameHandler.AssetHandler.GetTexture("player_use_blue");
				m.UseDownSprite.CurrentIndex = new Point(0, 0);

				m.SwordSprite.Texture = GameHandler.AssetHandler.GetTexture("player_sword_blue");

				return m;
			}
		}

		public static SpriteMapping RedPlayerMap {
			get {
				SpriteMapping m = new SpriteMapping();

				m.IdleSprite.Texture = GameHandler.AssetHandler.GetTexture("player_red");

				m.MoveForwardSprite.Texture = GameHandler.AssetHandler.GetTexture("player_red");
				m.MoveForwardSprite.Animated = true;
				m.MoveForwardSprite.AnimationStartIndex = new Point(0, 1);
				m.MoveForwardSprite.AnimationFrameTime = TimeSpan.FromMilliseconds(100);
				m.MoveForwardSprite.AnimationLength = 2;

				m.MoveBackSprite.Texture = GameHandler.AssetHandler.GetTexture("player_red");
				m.MoveBackSprite.Animated = true;
				m.MoveBackSprite.AnimationStartIndex = new Point(0, 0);
				m.MoveBackSprite.AnimationFrameTime = TimeSpan.FromMilliseconds(100);
				m.MoveBackSprite.AnimationLength = 2;

				m.MoveLeftSprite.Texture = GameHandler.AssetHandler.GetTexture("player_red");
				m.MoveLeftSprite.Animated = true;
				m.MoveLeftSprite.AnimationStartIndex = new Point(0, 3);
				m.MoveLeftSprite.AnimationFrameTime = TimeSpan.FromMilliseconds(100);
				m.MoveLeftSprite.AnimationLength = 2;

				m.MoveRightSprite.Texture = GameHandler.AssetHandler.GetTexture("player_red");
				m.MoveRightSprite.Animated = true;
				m.MoveRightSprite.AnimationStartIndex = new Point(0, 2);
				m.MoveRightSprite.AnimationFrameTime = TimeSpan.FromMilliseconds(100);
				m.MoveRightSprite.AnimationLength = 2;

				m.UseRightSprite.Texture = GameHandler.AssetHandler.GetTexture("player_use_red");
				m.UseRightSprite.CurrentIndex = new Point(3, 0);

				m.UseLeftSprite.Texture = GameHandler.AssetHandler.GetTexture("player_use_red");
				m.UseLeftSprite.CurrentIndex = new Point(1, 0);

				m.UseUpSprite.Texture = GameHandler.AssetHandler.GetTexture("player_use_red");
				m.UseUpSprite.CurrentIndex = new Point(2, 0);

				m.UseDownSprite.Texture = GameHandler.AssetHandler.GetTexture("player_use_red");
				m.UseDownSprite.CurrentIndex = new Point(0, 0);

				m.SwordSprite.Texture = GameHandler.AssetHandler.GetTexture("player_sword_red");

				return m;
			}
		}
	}
}