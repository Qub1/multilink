﻿using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLibrary.Types;
using MultiLink.Objects.Tiles;
using MultiLink.Worlds;

namespace MultiLink.Objects.Entities.Characters {
	/// <summary>
	/// A playable character.
	/// </summary>
	public class PlayerCharacter : CharacterObject {
		public static bool inMenu = false;

		/// <summary>
		/// Whether the character can be controlled.
		/// </summary>
		public bool CanControl = true;

		public int deathCount;
		private readonly SoundEffect Arrow;
		private readonly SoundEffect Chest;
		private readonly SoundEffect Die;

		/// <summary>
		/// The input used for the player
		/// </summary>
		private readonly InputMapping InputMap;

		/// <summary>
		/// Whether to lock the input.
		/// </summary>
		private bool lockInput;

		/// <summary>
		/// Whether the player is on ice.
		/// </summary>
		private bool onIce;

		private readonly SoundEffect Push;
		private readonly SoundEffect Sword;
		private readonly SoundEffect Throw;

		public bool Chatting = false;

		/// <summary>
		/// The bounding box of the Player.
		/// </summary>
		public override Rectangle BoundingBox => new Rectangle((int)(Position.X + BoundingBoxOffset.X - ParentRoom.Position.X), (int)(Position.Y + BoundingBoxOffset.Y - ParentRoom.Position.Y), (int)(Size.X + BoundingBoxSizeOffset.X), (int)(Size.Y + BoundingBoxSizeOffset.Y));

		public int DeathCount {
			get {
				return deathCount;
			}
			set {
				if (value != deathCount) {
					deathCount = value;

					MultiLink.MultiPlayer.SendSetDeathCount(this, DeathCount);
				}
			}
		}

		/// <summary>
		/// Creates a new PlayerCharacter.
		/// </summary>
		/// <param name="position">The position.</param>
		public PlayerCharacter(Vector2 position, InputMapping inputMap, SpriteMapping spriteMap) : base(position, spriteMap) {
			Layer = .6f;
			Size = new Vector2(40F, 40F);
			BoundingBoxOffset = new Vector2(4f, 21f);
			BoundingBoxSizeOffset = new Vector2(-12f, -20f);
			//BoundingBoxOffset = new Vector2(1.5f, 3f);
			//BoundingBoxSizeOffset = new Vector2(-3f, -3f);
			MovementSpeed = 3f;

			InputMap = inputMap;

			//respawnPosition = position; // TODO: Check if this works

			Sword = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Sword_Slash1");
			Chest = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Get_Item2");
			Die = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Link_Dying");
			Push = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Rock_Push");
			Throw = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Link_Throw");
			Arrow = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_BowArrow");

			PlayerCharacter.inMenu = false;
		}

		public override void OnDie() {
			DeathCount++;
			Die.Play();
			base.OnDie();
		}

		public override void Update() {
			Layer = .6f - 1 / (Position.Y + 10000);
			if ((invincibilityCounter % 100 < 50) && (invincibilityCounter >= 0)) {
				Visible = false;
			} else {
				Visible = true;
			}

			SpecialTileCheck();
			if (onIce) {
				IceMovement();
			} else if (!openChestAnimation) {
				lockInput = false;
			}

			if (!lockInput && CanControl && !inMenu) {
				Velocity = Vector2.Zero;

				if (!Chatting) {
					if (GameHandler.InputHandler.OnKeyHold(InputMap.UpKey)) {
						Velocity = new Vector2(Velocity.X, -MovementSpeed);
					} else if (GameHandler.InputHandler.OnKeyHold(InputMap.DownKey)) {
						Velocity = new Vector2(Velocity.X, MovementSpeed);
					}

					if (GameHandler.InputHandler.OnKeyHold(InputMap.LeftKey)) {
						Velocity = new Vector2(-MovementSpeed, Velocity.Y);
					} else if (GameHandler.InputHandler.OnKeyHold(InputMap.RightKey)) {
						Velocity = new Vector2(MovementSpeed, Velocity.Y);
					}
				}
			}

			if (CanControl) {
				ItemUseUpdate();
			}

			if (onIce) {
				if (Velocity.Y != 0 && Velocity.X != 0) {
					Velocity = new Vector2(Velocity.X, 0f);
				}
			}

			damageDictionary.Clear();
			base.Update();
			if (CanControl) {
				InteractUpdate();
			}
			SwordUseUpdate();

			useTimer -= GameHandler.InfoHandler.GameTime.ElapsedGameTime.Milliseconds;
			use = useTimer > 0;

			AnimationUpdate();

			// Turn around
			/*World world = Parent as World;
			if ((world.Player.Position - world.Player2.Position).Length() >= 1000f) {
				Position -= Velocity;

				// Bounce back on ice
				Velocity = -Velocity;
			}*/
		}

		public void ShootArrow() {
			ShootArrowNetworked();
			MultiLink.MultiPlayer.SendShootArrow(this);
		}

		public void ShootArrowNetworked() {
			Vector2 vel = Vector2.Zero;
			Vector2 off = Vector2.Zero;
			useTimer = 100f;
			switch (CurrentDirection) {
				case Direction.Left:
					vel = -Vector2.UnitX;
					off.X -= Size.X;
					Arrow.Play();
					break;
				case Direction.Right:
					vel = Vector2.UnitX;
					off.X += Size.X;
					Arrow.Play();
					break;
				case Direction.Up:
					vel = -Vector2.UnitY;
					off.Y -= Size.Y;
					Arrow.Play();
					break;
				case Direction.Down:
					vel = Vector2.UnitY;
					off.Y += Size.Y;
					Arrow.Play();
					break;
			}
			Vector2 position = Position - ParentRoom.Position + off;
			Vector2 direction = vel;
			float speed = 10f;
			Arrows--;
			ParentRoom.AddEntity(new ArrowEntity(position, direction, speed));
		}

		public void SwingSword() {
			SwingSwordNetworked();
			MultiLink.MultiPlayer.SendSwingSword(this);
		}

		public void SwingSwordNetworked() {
			useSword = true;
			useSwordTimer = 100f;
			Sword.Play();

			Rectangle r = Rectangle.Empty;
			switch (CurrentDirection) {
				case Direction.Left:
					r = new Rectangle((int)(PositionRelativeToRoom.X - Tile.UnitX), (int)PositionRelativeToRoom.Y, (int)Tile.UnitX, (int)Tile.UnitY);
					break;
				case Direction.Right:
					r = new Rectangle((int)(PositionRelativeToRoom.X + Tile.UnitX), (int)PositionRelativeToRoom.Y, (int)Tile.UnitX, (int)Tile.UnitY);
					break;
				case Direction.Up:
					r = new Rectangle((int)PositionRelativeToRoom.X, (int)(PositionRelativeToRoom.Y - Tile.UnitY), (int)Tile.UnitX, (int)Tile.UnitY);
					break;
				case Direction.Down:
					r = new Rectangle((int)PositionRelativeToRoom.X, (int)(PositionRelativeToRoom.Y + Tile.UnitY), (int)Tile.UnitX, (int)Tile.UnitY);
					break;
			}
			damageDictionary.Add(r, new DamagePair(this, swordDamage));
		}

		public void ThrowBomb() {
			ThrowBombNetworked();
			MultiLink.MultiPlayer.SendThrowBomb(this);
		}

		public void ThrowBombNetworked() {
			Vector2 vel = Vector2.Zero;
			Vector2 off = Vector2.Zero;
			useTimer = 100f;
			switch (CurrentDirection) {
				case Direction.Left:
					vel = -Vector2.UnitX;
					off.X -= Size.X;
					Throw.Play();
					break;
				case Direction.Right:
					vel = Vector2.UnitX;
					off.X += Size.X;
					Throw.Play();
					break;
				case Direction.Up:
					vel = -Vector2.UnitY;
					off.Y -= Size.Y;
					Throw.Play();
					break;
				case Direction.Down:
					vel = Vector2.UnitY;
					off.Y += Size.Y;
					Throw.Play();
					break;
			}
			Bombs--;
			Vector2 position = Position - ParentRoom.Position + off;
			float timer = 2000f;
			Vector2 velocity = vel * 15f;
			ParentRoom.AddEntity(new BombEntity(position, timer, velocity));
		}

		private void AnimationUpdate() {
			if (openChestAnimation) {
				lockInput = true;
				Sprite = SpriteMap.UseDownSprite;
				if (tempChestItem == null) {
					tempChestItem = new TextureGameObject(new Vector2(10, 50), new Vector2(30, 30)) {
						Sprite = openChestItemSprite,
						Layer = .9f
					};
					AddChild(tempChestItem);
				}

				openChestAnimationtimer -= GameHandler.InfoHandler.GameTime.ElapsedGameTime.Milliseconds;

				if (openChestAnimationtimer <= 0) {
					openChestAnimationtimer = -1f;
					lockInput = false;
					RemoveChild(tempChestItem);
					openChestAnimation = false;
					tempChestItem = null;
				}
			}
		}

		private void IceMovement() {
			lockInput = !Velocity.Equals(Vector2.Zero);
		}

		private void InteractUpdate() {
			if (GameHandler.InputHandler.OnKeyDown(InputMap.InteractKey) && !Chatting) {
				Interact();
			}
		}

		public void Interact() {
			InteractNetworked();
			MultiLink.MultiPlayer.SendInteract(this);
		}

		public void InteractNetworked() {
			if (Velocity.X == 0 && Velocity.Y == 0) {
				//Determine the area where there can be interacted with
				int interactionZoneSize = 4;
				Rectangle interactionZone = new Rectangle();
				switch (CurrentDirection) {
					case Direction.Left:
						interactionZone = new Rectangle((int)(PositionRelativeToRoom.X - interactionZoneSize), (int)PositionRelativeToRoom.Y + 10, interactionZoneSize, (int)Tile.UnitY - 20);
						break;
					case Direction.Right:
						interactionZone = new Rectangle((int)(PositionRelativeToRoom.X + Size.X + interactionZoneSize), (int)PositionRelativeToRoom.Y + 10, interactionZoneSize, (int)Tile.UnitY - 20);
						break;
					case Direction.Down:
						interactionZone = new Rectangle((int)PositionRelativeToRoom.X + 10, (int)(PositionRelativeToRoom.Y + Size.Y + interactionZoneSize), (int)Tile.UnitX - 20, interactionZoneSize);
						break;
					case Direction.Up:
						interactionZone = new Rectangle((int)PositionRelativeToRoom.X + 10, (int)(PositionRelativeToRoom.Y - interactionZoneSize), (int)Tile.UnitX - 20, interactionZoneSize);
						break;
				}

				foreach (ChestTile t in ParentRoom.Tiles.OfType<ChestTile>().Where(t => t.BoundingBox.Intersects(interactionZone)).Where(t => t.IsClosed)) {
					openChestItemSprite = t.OnOpen(this);
					openChestAnimation = true;
					openChestAnimationtimer = 500f;
					lockInput = true;
					Chest.Play();
				}

				// Only actually process rock interactions on host
				if (MultiLink.MultiPlayer.IsHost) {
					foreach (Entity e in ParentRoom.Entities) {
						if (e is PushableBlockEntity) {
							if (e.BoundingBox.Intersects(interactionZone)) {
								PushableBlockEntity.Directions d = PushableBlockEntity.Directions.None;
								switch (CurrentDirection) {
									case Direction.Left:
										d = PushableBlockEntity.Directions.Left;
										Push.Play();
										break;
									case Direction.Right:
										d = PushableBlockEntity.Directions.Right;
										Push.Play();
										break;
									case Direction.Up:
										d = PushableBlockEntity.Directions.Up;
										Push.Play();
										break;
									case Direction.Down:
										d = PushableBlockEntity.Directions.Down;
										Push.Play();
										break;
								}

								(e as PushableBlockEntity).PushDirection(d);
							}
						}
					}
				}
			}
		}

		private void ItemUseUpdate() {
			//Arrows
			if (GameHandler.InputHandler.OnKeyDown(InputMap.ArrowKey) && Arrows > 0 && !Chatting) {
				Rectangle ab = new Rectangle();
				switch (CurrentDirection) {
					case Direction.Left:
						ab = new Rectangle((int)(PositionRelativeToRoom.X - Tile.UnitX), (int)PositionRelativeToRoom.Y, (int)Tile.UnitX, (int)Tile.UnitY);
						break;
					case Direction.Right:
						ab = new Rectangle((int)(PositionRelativeToRoom.X + Tile.UnitX), (int)PositionRelativeToRoom.Y, (int)Tile.UnitX, (int)Tile.UnitY);
						break;
					case Direction.Up:
						ab = new Rectangle((int)PositionRelativeToRoom.X, (int)(PositionRelativeToRoom.Y - Tile.UnitY), (int)Tile.UnitX, (int)Tile.UnitY);
						break;
					case Direction.Down:
						ab = new Rectangle((int)PositionRelativeToRoom.X, (int)(PositionRelativeToRoom.Y + Tile.UnitY), (int)Tile.UnitX, (int)Tile.UnitY);
						break;
				}

				bool canPlace = ParentRoom.CollisionList.All(r => !ab.Intersects(r));
				if (canPlace) {
					ShootArrow();
				}
			}

			// Bombs
			if (GameHandler.InputHandler.OnKeyDown(InputMap.BombKey) && Bombs > 0 && !Chatting) {
				ThrowBomb();
			}
		}

		private void SpecialTileCheck() {
			Rectangle bb = BoundingBox;
			onIce = ParentRoom.SpecialFloors.Where(t => bb.Intersects(t.BoundingBox)).ToList().Count > 0;
		}

		private void SwordUseUpdate() {
			if (GameHandler.InputHandler.OnKeyDown(InputMap.SwordKey) && CanControl && !Chatting && !useSword) {
				SwingSword();
			}
			if (useSwordTimer > 0f) {
				useSwordTimer -= GameHandler.InfoHandler.GameTime.ElapsedGameTime.Milliseconds;
			}
			if (useSwordTimer <= 0f) {
				useSwordTimer = -1f;
				useSword = false;
			}
		}

		#region Inventory

		/// <summary>
		/// The ammount of keys the player currently has
		/// </summary>
		public int Key {
			get;
			private set;
		}

		/// <summary>
		/// The ammount of arrows the player curently has
		/// </summary>
		public int Arrows {
			get;
			private set;
		} = 10;

		/// <summary>
		/// The ammount of bombs the player currently has
		/// </summary>
		public int Bombs {
			get;
			private set;
		} = 5;

		#endregion

		#region Animation

		private bool openChestAnimation;
		private Sprite openChestItemSprite;
		private float openChestAnimationtimer = -1;
		private TextureGameObject tempChestItem;

		#endregion

		#region Inventory Adding

		/// <summary>
		/// Add keys to players inventory
		/// </summary>
		/// <param name="a">ammount of keys</param>
		public void AddKeys(int a = 1) {
			Key += a;
		}

		/// <summary>
		/// Add arrows to players inventory
		/// </summary>
		/// <param name="a">ammount of arrows</param>
		public void AddArrows(int a = 1) {
			Arrows += a;
		}

		/// <summary>
		/// Add bombs to players inventory
		/// </summary>
		/// <param name="a">ammount of bombs</param>
		public void AddBombs(int a = 1) {
			Bombs += a;
		}

		#endregion
	}
}