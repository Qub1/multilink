﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Entities {
	internal class ProjectileEntity : Entity {
		private const int damage = 2;
		private readonly Dictionary<Rectangle, DamagePair> damageDictionary = new Dictionary<Rectangle, DamagePair>();
		private Vector2 lastPosition = Vector2.Zero;
		private int stoppedCount;

		public ProjectileEntity(Vector2 position, Vector2 direction, float speed) : base(position, Tile.UnitSize) {
			Velocity = direction * speed;
			MovementSpeed = Velocity.Length();
			Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("4x1@Projectile"));
			isSolid = false;
			Sprite.CurrentIndex = new Point(0, 0);
			GraphicsUpdate();
			networkedPosition = false;
		}

		public override Dictionary<Rectangle, DamagePair> GetDamageRectangles() {
			return damageDictionary;
		}

		public override void Update() {
			if (!new Rectangle(0, 0, (int)(ParentRoom.Size.X * Tile.UnitX), (int)(ParentRoom.Size.Y * Tile.UnitY)).Intersects(BoundingBox)) {
				ParentRoom.RemoveEntity(this);
			}

			if (Velocity.Equals(Vector2.Zero)) {
				ParentRoom.RemoveEntity(this);
			}
			base.Update();
			if (lastPosition == Position) {
				stoppedCount++;
			} else {
				stoppedCount = 0;
			}
			lastPosition = Position;
			if (stoppedCount > 10) {
				ParentRoom.RemoveEntity(this);
			}
			damageDictionary.Clear();
			Rectangle bb = BoundingBox;
			bb.Inflate(5, 5);
			damageDictionary.Add(bb, new DamagePair(this, damage));
		}

		private void GraphicsUpdate() {
			if (Velocity.X < 0) {
				Sprite.CurrentIndex = new Point(3, 0);
				return;
			}
			if (Velocity.X > 0) {
				Sprite.CurrentIndex = new Point(1, 0);
				return;
			}
			if (Velocity.Y < 0) {
				Sprite.CurrentIndex = new Point(0, 0);
				return;
			}
			if (Velocity.Y > 0) {
				Sprite.CurrentIndex = new Point(2, 0);
			}
		}
	}
}