﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Entities.Enemies {
	internal class RopeEntity : Enemy {
		private const int damage = 2;
		private const int ropeHealth = 2;
		private State state = State.Inactive;
		private float timer;
		private Vector2 velocity = Vector2.Zero;

		/// <summary>
		/// Creates a new RopeEntity.
		/// </summary>
		public RopeEntity(Vector2 position) : base(position, Tile.UnitSize, ropeHealth) {
			Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("4x1@Rope")) {
				Animated = true,
				AnimationStartIndex = new Point(0, 0),
				AnimationFrameTime = TimeSpan.FromMilliseconds(200),
				AnimationLength = 2
			};
			isSolid = true;
			timer = 0;
		}

		public override Dictionary<Rectangle, DamagePair> GetDamageRectangles() {
			Dictionary<Rectangle, DamagePair> dic = new Dictionary<Rectangle, DamagePair>();
			Rectangle bb = BoundingBox;
			bb.Inflate(3, 3);
			dic.Add(bb, new DamagePair(this, damage));
			return dic;
		}

		public override void Update() {
			Velocity = Vector2.Zero;
			if (!TryMove(velocity)) {
				if (!TryMove(new Vector2(velocity.X, 0))) {
					TryMove(new Vector2(0, velocity.Y));
				}
			}

			if (timer > 0) {
				timer -= (float)GameHandler.InfoHandler.GameTime.ElapsedGameTime.TotalMilliseconds;
			} else if ((timer <= 0 || Velocity == Vector2.Zero) && state == State.Inactive) {
				RandomMovement();
			}
			CheckPlayerInRangeAndMove();

			//Update sprite
			if (Velocity.X < 0) {
				Sprite.AnimationStartIndex = new Point(0, 0);
			} else if (Velocity.X > 0) {
				Sprite.AnimationStartIndex = new Point(2, 0);
			}

			base.Update();
		}

		/// <summary>
		/// Check whether player is in range
		/// </summary>
		private void CheckPlayerInRangeAndMove() {
			List<PlayerCharacter> list = new List<PlayerCharacter>();
			foreach (Entity entity in ParentRoom.EntitiesInclPlayer) {
				if (entity is PlayerCharacter) {
					list.Add((PlayerCharacter)entity);
				}
			}

			double prevDist = 200;

			if (list.Count == 0) {
				state = State.Inactive;
				return;
			}

			foreach (PlayerCharacter player in list) {
				double distsq = Math.Sqrt(Math.Pow(Math.Abs(player.PositionRelativeToRoom.X - Position.X), 2) + Math.Pow(Math.Abs(player.PositionRelativeToRoom.Y - Position.Y), 2));
				if (distsq < 200 && distsq < prevDist) {
					prevDist = distsq;
					Vector2 dir = player.PositionRelativeToRoom - Position;
					dir.Normalize();
					dir *= 2.5f;
					velocity = dir;
					state = State.Aggressive;
				}
			}
		}

		/// <summary>
		/// Move at random when player is not in sight
		/// </summary>
		private void RandomMovement() {
			switch (GameHandler.InfoHandler.Random.Next(0, 4)) {
				case 0:
					velocity = new Vector2(0, 1);
					break;
				case 1:
					velocity = new Vector2(0, -1);
					break;
				case 2:
					velocity = new Vector2(-1, 0);
					break;
				case 3:
					velocity = new Vector2(1, 0);
					break;
			}
			timer = GameHandler.InfoHandler.Random.Next(800, 2400);
		}

		private bool TryMove(Vector2 velo) {
			Rectangle bb = BoundingBox;
			bb.Inflate((int)Math.Abs(velo.X), (int)Math.Abs(velo.Y));
			foreach (Rectangle r in ParentRoom.EmptyList.Where(r => bb.Intersects(r))) {
				return false;
			}
			Velocity = velo;
			return true;
		}

		/// <summary>
		/// Possible states for this entity
		/// </summary>
		private enum State {
			Inactive,
			Aggressive
		}
	}
}