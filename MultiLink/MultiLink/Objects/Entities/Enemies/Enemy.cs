﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Items.DropTable;

namespace MultiLink.Objects.Entities.Enemies {
	public class Enemy : LivingEntity {
		private float deathAnimationTimer = -1f;
		private readonly SoundEffect Die;

		protected Enemy(Vector2 position, Vector2 size, int maxHealth) : base(position, size, maxHealth) {
			Die = GameHandler.AssetHandler.GetSoundEffect("Sounds/LA_Enemy_Die");
			CurrentDropTable = DropTable.Standard;
		}

		public override void OnDie() {
			Velocity = Vector2.Zero;
			if (deathAnimationTimer == -1f) {
				deathAnimationTimer += 600f;
				Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("monster_die"));
				Sprite.AnimationLength = 4;
				Sprite.AnimationStartIndex = Point.Zero;
				Sprite.AnimationFrameTime = TimeSpan.FromMilliseconds(500f / 4f);
				Sprite.Animated = true;
				Die.Play();
			}
		}

		public override void Update() {
			if ((invincibilityCounter % 100 < 50) && (invincibilityCounter >= 0)) {
				Visible = false;
			} else {
				Visible = true;
			}

			if (deathAnimationTimer > 0f) {
				deathAnimationTimer -= GameHandler.InfoHandler.GameTime.ElapsedGameTime.Milliseconds;
				if (deathAnimationTimer <= 0f) {
					base.OnDie();
				}
			}
			base.Update();
		}
	}
}