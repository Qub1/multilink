﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Entities.Enemies;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Entities {
	internal class ArmosEntity : Enemy {
		private const int armosHealth = 6;
		private const int damage = 3;
		private State state = State.Inactive;

		/// <summary>
		/// Creates a new ArmosEntity.
		/// </summary>
		public ArmosEntity(Vector2 position) : base(position, Tile.UnitSize, armosHealth) {
			Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("2x1@Armos"));
			Sprite.Animated = true;
			Sprite.AnimationStartIndex = new Point(0, 0);
			Sprite.AnimationFrameTime = TimeSpan.FromMilliseconds(200);
			Sprite.AnimationLength = 1;
			isSolid = true;
		}

		public override Dictionary<Rectangle, DamagePair> GetDamageRectangles() {
			Dictionary<Rectangle, DamagePair> dic = new Dictionary<Rectangle, DamagePair>();
			Rectangle bb = BoundingBox;
			bb.Inflate(3, 3);
			dic.Add(bb, new DamagePair(this, damage));
			return dic;
		}

		public override void Update() {
			CheckPlayerInRangeAndMove();
			base.Update();

			if (state == State.Inactive) {
				Sprite.AnimationStartIndex = new Point(0, 0);
				Sprite.CurrentIndex = new Point(0, 0);
			} else {
				Sprite.AnimationStartIndex = new Point(1, 0);
				Sprite.CurrentIndex = new Point(1, 0);
			}
		}

		/// <summary>
		/// Check whether player is in range
		/// </summary>
		private void CheckPlayerInRangeAndMove() {
			List<PlayerCharacter> list = new List<PlayerCharacter>();
			foreach (Entity entity in ParentRoom.EntitiesInclPlayer) {
				if (entity is PlayerCharacter) {
					list.Add((PlayerCharacter)entity);
				}
			}

			double prevDist = 200;

			if (list.Count == 0) {
				state = State.Inactive;
				return;
			}

			foreach (PlayerCharacter player in list) {
				double distsq = Math.Sqrt(Math.Pow(Math.Abs(player.PositionRelativeToRoom.X - Position.X), 2) + Math.Pow(Math.Abs(player.PositionRelativeToRoom.Y - Position.Y), 2));
				if (distsq < 200 && distsq < prevDist) {
					prevDist = distsq;
					if (distsq < 140 && state == State.Aggressive) {
						Vector2 dir = player.PositionRelativeToRoom - Position;
						dir.Normalize();
						dir *= 2;
						Velocity = dir;
						return;
					}
					if (state == State.Inactive && distsq < 90) {
						state = State.Aggressive;
						return;
					}
					state = State.Inactive;
					Velocity = Vector2.Zero;
				}
			}
		}

		/// <summary>
		/// Possible states for this entity
		/// </summary>
		private enum State {
			Inactive,
			Aggressive
		}
	}
}