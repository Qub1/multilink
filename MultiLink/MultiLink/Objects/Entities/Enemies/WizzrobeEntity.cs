﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Entities.Enemies;
using MultiLink.Objects.Rooms;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Entities {
	public class WizzrobeEntity : Enemy {
		private const int damage = 0;
		private const int wizzHealth = 1;
		private bool inRoom;
		private Vector2 startPosition;
		private Vector2 direction;
		private float timer;

		/// <summary>
		/// Creates a new WizzrobeEntity
		/// </summary>
		public WizzrobeEntity(Vector2 position) : base(position, Tile.UnitSize, wizzHealth) {
			Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("4x1@Wizzrobe"));
			Sprite.Animated = true;
			Sprite.AnimationLength = 1;
			Sprite.AnimationStartIndex = new Point(0, 0);
			Sprite.CurrentIndex = Sprite.AnimationStartIndex;
			isSolid = true;
			startPosition = position;
			direction = Vector2.UnitY;
			timer = 0;
		}

		public override Dictionary<Rectangle, DamagePair> GetDamageRectangles() {
			Dictionary<Rectangle, DamagePair> dic = new Dictionary<Rectangle, DamagePair>();
			Rectangle bb = BoundingBox;
			bb.Inflate(3, 3);
			dic.Add(bb, new DamagePair(this, damage));
			return dic;
		}

		public override void Update() {
			timer += (float)GameHandler.InfoHandler.GameTime.ElapsedGameTime.TotalMilliseconds / 17; // 1000 ms / 50 fps = ~17 ms/frame
			if (inRoom && timer > 120) {
				// create projectile
				ShootProjectile();
				timer = 0;
			}
			SetDirectionAndSprite();

			base.Update();
		}

		public void ShootProjectile() {
			ShootProjectileNetworked();
			MultiLink.MultiPlayer.SendShootProjectile(this);
		}

		public void ShootProjectileNetworked() {
			//fired = true;
			ParentRoom.AddEntity(new ProjectileEntity(Position + Tile.UnitSize * direction * 1.5f, direction, 10f));
		}

		/// <summary>
		/// Set the CurrentDirection as close as possible to the player position
		/// </summary>
		private void SetDirectionAndSprite() {
			Room parentRoom = Parent as Room;
			if (parentRoom == null) {
				return;
			}
			inRoom = false;

			List<PlayerCharacter> list = new List<PlayerCharacter>();
			foreach (Entity entity in ParentRoom.EntitiesInclPlayer) {
				if (entity is PlayerCharacter) {
					list.Add((PlayerCharacter)entity);
					inRoom = true;
				}
			}

			double prevDist = 400;

			foreach (PlayerCharacter player in list) {
				double distsq = Math.Sqrt(Math.Pow(Math.Abs(player.PositionRelativeToRoom.X - Position.X), 2) + Math.Pow(Math.Abs(player.PositionRelativeToRoom.Y - Position.Y), 2));
				if (distsq < 400 && distsq < prevDist) {
					prevDist = distsq;
					Vector2 dir = player.PositionRelativeToRoom - Position;
					dir.Normalize();
					if (Math.Abs(dir.X) < Math.Abs(dir.Y)) {
						// Attack in y CurrentDirection
						direction = new Vector2(0, dir.Y);
					} else {
						// Attack in x CurrentDirection
						direction = new Vector2(dir.X, 0);
					}
					if (direction.X > 0) {
						Sprite.AnimationStartIndex = new Point(3, 0);
					} else if (direction.X < 0) {
						Sprite.AnimationStartIndex = new Point(1, 0);
					} else if (direction.Y < 0) {
						Sprite.AnimationStartIndex = new Point(2, 0);
					} else {
						Sprite.AnimationStartIndex = new Point(0, 0);
					}
					Sprite.CurrentIndex = Sprite.AnimationStartIndex;
				}
			}
		}
	}
}