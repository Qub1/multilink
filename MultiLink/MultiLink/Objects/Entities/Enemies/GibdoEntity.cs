﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Entities.Enemies {
	internal class GibdoEntity : Enemy {
		private const int damage = 2;
		private const int gibdoHealth = 2;
		private Vector2 direction; // TODO: Use variable
		private Vector2 startPosition; // TODO: Use variable
		private State state = State.Inactive;
		private float timer;

		/// <summary>
		/// Creates a new GibdoEntity
		/// </summary>
		public GibdoEntity(Vector2 position) : base(position, Tile.UnitSize, gibdoHealth) {
			Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("2x1@Gibdo")) {
				Animated = true,
				AnimationStartIndex = new Point(0, 0),
				AnimationFrameTime = TimeSpan.FromMilliseconds(200),
				AnimationLength = 2
			};
			isSolid = true;
			startPosition = position;
			direction = Vector2.Zero;
			timer = 0;
		}

		public override Dictionary<Rectangle, DamagePair> GetDamageRectangles() {
			Dictionary<Rectangle, DamagePair> dic = new Dictionary<Rectangle, DamagePair>();
			Rectangle bb = BoundingBox;
			bb.Inflate(3, 3);
			dic.Add(bb, new DamagePair(this, damage));
			return dic;
		}

		public override void Update() {
			if (timer > 0) {
				timer -= (float)GameHandler.InfoHandler.GameTime.ElapsedGameTime.TotalMilliseconds;
			} else if ((timer <= 0 || Velocity == Vector2.Zero) && state == State.Inactive) {
				RandomMovement();
			}
			CheckPlayerInRangeAndMove();
			base.Update();
		}

		/// <summary>
		/// Checks whether the player is in range
		/// </summary>
		private void CheckPlayerInRangeAndMove() {
			List<PlayerCharacter> list = new List<PlayerCharacter>();
			foreach (Entity entity in ParentRoom.EntitiesInclPlayer) {
				if (entity is PlayerCharacter) {
					list.Add((PlayerCharacter)entity);
				}
			}

			double prevDist = 200;

			if (list.Count == 0) {
				state = State.Inactive;
				return;
			}

			foreach (PlayerCharacter player in list) {
				double distsq = Math.Sqrt(Math.Pow(Math.Abs(player.PositionRelativeToRoom.X - Position.X), 2) + Math.Pow(Math.Abs(player.PositionRelativeToRoom.Y - Position.Y), 2));
				if (distsq < 200 && distsq < prevDist) {
					prevDist = distsq;
					Vector2 dir = player.PositionRelativeToRoom - Position;
					dir.Normalize();
					dir *= 1.5f;
					Velocity = dir;
					state = State.Aggressive;
				}
			}
		}

		/// <summary>
		/// Move randomly
		/// </summary>
		private void RandomMovement() {
			switch (GameHandler.InfoHandler.Random.Next(0, 4)) {
				case 0:
					Velocity = new Vector2(0, 1);
					break;
				case 1:
					Velocity = new Vector2(0, -1);
					break;
				case 2:
					Velocity = new Vector2(-1, 0);
					break;
				case 3:
					Velocity = new Vector2(1, 0);
					break;
			}
			timer = GameHandler.InfoHandler.Random.Next(600, 3000);
		}

		/// <summary>
		/// Possible states for this entity
		/// </summary>
		private enum State {
			Inactive,
			Aggressive
		}
	}
}