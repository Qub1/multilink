﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Entities.Enemies;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Entities {
	internal class KeeseEntity : Enemy {
		private const int damage = 1;
		private const int keeseHealth = 1;
		private Vector2 modVelocity = new Vector2(0.02f, -0.02f);
		private State state = State.Idle;

		/// <summary>
		/// Creates a new KeeseEntity
		/// </summary>
		public KeeseEntity(Vector2 position) : base(position, Tile.UnitSize, keeseHealth) {
			Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("2x1@Keese"));
			Sprite.Animated = true;
			Sprite.AnimationStartIndex = new Point(0, 0);
			Sprite.AnimationFrameTime = TimeSpan.FromMilliseconds(100);
			Sprite.AnimationLength = 2;
			isSolid = true;
		}

		public override Dictionary<Rectangle, DamagePair> GetDamageRectangles() {
			Dictionary<Rectangle, DamagePair> dic = new Dictionary<Rectangle, DamagePair>();
			Rectangle bb = BoundingBox;
			bb.Inflate(3, 3);
			dic.Add(bb, new DamagePair(this, damage));
			return dic;
		}

		public override void Update() {
			UpdateSprite();
			if (GameHandler.InfoHandler.Random.Next(0, 1000) < 3) {
				state = State.Idle;
				Velocity = Vector2.Zero;
			}
			base.Update();
		}

		/// <summary>
		/// Checks whether the player is in range
		/// </summary>
		private void CheckPlayerInRange() {
			foreach (Entity entity in ParentRoom.EntitiesInclPlayer) {
				if (entity is PlayerCharacter) {
					PlayerCharacter player = entity as PlayerCharacter;
					double distsq = Math.Sqrt(Math.Pow(Math.Abs(player.PositionRelativeToRoom.X - Position.X), 2) + Math.Pow(Math.Abs(player.PositionRelativeToRoom.Y - Position.Y), 2));
					if (distsq < 200) {
						state = State.Aggressive;
						Velocity = new Vector2(GameHandler.InfoHandler.Random.Next(0, 40) / 10 - 2, GameHandler.InfoHandler.Random.Next(0, 40) / 10 - 2);
						return;
					}
				}
			}
			state = State.Idle;
		}

		/// <summary>
		/// Move towards the player
		/// </summary>
		private void CheckPlayerInRangeAndMove() {
			foreach (Entity entity in ParentRoom.EntitiesInclPlayer) {
				if (entity is PlayerCharacter) {
					PlayerCharacter player = entity as PlayerCharacter;
					double distsq = Math.Sqrt(Math.Pow(Math.Abs(player.PositionRelativeToRoom.X - Position.X), 2) + Math.Pow(Math.Abs(player.PositionRelativeToRoom.Y - Position.Y), 2));
					if (distsq < 200) {
						Vector2 dir = player.PositionRelativeToRoom - Position;
						dir.Normalize();
						dir *= 1.5f;
						Velocity = dir;
						state = State.Aggressive;
						return;
					}
					state = State.Idle;
					return;
				}
			}
			state = State.Idle;
		}

		private void UpdateSprite() {
			if (state == State.Aggressive) {
				Sprite.AnimationLength = 2;
				Sprite.AnimationStartIndex = new Point(0, 0);
				CheckPlayerInRangeAndMove();
			} else {
				Sprite.AnimationLength = 1;
				Sprite.AnimationStartIndex = new Point(1, 0);
				CheckPlayerInRange();
			}
		}

		/// <summary>
		/// Possible states for this entity
		/// </summary>
		private enum State {
			Idle,
			Aggressive
		}
	}
}