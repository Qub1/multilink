﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MultiLink.Objects.Items.DropTable {
	public class DropTable {
		/// <summary>
		/// Chest droptable
		/// </summary>
		public static DropTable Chest {
			get {
				DropTable d = new DropTable();
				d.AddItem(new Item(Item.ItemTypes.Arrow, 5), 1);
				d.AddItem(new Item(Item.ItemTypes.Bomb, 5), 1);
				d.AddItem(new Item(Item.ItemTypes.Halfheart, 5), 1);
				return d;
			}
		}

		/// <summary>
		/// A Hearts only droptable
		/// </summary>
		public static DropTable HeartsOnly {
			get {
				DropTable d = new DropTable();
				d.AddItem(new Item(Item.ItemTypes.Halfheart), 1);
				d.AddItem(new Item(Item.ItemTypes.Heart), 1);
				return d;
			}
		}

		/// <summary>
		/// Standard DropTable
		/// </summary>
		public static DropTable Standard {
			get {
				DropTable d = new DropTable();
				d.AddItem(new Item(Item.ItemTypes.Arrow, 5), 2);
				d.AddItem(new Item(Item.ItemTypes.Bomb, 3), 1);
				d.AddItem(new Item(Item.ItemTypes.Halfheart, 1), 2);
				d.AddItem(new Item(Item.ItemTypes.Heart, 1), 1);
				return d;
			}
		}

		public Dictionary<Item, int> Table {
			get;
		} = new Dictionary<Item, int>();

		private int changeSum => Table.Sum(p => p.Value); // TODO: Use variable

		private List<Item> tableList {
			get {
				List<Item> l = new List<Item>();
				foreach (KeyValuePair<Item, int> p in Table) {
					for (int i = 0; i < p.Value; i++) {
						l.Add(p.Key);
					}
				}
				return l;
			}
		}

		/// <summary>
		/// Add in item to the DropTable
		/// </summary>
		/// <param name="item">item to add</param>
		/// <param name="change">change of the item to drop (higher the number, more change)</param>
		public void AddItem(Item item, int change) {
			Table.Add(item, change);
		}

		public Item GetRandomItem() {
			List<Item> list = tableList;
			Random r = new Random();
			int i = r.Next(list.Count);
			return list[i];
		}
	}
}