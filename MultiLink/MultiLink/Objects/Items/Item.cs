﻿using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities;
using MultiLink.Objects.Entities.Characters;

namespace MultiLink.Objects.Items {
	public class Item {
		public enum ItemTypes {
			Halfheart,
			Heart,
			Key,
			Arrow,
			Bomb
		}

		public int Amount;

		public Sprite ItemSprite {
			get {
				switch (ItemType) {
					case ItemTypes.Halfheart:
						return new Sprite(GameHandler.AssetHandler.GetTexture("halfheart"));
					case ItemTypes.Heart:
						return new Sprite(GameHandler.AssetHandler.GetTexture("heart"));
					case ItemTypes.Key:
						return new Sprite(GameHandler.AssetHandler.GetTexture("key"));
					case ItemTypes.Bomb:
						return new Sprite(GameHandler.AssetHandler.GetTexture("bomb"));
					case ItemTypes.Arrow:
						return new Sprite(GameHandler.AssetHandler.GetTexture("arrow"));
					default:
						return null;
				}
			}
		}

		public ItemTypes ItemType {
			get;
		}

		public Item(ItemTypes itemType, int amount = 1) {
			ItemType = itemType;
			Amount = amount;
		}

		public PickUpEntity AsPickUpEntity(Vector2 position, int activeTime) {
			return new PickUpEntity(position, this, activeTime);
		}

		public void OnPickUp(PlayerCharacter e) {
			switch (ItemType) {
				case ItemTypes.Halfheart:
					e.Heal(1 * Amount);
					break;
				case ItemTypes.Heart:
					e.Heal(2 * Amount);
					break;
				case ItemTypes.Key:
					e.AddKeys(Amount);
					break;
				case ItemTypes.Arrow:
					e.AddArrows(Amount);
					break;
				case ItemTypes.Bomb:
					e.AddBombs(Amount);
					break;
			}
		}
	}
}