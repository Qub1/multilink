﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MultiLibrary.Handlers;
using MultiLibrary.Types;
using MultiLink.Objects.Entities;
using MultiLink.Objects.Entities.Enemies;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Rooms {
	partial class Room {
		private Vector2 ss;

		private Dictionary<int, string> IntAssetDictionary {
			get {
				Dictionary<int, string> dic = new Dictionary<int, string> {
					{
						0, "floor"
					}, {
						1, "floor"
					}, {
						2, "floor"
					}, {
						3, "crackedfloor"
					}, {
						4, "floor2"
					}, {
						5, "ice"
					}, {
						6, "block"
					}, {
						7, "movableblock"
					}, {
						8, "grass"
					}, {
						9, "ravine"
					}, {
						10, "wall"
					}, {
						11, "chest"
					}, {
						12, "keychest"
					}, {
						13, "ppcircle"
					}, {
						14, "ppsquare"
					}, {
						15, "pptriangle"
					}, {
						16, "wallsheet"
					}, {
						17, "rpsquare"
					}, {
						18, "rpcircle"
					}, {
						19, "rptriangle"
					}, {
						20, "stairs"
					}, {
						21, "tile_1"
					}, {
						22, "tile_2"
					}, {
						23, "tile_3"
					}, {
						24, "tile_5"
					}, {
						25, "tilesheet2"
					}, {
						26, "tile_6"
					}, {
						27, "tile_7"
					}, {
						28, "water_1"
					}, {
						29, "water_2"
					}, {
						30, "tile_8"
					}, {
						31, "lamp_dark"
					}, {
						32, "lamp_light"
					}, {
						33, "ravineladder"
					}, {
						34, "tile_9"
					}, {
						35, "sand"
					}, {
						36, "keydoor"
					}, {
						37, "wall2"
					}, {
						38, "tile_10"
					}, {
						39, "tile_11"
					}, {
						40, "tile_12"
					}, {
						41, "tile_13"
					}, {
						42, "endstairs"
					}
				};
				return dic;
			}
		}

		public void LoadFromFile(string path, bool start = false, Entity player = null) {
			if (!File.Exists(path)) {
				return;
			}

			//try {
				GameHandler.InfoHandler.Output("Loading room file \"" + path + "\"...");
				filePath = path;
				rotations = 0;

				StreamReader reader = new StreamReader(path);
				string line;
				string lastline = "";
				int i = 0;
				int y = 0;

				Dictionary<int, string> dictionary = IntAssetDictionary;

				Texture2D texture = GameHandler.AssetHandler.GetTexture("tilesheet");

				while ((line = reader.ReadLine()) != null) {
					if (i == 0) {
						Size = StringToVector2(line);
						roomSize = new Vector2(Size.X, Size.Y);
						Size.X = Size.X * UnitSize.X;
						Size.Y = Size.Y * UnitSize.Y;
						ss = new Vector2(Size.X, Size.Y);
						Tiles = new Tile[(int)Size.X, (int)Size.Y];
					} else if (i == 1) {
						if (line == "") {
							continue;
						}
						string[] spli = line.Split(',');
						EntrancePosition = new Point(int.Parse(spli[0]), int.Parse(spli[1]));
						EntranceOrientation = spli[2];
					} else if (i == 2) {
						if (line == "") {
							continue;
						}
						string[] spli = line.Split(',');
						ExitPosition = new Point(int.Parse(spli[0]), int.Parse(spli[1]));
						ExitOrientation = spli[2];
					} else if (i < Size.Y + 3) {
						var x = 0;
						string[] split = line.Split(',');
						for (int q = 0; q < Size.X; q++) {
							string str = split[q];
							if (str == "") {
								continue;
							}
							string[] s = str.Split(' ');
							AddTileFromInts(new Point(x, y), int.Parse(s[0]), int.Parse(s[1]), dictionary, texture);
							x++;
						}
						y++;
					} else {
						if (lastline != line) {
							string[] split = line.Split(' ');
							AddEntityFromInts(new Point(int.Parse(split[1]), int.Parse(split[2])), int.Parse(split[0]));
						}
					}

					lastline = line;
					i++;
				}

				if (start) {
					if (player != null) {
						player.Position = new Vector2(EntrancePosition.X, EntrancePosition.Y);
					}
				}

				Blackout();

				if (OOBPrevention) {
					CalculateOobPrevention();
				}

				redrawStaticRender = true;
				GameHandler.InfoHandler.Output("Loaded room file \"" + path + "\"!");
			//} catch (Exception) {
//				throw;
			//}
		}

		public void Reload() {
			string p = filePath;
			int r = rotations;

			LinkValues[0] = false;
			LinkValues[1] = false;
			LinkValues[2] = false;

			foreach (Tile t in Tiles) {
				if (t != null) {
					t.Reload();
				}
			}

			List<Entity> remove = new List<Entity>();
			foreach (Entity e in entities) {
				remove.Add(e);
			}
			foreach (Entity e in remove) {
				RemoveEntity(e);
			}

			ReloadEntities();
			RerotateEntities(r, ss);
		}

		public void Rotate(int i) {
			//Old Tiles
			Tile[,] oldTiles = Tiles;
			//Rotate size
			float s = Size.X;
			Size.X = Size.Y;
			Size.Y = s;

			s = roomSize.X;
			roomSize.X = roomSize.Y;
			roomSize.Y = s;

			int[,] rotmov = new int[16, 16] {
				{
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				}, {
					1,
					1,
					1,
					-3,
					1,
					2,
					-2,
					-1,
					1,
					1,
					1,
					-3,
					1,
					-1,
					0,
					0
				}, {
					1,
					1,
					1,
					-3,
					1,
					1,
					1,
					-3,
					1,
					1,
					1,
					-3,
					0,
					0,
					0,
					0
				}, {
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				}, {
					1,
					1,
					1,
					-3,
					1,
					1,
					1,
					-3,
					1,
					1,
					1,
					-3,
					0,
					0,
					0,
					0
				}, {
					0,
					0,
					0,
					0,
					0,
					0,
					1,
					-1,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				}, {
					1,
					1,
					1,
					-3,
					1,
					1,
					1,
					-3,
					1,
					1,
					1,
					-3,
					0,
					0,
					0,
					0
				}, {
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				}, {
					3,
					1,
					-2,
					-2,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				}, {
					2,
					-1,
					1,
					-2,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				}, {
					1,
					-1,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				}, {
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				}, {
					1,
					1,
					1,
					-3,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				}, {
					1,
					1,
					1,
					-3,
					1,
					1,
					1,
					-3,
					1,
					1,
					1,
					-3,
					0,
					0,
					0,
					0
				}, {
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				}, {
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0
				}
			};

			//Switch x and y, and object positions
			Tiles = new Tile[(int)Size.X, (int)Size.Y];
			for (int x = 0; x < oldTiles.GetLength(0); x++) {
				for (int y = 0; y < oldTiles.GetLength(1); y++) {
					if (oldTiles[x, y] == null) {
						continue;
					}
					Point np = new Point(x, y);
					//Transpose
					np = new Point(np.Y, np.X);
					//Reverse collumns
					np.X = Tiles.GetLength(0) - np.X - 1;
					Tiles[np.X, np.Y] = oldTiles[x, y];
					Tiles[np.X, np.Y].Rotate(1);
					Tiles[np.X, np.Y].Position = new Vector2(np.X * Tile.UnitX, np.Y * Tile.UnitY);

					Point ci = Tiles[np.X, np.Y].Sprite.CurrentIndex;
					if (ci.X < 0 || ci.X > 16 || ci.Y < 0 || ci.Y > 16) {
						continue;
					}
					Tiles[np.X, np.Y].Sprite.CurrentIndex = new Point(ci.X + rotmov[ci.Y, ci.X], ci.Y);
				}
			}
			//Rotate entities
			foreach (Entity e in Entities) {
				e.Position = new Vector2(Size.X * Tile.UnitX - e.Position.Y - Tile.UnitX, e.Position.X);
			}

			if (OOBPrevention) {
				CalculateOobPrevention();
			}

			if (i - 1 > 0) {
				Rotate(i - 1);
			}
		}

		public static Vector2 StringToVector2(string str) {
			string[] s = str.Split(',');
			return new Vector2(int.Parse(s[0]), int.Parse(s[1]));
		}

		private void AddEntityFromInts(Point p, int entityType) {
			if (entityType < 0 || entityType > 14) {
				return;
			}
			Vector2 np = new Vector2(p.X * Tile.UnitX, p.Y * Tile.UnitY);
			switch (entityType) {
				case 0:
					Entity e = new PushableBlockEntity(np, PushableBlockEntity.Pushabilities.Infinite, PushableBlockEntity.MovementStyles.Omni);
					AddEntity(e);
					return;
				case 1:
					e = new BlockEntity(np, new Sprite(GameHandler.AssetHandler.GetTexture("tilesheet")));
					e.Sprite.CurrentIndex = new Point(6, 0);
					AddEntity(e);
					return;
				case 2:
					e = new GrassEntity(np);
					AddEntity(e);
					return;
				case 3:
					e = new PillarEntity(np, PillarEntity.Types.Square, false);
					AddEntity(e);
					return;
				case 4:
					e = new PillarEntity(np, PillarEntity.Types.Circle, false);
					AddEntity(e);
					return;
				case 5:
					e = new PillarEntity(np, PillarEntity.Types.Triangle, false);
					AddEntity(e);
					return;
				case 6:
					e = new BlockEntity(np, new Sprite(GameHandler.AssetHandler.GetTexture("tilesheet")));
					e.Sprite.CurrentIndex = new Point(1, 7);
					AddEntity(e);

					// Add torch to torch list
					//DiscoverableObject.Torches.Add(e);
					return;
				case 7:
					e = new BladeTrapEntity(np);
					AddEntity(e);
					return;
				case 8:
					e = new GhiniEntity(np);
					AddEntity(e);
					return;
				case 9:
					e = new GibdoEntity(np);
					AddEntity(e);
					return;
				case 10:
					e = new RopeEntity(np);
					AddEntity(e);
					return;
				case 11:
					e = new KeeseEntity(np);
					AddEntity(e);
					return;
				case 12:
					e = new HardhatBeetleEntity(np);
					AddEntity(e);
					return;
				case 13:
					e = new WizzrobeEntity(np);
					AddEntity(e);
					return;
				case 14:
					e = new ArmosEntity(np);
					AddEntity(e);
					return;
			}
		}

		private void AddTileFromInts(Point p, int tileType, int tileSubType, Dictionary<int, string> dictionary, Texture2D texture) {
			if (tileType < 0) {
				return;
			}
			string assetname;
			if (tileType < 43) {
				assetname = dictionary[tileType];
			} else {
				assetname = "";
			}

			Tile t = null;
			switch (assetname) {
				case "floor":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(2, 0);
					t.staticRendered = true;
					break;
				case "crackedfloor":
					t = new CrackedFloor(new Vector2(p.X, p.Y), CrackedFloor.Types.Once, true);
					break;
				case "floor2":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(4, 0);
					t.staticRendered = true;
					break;
				case "ice":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture), false, Tile.SpecialTypes.Ice);
					t.Sprite.CurrentIndex = new Point(5, 0);
					t.staticRendered = true;
					break;
				case "block":
					t = new BlockTile(new Vector2(p.X, p.Y));
					break;
				case "movableblock":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(2, 0);
					t.staticRendered = true;

					Entity e = new PushableBlockEntity(new Vector2(p.X, p.Y), PushableBlockEntity.Pushabilities.Infinite, PushableBlockEntity.MovementStyles.Omni);
					AddEntity(e);
					break;
				case "grass":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(2, 0);
					t.staticRendered = true;

					e = new GrassEntity(new Vector2(p.X, p.Y));
					AddEntity(e);
					break;
				case "ravine":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture), false, Tile.SpecialTypes.None, false);
					t.Sprite.CurrentIndex = new Point(tileSubType, 1);
					t.staticRendered = true;
					break;
				case "wall":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture), true);
					t.Sprite.CurrentIndex = new Point(tileSubType, 2);
					t.staticRendered = true;
					break;
				case "chest":
					t = new ChestTile(new Vector2(p.X, p.Y), ChestTile.Types.Standard);
					break;
				case "keychest":
					t = new ChestTile(new Vector2(p.X, p.Y), ChestTile.Types.Key);
					break;
				case "ppcircle":
					t = new PressurePlateTile(new Vector2(p.X, p.Y), PressurePlateTile.Types.Circle, tileSubType == 0 ? false : true, true);
					break;
				case "ppsquare":
					t = new PressurePlateTile(new Vector2(p.X, p.Y), PressurePlateTile.Types.Square, tileSubType == 0 ? false : true, true);
					break;
				case "pptriangle":
					t = new PressurePlateTile(new Vector2(p.X, p.Y), PressurePlateTile.Types.Triangle, tileSubType == 0 ? false : true, true);
					break;
				case "wallsheet":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture), true);
					t.Sprite.CurrentIndex = new Point(tileSubType, 4);
					t.staticRendered = true;
					break;
				case "rpsquare":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(2, 0);
					t.staticRendered = true;
					e = new PillarEntity(new Vector2(p.X, p.Y), PillarEntity.Types.Square, bool.Parse(tileSubType.ToString()));
					AddEntity(e);
					break;
				case "rpcircle":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(2, 0);
					t.staticRendered = true;
					e = new PillarEntity(new Vector2(p.X, p.Y), PillarEntity.Types.Circle, bool.Parse(tileSubType.ToString()));
					AddEntity(e);
					break;
				case "rptriangle":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(2, 0);
					t.staticRendered = true;
					e = new PillarEntity(new Vector2(p.X, p.Y), PillarEntity.Types.Triangle, bool.Parse(tileSubType.ToString()));
					AddEntity(e);
					break;
				case "stairs":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(6 + tileSubType, 5);
					t.staticRendered = true;
					break;
				case "tile_1":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(8, 5);
					t.staticRendered = true;
					break;
				case "tile_2":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(9, 5);
					t.staticRendered = true;
					break;
				case "tile_3":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(10, 5);
					t.staticRendered = true;
					break;
				case "tile_5":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(11, 5);
					t.staticRendered = true;
					break;
				case "tilesheet2":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture), true);
					t.Sprite.CurrentIndex = new Point(tileSubType, 6);
					t.staticRendered = true;
					break;
				case "tile_6":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(0, 7);
					t.staticRendered = true;
					break;
				case "tile_7":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture), true);
					t.Sprite.CurrentIndex = new Point(1, 7);
					t.staticRendered = true;
					break;
				case "water_1":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(2, 7);
					t.staticRendered = true;
					break;
				case "water_2":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(3, 7);
					t.staticRendered = true;
					break;
				case "tile_8":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(4, 7);
					t.staticRendered = true;
					break;
				case "lamp_dark":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture), true);
					t.Sprite.CurrentIndex = new Point(tileSubType, 8);
					t.staticRendered = true;

					// Add torch to torch list
					//DiscoverableObject.Torches.Add(t);
					break;
				case "lamp_light":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture), true);
					t.Sprite.CurrentIndex = new Point(tileSubType, 9);
					t.staticRendered = true;

					// Add torch to torch list
					//DiscoverableObject.Torches.Add(t);
					break;
				case "ravineladder":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(tileSubType, 10);
					t.staticRendered = true;
					break;
				case "tile_9":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture), true);
					t.Sprite.CurrentIndex = new Point(0, 11);
					t.staticRendered = true;
					break;
				case "sand":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(1, 11);
					t.staticRendered = true;
					break;
				case "keydoor":
					t = new KeyLockedDoor(new Vector2(p.X, p.Y));
					t.Sprite.CurrentIndex = new Point(tileSubType, 12);
					break;
				case "wall2":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture), true);
					t.Sprite.CurrentIndex = new Point(tileSubType, 13);
					t.staticRendered = true;
					break;
				case "tile_10":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(0, 14);
					t.staticRendered = true;
					break;
				case "tile_11":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(1, 14);
					t.staticRendered = true;
					break;
				case "tile_12":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(2, 14);
					t.staticRendered = true;
					break;
				case "tile_13":
					t = new Tile(new Vector2(p.X, p.Y), new Sprite(texture));
					t.Sprite.CurrentIndex = new Point(3, 14);
					t.staticRendered = true;
					break;
				case "endstairs":
					t = new EndStairsTile(new Vector2(p.X, p.Y));
					t.staticRendered = true;
					break;
			}

			AddTile(t, new Vector2(p.X, p.Y));
		}

		private void ReloadEntities() {
			if (!File.Exists(filePath)) {
				return;
			}

			StreamReader reader = new StreamReader(filePath);
			string line;
			string lastline = "";
			int i = 0;

			Dictionary<int, string> dictionary = IntAssetDictionary;

			Texture2D texture = GameHandler.AssetHandler.GetTexture("tilesheet");

			while ((line = reader.ReadLine()) != null) {
				if (i >= Size.Y + 3) {
					if (lastline != line) {
						string[] split = line.Split(' ');
						AddEntityFromInts(new Point(int.Parse(split[1]), int.Parse(split[2])), int.Parse(split[0]));
					}
				}

				lastline = line;
				i++;
			}
		}

		private void RerotateEntities(int i, Vector2 ss) {
			Vector2 ts = new Vector2(ss.X, ss.Y);

			//Rotate size
			float s = ts.X;
			ts.X = ts.Y;
			ts.Y = s;

			//Rotate entities
			foreach (Entity e in Entities) {
				e.Position = new Vector2(ts.X * Tile.UnitX - e.Position.Y - Tile.UnitX, e.Position.X);
			}

			if (i - 1 > 0) {
				RerotateEntities(i - 1, ts);
			}
		}
	}
}