﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLibrary.Types;
using MultiLink.Objects.Tiles;

namespace MultiLink.Objects.Rooms {
	public partial class Room : GameObject {
		public static readonly Point UnitSize = new Point(11, 11);
		public List<Entity> entities = new List<Entity>();
		public string EntranceOrientation;
		public Point EntrancePosition;
		public string ExitOrientation;
		public Point ExitPosition;
		public bool[] LinkValues = new bool[3];
		public bool redrawStaticRender;
		public int rotations = 0;
		private bool calculatedCollisionList;
		private bool calculatedDamageList;
		private bool calculatedEmptyList;
		private List<Rectangle> collisionList;
		private Dictionary<Rectangle, DamagePair> damageList;
		private List<Rectangle> emptyList;
		private string filePath;
		private readonly bool OOBPrevention = true;
		private List<Rectangle> oobPreventionCollision = new List<Rectangle>();
		private Vector2 roomSize;
		private RenderTarget2D staticRender;
		private float staticRenderAlpha = 1f;

		/// <summary>
		/// List of boundingboxes of all the solids
		/// </summary>
		public List<Rectangle> CollisionList {
			get {
				if (!calculatedCollisionList) {
					List<Rectangle> list = (from Tile t in Tiles where t != null where t.IsSolid select t.BoundingBox).ToList();
					list.AddRange(from e in Entities where e.IsSolid select e.BoundingBox);
					if ((Parent as WorldObject).Player.ParentRoom == this) {
						list.Add((Parent as WorldObject).Player.BoundingBox);
					}
					if ((Parent as WorldObject).Player2.ParentRoom == this) {
						list.Add((Parent as WorldObject).Player2.BoundingBox);
					}
					// OOBPrevention
					if (OOBPrevention) {
						CalculateOobPrevention();
						list.AddRange(oobPreventionCollision);
					}

					collisionList = list;
					calculatedCollisionList = true;
					return list;
				}
				return collisionList;
			}
		}

		public Dictionary<Rectangle, DamagePair> DamageList {
			get {
				if (!calculatedDamageList) {
					Dictionary<Rectangle, DamagePair> d = new Dictionary<Rectangle, DamagePair>();
					foreach (KeyValuePair<Rectangle, DamagePair> p in from e in Entities from p in e.GetDamageRectangles() where !d.ContainsKey(p.Key) select p) {
						d.Add(p.Key, p.Value);
					}

					if (((WorldObject)Parent).Player.ParentRoom == this) {
						foreach (KeyValuePair<Rectangle, DamagePair> p in ((WorldObject)Parent).Player.GetDamageRectangles()) {
							d.Add(p.Key, p.Value);
						}
					}
					if (((WorldObject)Parent).Player2.ParentRoom == this) {
						foreach (KeyValuePair<Rectangle, DamagePair> p in ((WorldObject)Parent).Player2.GetDamageRectangles()) {
							d.Add(p.Key, p.Value);
						}
					}
					damageList = d;
					return d;
				}
				return damageList;
			}
		}

		/// <summary>
		/// List of all boundingboxes of non-solid floors
		/// </summary>
		public List<Rectangle> EmptyList {
			get {
				if (!calculatedEmptyList) {
					List<Rectangle> list = new List<Rectangle>();
					foreach (Rectangle r in from Tile t in Tiles where t != null where !t.IsSolid && !t.IsSolidFloor select t.BoundingBox) {
						r.Inflate(-10, -10);
						list.Add(r);
					}
					emptyList = list;
					calculatedEmptyList = true;
					return list;
				}
				return emptyList;
			}
		}

		/// <summary>
		/// All the entities in the room, excluding the player
		/// </summary>
		public List<Entity> Entities {
			get {
				return entities;
			}
		}

		/// <summary>
		/// All the entities in the room, including the player
		/// </summary>
		public List<Entity> EntitiesInclPlayer {
			get {
				List<Entity> ent = Entities.ToList();
				if (((WorldObject)Parent).Player.ParentRoom == this) {
					ent.Add(((WorldObject)Parent).Player);
				}
				if (((WorldObject)Parent).Player2.ParentRoom == this) {
					ent.Add(((WorldObject)Parent).Player2);
				}
				return ent;
			}
		}

		/// <summary>
		/// The size of the room in UnitSize
		/// </summary>
		public Vector2 RoomSize => roomSize;

		public List<Tile> SpecialFloors => (from Tile t in Tiles where t != null where t.SpecialType != Tile.SpecialTypes.None select t).ToList();

		/// <summary>
		/// A 2D-array of tile objects
		/// </summary>
		public Tile[,] Tiles {
			get;
			private set;
		}

		/// <summary>
		/// Create a new room
		/// </summary>
		/// <param name="position">position of the room</param>
		/// <param name="roomSize">size of the room</param>
		public Room(Vector2 position, Vector2 roomSize) : base(position, Vector2.Zero) {
			this.roomSize = roomSize;
			Tiles = new Tile[(int)roomSize.X, (int)roomSize.Y];
			Size = this.roomSize * Tile.UnitSize;
		}

		public override void Update() {
			calculatedEmptyList = false;
			calculatedDamageList = false;
			calculatedCollisionList = false;

			Rectangle r = new Rectangle(0, 0, (int)(Size.X * Tile.UnitX), (int)(Size.Y * Tile.UnitY));
			List<Entity> l = new List<Entity>();
			foreach (Entity e in Entities) {
				if (!e.BoundingBox.Intersects(r)) {
					l.Add(e);
				}
			}
			foreach (Entity e in l) {
				GameHandler.InfoHandler.Output(e.ToString());
				RemoveEntity(e);
			}

			base.Update();
		}

		/// <summary>
		/// Add an entity to the room
		/// </summary>
		/// <param name="e">Entity</param>
		public void AddEntity(Entity e) {
			entities.Add(e);
			AddChild(e);
			e.ParentRoom = this;
		}

		public void AddTile(Tile obj, Vector2 position) {
			AddTile(obj, (int)position.X, (int)position.Y);
		}

		public void AddTile(Tile obj, int x, int y) {
			if (!InBounds(x, y)) {
				return;
			}
			if (Tiles[x, y] != null) {
				RemoveTileAtPosition(x, y);
			}
			Tiles[x, y] = obj;
			AddChild(obj);
		}

		public void Blackout() {
			/*staticRenderAlpha = .2f;
            foreach (Tile t in Tiles) {
				if (t != null)
					t.Alpha = .2f;
            }
            foreach (Entity e in Entities) {
				e.Alpha = .2f;
			}*/
		}

		public Tile GetTile(Vector2 position) {
			return GetTile((int)position.X, (int)position.Y);
		}

		public Tile GetTile(int x, int y) {
			if (!InBounds(x, y)) {
				return null;
			}
			return Tiles[x, y];
		}

		public bool IsNull(Vector2 position) {
			return IsNull((int)position.X, (int)position.Y);
		}

		public bool IsNull(int x, int y) {
			if (!InBounds(x, y)) {
				return true;
			}
			return Tiles[x, y] == null;
		}

		/// <summary>
		/// Remove an entity from the room
		/// </summary>
		/// <param name="e">Entity</param>
		public void RemoveEntity(Entity e) {
			entities.Remove(e);
			RemoveChild(e);
			MultiLink.MultiPlayer.SendRemoveObject(e);
		}

		public void RemoveTileAtPosition(Vector2 position) {
			RemoveTileAtPosition((int)position.X, (int)position.Y);
		}

		public void RemoveTileAtPosition(int x, int y) {
			RemoveTileAtPositionNetworked(x, y);
			MultiLink.MultiPlayer.SendRemoveTileAtPosition(this, x, y);
		}

		public void RemoveTileAtPositionNetworked(int x, int y) {
			if (!InBounds(x, y)) {
				return;
			}
			if (Tiles[x, y] == null) {
				return;
			}
			Tile obj = Tiles[x, y];
			RemoveChild(obj);
			Tiles[x, y] = null;
		}

		public void UnBlackout() {
			/*staticRenderAlpha = 1f;
            foreach (Tile t in Tiles) {
				if (t != null)
                    t.Alpha = 1f;
            }
            foreach (Entity e in Entities) {
                e.Alpha = 1f;
			}*/
		}

		private void CalculateOobPrevention() {
			oobPreventionCollision = new List<Rectangle>();
			Point s = new Point((int)(roomSize.X * UnitSize.X * Tile.UnitX), (int)(roomSize.Y * UnitSize.Y * Tile.UnitY));
			for (int x = 0; x < roomSize.X; x++) {
				oobPreventionCollision.Add(new Rectangle((int)(x * UnitSize.X * Tile.UnitX), (int)-Tile.UnitY, (int)((UnitSize.X - 1) * Tile.UnitX * .5f), (int)Tile.UnitY));
				oobPreventionCollision.Add(new Rectangle((int)(x * UnitSize.X * Tile.UnitX) + (int)((UnitSize.X + 1) * Tile.UnitX * .5f), (int)-Tile.UnitY, (int)((UnitSize.X - 1) * Tile.UnitX * .5f), (int)Tile.UnitY));

				oobPreventionCollision.Add(new Rectangle((int)(x * UnitSize.X * Tile.UnitX), s.Y, (int)((UnitSize.X - 1) * Tile.UnitX * .5f), (int)Tile.UnitY));
				oobPreventionCollision.Add(new Rectangle((int)(x * UnitSize.X * Tile.UnitX) + (int)((UnitSize.X + 1) * Tile.UnitX * .5f), s.Y, (int)((UnitSize.X - 1) * Tile.UnitX * .5f), (int)Tile.UnitY));
			}
			for (int y = 0; y < roomSize.Y; y++) {
				oobPreventionCollision.Add(new Rectangle((int)-Tile.UnitX, (int)(y * UnitSize.Y * Tile.UnitY), (int)Tile.UnitX, (int)((UnitSize.Y - 1) * Tile.UnitY * .5f)));
				oobPreventionCollision.Add(new Rectangle((int)-Tile.UnitX, (int)(y * UnitSize.Y * Tile.UnitY) + (int)((UnitSize.Y + 1) * Tile.UnitY * .5f), (int)Tile.UnitX, (int)((UnitSize.Y - 1) * Tile.UnitY * .5f)));

				oobPreventionCollision.Add(new Rectangle(s.X, (int)(y * UnitSize.Y * Tile.UnitY), (int)Tile.UnitX, (int)((UnitSize.Y - 1) * Tile.UnitY * .5f)));
				oobPreventionCollision.Add(new Rectangle(s.X, (int)(y * UnitSize.Y * Tile.UnitY) + (int)((UnitSize.Y + 1) * Tile.UnitY * .5f), (int)Tile.UnitX, (int)((UnitSize.Y - 1) * Tile.UnitY * .5f)));
			}
		}

		/*public override void Draw() {
			if (redrawStaticRender) {
				DrawRenderTarget();
            }

			GameHandler.GraphicsHandler.DrawTexture(Position, 0, new Vector2(staticRender.Bounds.Width, staticRender.Bounds.Height), staticRender, null, Color.White * staticRenderAlpha, SpriteEffects.None, 0f, false);

            base.Draw();
        }*/

		/*private void DrawRenderTarget() {
			staticRender = new RenderTarget2D(GameHandler.GraphicsHandler.GraphicsDeviceManager.GraphicsDevice, (int)(roomSize.X * UnitSize.X * Tile.UnitX), (int)(roomSize.Y * UnitSize.Y * Tile.UnitY));

			GameHandler.GraphicsHandler.SetRenderTarget(staticRender);
			GameHandler.GraphicsHandler.BeginRenderTargetDraw();
			foreach (Tile t in Tiles) {
				if (t == null) {
					continue;
				}
				if (t.staticRendered) {
					t.StaticDraw();
				}
			}
			GameHandler.GraphicsHandler.EndRenderTargetDraw();

			redrawStaticRender = false;
		}*/

		private bool InBounds(int x, int y) {
			return !((x < 0) || (x >= Size.X) || (y < 0) || (y >= Size.Y));
		}
	}
}