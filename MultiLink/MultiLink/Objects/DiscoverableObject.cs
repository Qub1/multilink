﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLink.Objects.Entities;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Entities.Enemies;
using MultiLink.Objects.Tiles;
using MultiLink.Worlds;

namespace MultiLink.Objects {
	public class DiscoverableObject : TextureGameObject {
		//public static List<GameObject> Torches = new List<GameObject>();
		public static float blackDistance = 250f;
		public static float torchBlackDistance = 150f;
		public static float smootheningFactor = 2f;
		public static float normalFade = 0.003f;
		public static float enemyFade = 0.004f;
		public static float discoveredAlpha = 0.075f;
		public static float defaultAlpha = 0f;
		public static float discoveredEnemyAlpha = 0.05f;

		public DiscoverableObject(Vector2 position, Vector2 size) : base(position, size) {
			if (!(this is PlayerCharacter)) {
				Alpha = defaultAlpha;
			}
		}

		public override void Update() {
			if (!(this is PlayerCharacter)) {
				Fade();

				// Check torches
				float playerAlpha = PlayerAlpha();
				//float torchAlpha = TorchAlpha();
				float newAlpha = playerAlpha;//torchAlpha;
				//if (playerAlpha > newAlpha) {
					//newAlpha = playerAlpha;
				//}

				if (newAlpha > Alpha) {
					Alpha = MathHelper.Clamp(newAlpha, 0f, 1f);
				}
			}

			base.Update();
		}

		private void Fade() {
			if ((Alpha > discoveredAlpha && !(this is Enemy))) {
				Alpha -= normalFade;
			}
			if ((Alpha > discoveredEnemyAlpha && this is Enemy)) {
				Alpha -= enemyFade;
			}
		}

		private float PlayerAlpha() {
			World world = Parent.Parent as World;
			float p1Distance = (world.Player.GlobalPosition - GlobalPosition).Length();
			float p2Distance = (world.Player2.GlobalPosition - GlobalPosition).Length();
			float calculateDistance = 0f;
			if (p1Distance > p2Distance) {
				calculateDistance = p2Distance;
			} else {
				calculateDistance = p1Distance;
			}
			return CalculateAlpha(calculateDistance, blackDistance);
		}

		/*private float TorchAlpha() {
			float alpha = 0f;
			float smallestDistance = -1f;
			foreach (GameObject torch in Torches) {
				float torchDistance = (torch.GlobalPosition - GlobalPosition).Length();
				if (torchDistance < smallestDistance || smallestDistance == -1f) {
					smallestDistance = torchDistance;
				}
			}

			if (smallestDistance == -1f) {
				return 0f;
			}
			return CalculateAlpha(smallestDistance, torchBlackDistance) + (float)(Math.Sin(GameHandler.InfoHandler.GameTime.TotalGameTime.TotalMilliseconds / 500f) * 0.05f);
		}*/

		private float CalculateAlpha(float distance, float lightReach) {
			return (float)(1 - Math.Pow(distance, smootheningFactor) / Math.Pow(lightReach, smootheningFactor));
		}
	}
}
