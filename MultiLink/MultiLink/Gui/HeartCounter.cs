using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MultiLibrary.Objects;
using MultiLibrary.Types;

namespace MultiLink.Gui {
	public sealed class HeartCounter : TextureGameObject {
		private readonly Texture2D fullheart;
		private readonly Texture2D halfheart;
		private readonly TextureGameObject[] hearts;
		private int lastHealth = -1;

		public HeartCounter(Vector2 position, Texture2D fullheart, Texture2D halfheart, Texture2D emptyheart) : base(position, Vector2.Zero) {
			this.fullheart = fullheart;
			this.halfheart = halfheart;
			Gui = true;

			int x = 0;
			for (int i = 0; i < 5; i++) {
				TextureGameObject o = new TextureGameObject(new Vector2(x, 0), new Vector2(fullheart.Width));
				Sprite s = new Sprite(emptyheart);
				o.Sprite = s;
				o.Layer = .99f;
				o.Gui = true;
				AddChild(o);
				x += fullheart.Width;
			}

			hearts = new TextureGameObject[5];

			x = 0;
			for (int i = 0; i < 5; i++) {
				TextureGameObject o = new TextureGameObject(new Vector2(x, 0), new Vector2(fullheart.Width));
				Sprite s = new Sprite(fullheart);
				o.Sprite = s;
				o.Layer = 1f;
				o.Gui = true;
				AddChild(o);
				x += fullheart.Width;
				hearts[i] = o;
			}
		}

		public void UpdateHearts(int health) {
			if (health == lastHealth) {
				return;
			}
			lastHealth = health;

			int fullhearts = (int)Math.Floor(health / 2f);
			int halfhearts = health % 2;

			for (int i = 0; i < 5; i++) {
				if (i < fullhearts) {
					hearts[i].Sprite.Texture = fullheart;
					hearts[i].Visible = true;
				} else if (i - fullhearts < halfhearts) {
					hearts[i].Sprite.Texture = halfheart;
					hearts[i].Visible = true;
				} else {
					hearts[i].Visible = false;
				}
			}
		}
	}
}