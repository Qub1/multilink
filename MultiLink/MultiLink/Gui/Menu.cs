﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLink.Menu;
using MultiLink.Objects;

namespace MultiLink.Gui {
	public sealed class Menu : TextureGameObject {
		private readonly SoundEffect select;
		private readonly Selector selector = new Selector();

		/// <summary>
		/// Creates a new Menu.
		/// </summary>
		public Menu(Vector2 position, Vector2 size) : base(position, size) {
			select = GameHandler.AssetHandler.GetSoundEffect(@"Sounds/LA_Menu_Select");

			// Setup background
			TextureGameObject background = new TextureGameObject(Vector2.Zero, Size) {
				Sprite = {
					Texture = GameHandler.AssetHandler.GetTexture("background_menu")
				},
				Layer = 0.9f,
				Gui = true
			};

			// Setup menu items
			TextGameObject resumeLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "RESUME") {
				Layer = 1f,
				Gui = true
			};
			resumeLabel.Position = new Vector2((Size.X - resumeLabel.Size.X) / 2F, 125);
			TextGameObject mainMenuLabel = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "MAIN MENU") {
				Layer = 1f,
				Gui = true
			};
			mainMenuLabel.Position = new Vector2((Size.X - mainMenuLabel.Size.X) / 2F, 225);

			selector.Gui = true;
			selector.Layer = 1f;

			// Add to this GameObject as children
			AddChild(background);
			AddChild(resumeLabel);
			AddChild(mainMenuLabel);
			AddChild(selector);

			// Add text items to list
			selector.AddItem(resumeLabel);
			selector.AddItem(mainMenuLabel);
		}

		public override void Update() {
			// Move selector up / down and left / right
			if (GameHandler.InputHandler.OnKeyDown(Keys.Up) || GameHandler.InputHandler.OnKeyDown(Keys.Left)) {
				if (selector.CurrentIndex > 0) {
					selector.CurrentIndex -= 1;
				} else {
					selector.CurrentIndex = selector.Items.Count - 1;
				}
			}
			if (GameHandler.InputHandler.OnKeyDown(Keys.Down) || GameHandler.InputHandler.OnKeyDown(Keys.Right)) {
				if (selector.CurrentIndex < selector.Items.Count - 1) {
					selector.CurrentIndex += 1;
				} else {
					selector.CurrentIndex = 0;
				}
			}

			// Switch to another state is Enter or Space is pressed
			if (GameHandler.InputHandler.OnKeyDown(Keys.Enter) || GameHandler.InputHandler.OnKeyDown(Keys.Space)) {
				if (selector.CurrentIndex == 0) // Resume
				{
					Resume();
					@select.Play();
				} else if (selector.CurrentIndex == 1) // Main menu
				{
					MainMenu();
					@select.Play();
				}
			}

			// Handle mouse input
			Point mousePosition = new Point((int)GameHandler.InputHandler.MousePosition.X, (int)GameHandler.InputHandler.MousePosition.Y);
			if (selector.Items[0].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 0;
				if (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left)) {
					Resume();
					@select.Play();
				}
			} else if (selector.Items[1].BoundingBox.Contains(mousePosition)) {
				selector.CurrentIndex = 1;
				if (GameHandler.InputHandler.OnMouseButtonDown(InputHandler.MouseButton.Left)) {
					MainMenu();
					@select.Play();
				}
			}

			base.Update();
		}

		private void MainMenu() {
			GameHandler.StateHandler.States["Menu"].Reset();
			GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["Menu"];
		}

		private void Resume() {
			Active = false;
			Visible = false;
			Entity.inMenu = false;
		}
	}
}