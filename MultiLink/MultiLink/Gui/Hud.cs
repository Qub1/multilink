﻿using System;
using System.Runtime.CompilerServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLibrary.Types;
using MultiLink.Objects;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Rooms;
using MultiLink.Objects.Tiles;
using MultiLink.Worlds;

namespace MultiLink.Gui {
	/// <summary>
	/// The graphical user interface.
	/// </summary>
	public sealed class Hud : GameObject {
		public PlayerCharacter Player;
		public PlayerCharacter Player2;
		public bool StopGame = false;

		private readonly TextGameObject arrowText,
										arrowText2;

		private readonly TextGameObject bombText,
										bombText2;

		private TimeSpan clock;

		private readonly TextGameObject deathText,
										deathText2;

		private readonly TextGameObject fps;
		private bool gameStopped;

		private readonly HeartCounter heartCounter,
									  heartCounter2;

		private readonly TextGameObject keyText,
										keyText2;

		private readonly Menu menu;
		private readonly TextGameObject time;
		private readonly TextGameObject tps;
		private TextGameObject chatHistory1;
		private TextGameObject chatHistory2;
		private TextGameObject chatHistory3;
		private TextGameObject chatHistory4;
		private TextGameObject chatHistory5;
		private TextureGameObject background;

		private DateTime lastChatMove = DateTime.Now;
		private TextBoxGameObject chat;


		public void AddChat(string text, Color color) {
			MoveChat();

			chatHistory1.Text = text;
			chatHistory1.Color = color;
		}

		private void MoveChat() {
			chatHistory5.Text = chatHistory4.Text;
			chatHistory5.Color = chatHistory4.Color;
			chatHistory4.Text = chatHistory3.Text;
			chatHistory4.Color = chatHistory3.Color;
			chatHistory3.Text = chatHistory2.Text;
			chatHistory3.Color = chatHistory2.Color;
			chatHistory2.Text = chatHistory1.Text;
			chatHistory2.Color = chatHistory1.Color;

			chatHistory1.Text = "";

			lastChatMove = DateTime.Now;
		}

		public bool localMultiplayer {
			get {
				return !MultiLink.MultiPlayer.IsOnline;
			}
		}

		/// <summary>
		/// Creates a new GUIState.
		/// </summary>
		public Hud() : base(Vector2.Zero, Vector2.Zero) {
			//Background
			if (localMultiplayer) {
				background = new TextureGameObject(new Vector2(0, GameHandler.GraphicsHandler.BaseResolution.Y - 120), new Vector2(1280, 120));
				background.Sprite.Texture = GameHandler.AssetHandler.GetTexture("background_hud_local_mp");
			} else {
				background = new TextureGameObject(new Vector2(0, GameHandler.GraphicsHandler.BaseResolution.Y - 80), new Vector2(1280, 80));
				background.Sprite.Texture = GameHandler.AssetHandler.GetTexture("background_hud");
			}
			background.Layer = .9f;
			background.Gui = true;
			AddChild(background);

			// Menu
			Vector2 resolution = new Vector2(GameHandler.GraphicsHandler.BaseResolution.X, GameHandler.GraphicsHandler.BaseResolution.Y);
			menu = new Menu(resolution / 4F, resolution / 2F) {
				Active = false,
				Visible = false,
				Position = resolution / 4F,
				Layer = 1f,
				Gui = true
			};
			AddChild(menu);

			// Chat interface
			if (!localMultiplayer) {
				chatHistory1 = new TextGameObject(new Vector2(20, 20), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "Welcome to MultiLink multiplayer!") {
					Color = Color.White,
					Layer = 1f,
					Gui = true
				};
				chatHistory2 = new TextGameObject(new Vector2(20, chatHistory1.Position.Y + chatHistory1.TextSize.Y + 5), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), "Press enter to chat.") {
					Color = Color.White,
					Layer = 1f,
					Gui = true
				};
				chatHistory3 = new TextGameObject(new Vector2(20, chatHistory2.Position.Y + chatHistory2.TextSize.Y + 5), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), " ") {
					Color = Color.White,
					Layer = 1f,
					Gui = true
				};
				chatHistory4 = new TextGameObject(new Vector2(20, chatHistory3.Position.Y + chatHistory3.TextSize.Y + 5), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), " ") {
					Color = Color.White,
					Layer = 1f,
					Gui = true
				};
				chatHistory5 = new TextGameObject(new Vector2(20, chatHistory4.Position.Y + chatHistory4.TextSize.Y + 5), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"), " ") {
					Color = Color.White,
					Layer = 1f,
					Gui = true
				};

				AddChild(chatHistory1);
				AddChild(chatHistory2);
				AddChild(chatHistory3);
				AddChild(chatHistory4);
				AddChild(chatHistory5);

				chat = new TextBoxGameObject(Vector2.Zero, new Vector2(960, 44), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28")) {
					Sprite = {
						Texture = GameHandler.AssetHandler.GetTexture("background_inputbox")
					},
					Text = {
						Color = Color.White
					},
					Layer = 0.9f,
					Gui = true,
					Active = false,
					Visible = false,
					HasFocus = false,
					MaxLength = 36
				};
				chat.Position = (resolution - chat.Size) / 2F;
				AddChild(chat);
			}

			//Arrow text and image
			if (localMultiplayer) {
				arrowText = new TextGameObject(new Vector2(65, 600), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"), "-") {
					Color = Color.Black,
					Layer = 1f,
					Gui = true
				};
				arrowText2 = new TextGameObject(new Vector2(65, 660), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"), "-") {
					Color = Color.Black,
					Layer = 1f,
					Gui = true
				};
				AddChild(arrowText2);
			} else {
				arrowText = new TextGameObject(new Vector2(65, 650), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"), "-") {
					Color = Color.Black,
					Layer = 1f,
					Gui = true
				};
			}
			AddChild(arrowText);

			if (localMultiplayer) {
				TextureGameObject arrowImage = new TextureGameObject(new Vector2(5, 610), new Vector2(40, 40)) {
					Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("menu_arrow")),
					Layer = 1f,
					Gui = true
				};
				AddChild(arrowImage);
				TextureGameObject arrowImage2 = new TextureGameObject(new Vector2(5, 670), new Vector2(40, 40)) {
					Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("menu_arrow")),
					Layer = 1f,
					Gui = true
				};
				AddChild(arrowImage2);
			} else {
				TextureGameObject arrowImage = new TextureGameObject(new Vector2(5, 660), new Vector2(40, 40)) {
					Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("menu_arrow")),
					Layer = 1f,
					Gui = true
				};
				AddChild(arrowImage);
			}

			//Bomb text and image
			if (localMultiplayer) {
				bombText = new TextGameObject(new Vector2(293, 600), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"), "-") {
					Color = Color.Black,
					Layer = 1f,
					Gui = true
				};
				AddChild(bombText);
				bombText2 = new TextGameObject(new Vector2(293, 660), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"), "-") {
					Color = Color.Black,
					Layer = 1f,
					Gui = true
				};
				AddChild(bombText2);

				TextureGameObject bombImage = new TextureGameObject(new Vector2(230, 610), new Vector2(40, 40)) {
					Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("menu_bomb")),
					Layer = 1f,
					Gui = true
				};
				AddChild(bombImage);
				TextureGameObject bombImage2 = new TextureGameObject(new Vector2(230, 670), new Vector2(40, 40)) {
					Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("menu_bomb")),
					Layer = 1f,
					Gui = true
				};
				AddChild(bombImage2);
			} else {
				bombText = new TextGameObject(new Vector2(293, 650), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"), "-") {
					Color = Color.Black,
					Layer = 1f,
					Gui = true
				};
				AddChild(bombText);

				TextureGameObject bombImage = new TextureGameObject(new Vector2(230, 660), new Vector2(40, 40)) {
					Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("menu_bomb")),
					Layer = 1f,
					Gui = true
				};
				AddChild(bombImage);
			}

			//Key text and image
			if (localMultiplayer) {
				keyText = new TextGameObject(new Vector2(513, 600), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"), "-") {
					Color = Color.Black,
					Layer = 1f,
					Gui = true
				};
				AddChild(keyText);
				keyText2 = new TextGameObject(new Vector2(513, 660), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"), "-") {
					Color = Color.Black,
					Layer = 1f,
					Gui = true
				};
				AddChild(keyText2);

				TextureGameObject keyImage = new TextureGameObject(new Vector2(453, 610), new Vector2(40, 40)) {
					Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("key")),
					Layer = 1f,
					Gui = true
				};
				AddChild(keyImage);
				TextureGameObject keyImage2 = new TextureGameObject(new Vector2(453, 670), new Vector2(40, 40)) {
					Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("key")),
					Layer = 1f,
					Gui = true
				};
				AddChild(keyImage2);
			} else {
				keyText = new TextGameObject(new Vector2(513, 650), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"), "-") {
					Color = Color.Black,
					Layer = 1f,
					Gui = true
				};
				AddChild(keyText);

				TextureGameObject keyImage = new TextureGameObject(new Vector2(453, 660), new Vector2(40, 40)) {
					Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("key")),
					Layer = 1f,
					Gui = true
				};
				AddChild(keyImage);
			}

			//Heart counter
			if (localMultiplayer) {
				heartCounter = new HeartCounter(new Vector2(575, 615), GameHandler.AssetHandler.GetTexture("heart"), GameHandler.AssetHandler.GetTexture("halfheart"), GameHandler.AssetHandler.GetTexture("emptyheart"));
				heartCounter2 = new HeartCounter(new Vector2(575, 675), GameHandler.AssetHandler.GetTexture("heart"), GameHandler.AssetHandler.GetTexture("halfheart"), GameHandler.AssetHandler.GetTexture("emptyheart"));
				AddChild(heartCounter2);
			} else {
				heartCounter = new HeartCounter(new Vector2(575, 665), GameHandler.AssetHandler.GetTexture("heart"), GameHandler.AssetHandler.GetTexture("halfheart"), GameHandler.AssetHandler.GetTexture("emptyheart"));
			}
			AddChild(heartCounter);

			time = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48")) {
				Color = Color.Black,
				Layer = 1f,
				Gui = true
			};
			AddChild(time);

			if (localMultiplayer) {
				TextureGameObject skull = new TextureGameObject(new Vector2(775, 610), new Vector2(40, 40));
				skull.Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("skull"));
				skull.Layer = 1f;
				skull.Gui = true;
				AddChild(skull);

				TextureGameObject skull2 = new TextureGameObject(new Vector2(775, 670), new Vector2(40, 40));
				skull2.Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("skull"));
				skull2.Layer = 1f;
				skull2.Gui = true;
				AddChild(skull2);

				deathText = new TextGameObject(new Vector2(820, 600), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"));
				deathText.Color = Color.Black;
				deathText.Layer = 1f;
				deathText.Gui = true;
				AddChild(deathText);

				deathText2 = new TextGameObject(new Vector2(820, 660), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"));
				deathText2.Color = Color.Black;
				deathText2.Layer = 1f;
				deathText2.Gui = true;
				AddChild(deathText2);
			} else {
				TextureGameObject skull = new TextureGameObject(new Vector2(775, 665), new Vector2(40, 40));
				skull.Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("skull"));
				skull.Layer = 1f;
				skull.Gui = true;
				AddChild(skull);

				deathText = new TextGameObject(new Vector2(820, 650), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"));
				deathText.Color = Color.Black;
				deathText.Layer = 1f;
				deathText.Gui = true;
				AddChild(deathText);
			}

			// Create counters
			fps = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"));
			tps = new TextGameObject(Vector2.Zero, GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_28"));
			AddChild(fps);
			AddChild(tps);

			fps.Layer = 1f;
			fps.Gui = true;
			tps.Layer = 1f;
			tps.Gui = true;
		}

		public override void Update() {
			if (!localMultiplayer) {
				if ((DateTime.Now - lastChatMove).Seconds > 5) {
					MoveChat();
				}

				if (GameHandler.InputHandler.OnKeyDown(Keys.Enter)) {
					PlayerCharacter player = null;
					if (MultiLink.MultiPlayer.IsHost) {
						player = Player;
					} else {
						player = Player2;
					}

					if (chat.Visible && chat.Text.Text.Length > 0) {
						// Send chat
						MultiLink.MultiPlayer.SendChat(player, chat.Text.Text);
						chat.Visible = false;
						chat.Active = false;
						chat.HasFocus = false;
						player.Chatting = false;
					} else if (chat.Visible) {
						chat.Visible = false;
						chat.Active = false;
						chat.HasFocus = false;
						player.Chatting = false;
					} else {
						player.Chatting = true;
						chat.Visible = true;
						chat.Active = true;
						chat.HasFocus = true;
						chat.Text.Text = "";
					}
				}
			}

			if (GameHandler.InputHandler.OnKeyDown(Keys.M)) {
				//GameHandler.GraphicsHandler.Camera.UseOffset = false;

				World world = ((World)Player.Parent);

				float height = GameHandler.GraphicsHandler.Resolution.Y - (background.Size.Y / GameHandler.GraphicsHandler.BaseResolution.Y * GameHandler.GraphicsHandler.Resolution.Y);
				//world.Scale = new Vector2(height, height) / ((world.WorldSize.Y - world.SmallestRoomPosition.Y) / GameHandler.GraphicsHandler.BaseResolution.Y * GameHandler.GraphicsHandler.Resolution.Y);
				world.Scale = new Vector2(0.4f, 0.4f);
				GameHandler.GraphicsHandler.Camera.OffsetScale = world.Scale;
				//world.Position = -world.SmallestRoomPosition;
				world.UpdateScale = false;
			}

			if (GameHandler.InputHandler.OnKeyUp(Keys.M)) {
				//GameHandler.GraphicsHandler.Camera.UseOffset = true;
				
				World world = ((World)Player.Parent);

				world.Scale = Vector2.One;
				GameHandler.GraphicsHandler.Camera.OffsetScale = world.Scale;
				//world.Position = Vector2.Zero;
				world.UpdateScale = true;
			}

			if (!StopGame) {
				clock += GameHandler.InfoHandler.GameTime.ElapsedGameTime;
			}

			if (StopGame && !gameStopped) {
				TextGameObject finalTime = new TextGameObject(new Vector2(550, 470), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"), clock.ToString());
				finalTime.Color = Color.White;
				finalTime.Layer = 1f;
				finalTime.Gui = true;
				AddChild(finalTime);
				finalTime.Text = clock.ToString(@"mm\:ss");
				TextGameObject finalDeath = new TextGameObject(new Vector2(250, 470), GameHandler.AssetHandler.GetSpriteFont("Fonts\\font_48"), Player.DeathCount + Player2.DeathCount + " Deaths");
				finalDeath.Color = Color.White;
				finalDeath.Layer = 1f;
				finalDeath.Gui = true;
				AddChild(finalDeath);
				finalDeath.Text = (Player.DeathCount + Player2.DeathCount).ToString();
				gameStopped = true;
			}

			time.Text = clock.ToString(@"mm\:ss");
			if (localMultiplayer) {
				time.Position = new Vector2(GameHandler.GraphicsHandler.BaseResolution.X - 200F, 630);
			} else {
				time.Position = new Vector2(GameHandler.GraphicsHandler.BaseResolution.X - 200F, 650);
			}

			// Update inventory items
			if (localMultiplayer) {
				heartCounter.UpdateHearts(Player.Health);
				heartCounter2.UpdateHearts(Player2.Health);

				// Update inventory items
				bombText.Text = Player.Bombs.ToString();
				arrowText.Text = Player.Arrows.ToString();
				keyText.Text = Player.Key.ToString();
				bombText2.Text = Player2.Bombs.ToString();
				arrowText2.Text = Player2.Arrows.ToString();
				keyText2.Text = Player2.Key.ToString();
				deathText.Text = Player.DeathCount.ToString();
				deathText2.Text = Player2.DeathCount.ToString();
			} else {
				if (Player.CanControl) {
					heartCounter.UpdateHearts(Player.Health);

					// Update inventory items
					bombText.Text = Player.Bombs.ToString();
					arrowText.Text = Player.Arrows.ToString();
					keyText.Text = Player.Key.ToString();
					deathText.Text = Player.DeathCount + " / " + (Player.DeathCount + Player2.DeathCount);
				} else {
					heartCounter.UpdateHearts(Player2.Health);
					bombText.Text = Player2.Bombs.ToString();
					arrowText.Text = Player2.Arrows.ToString();
					keyText.Text = Player2.Key.ToString();
					deathText.Text = Player2.DeathCount + " / " + (Player.DeathCount + Player2.DeathCount);
				}
			}

			// Check menu
			if (GameHandler.InputHandler.OnKeyDown(Keys.Escape)) {
				if (menu.Visible) {
					menu.Active = false;
					menu.Visible = false;
					Entity.inMenu = false;
				} else {
					menu.Active = true;
					menu.Visible = true;
					Entity.inMenu = true;
				}
			}

			base.Update();
		}
	}
}