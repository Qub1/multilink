﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLibrary.Helpers;
using MultiLibrary.Objects;
using MultiLibrary.Types;
using MultiLink.Gui;
using MultiLink.Objects;
using MultiLink.Objects.Entities;
using MultiLink.Objects.Entities.Characters;
using MultiLink.Objects.Rooms;
using MultiLink.Objects.Tiles;
using MultiLink.States;

namespace MultiLink.MultiPlayer {
	public class MultiPlayer {
		public Client Client;
		public bool IsOnline;
		public bool OtherReady;
		public bool SelfReady;
		public Server Server;
		private int remoteNextId = -1;
		public bool IsHost => (Server != null) || (Server == null && Client == null);

		public void Host() {
			IsOnline = true;

			Server = new Server(80);
			while (!Server.AcceptingConnections) {
				GameHandler.InfoHandler.Output("Waiting for server to start...");
				Thread.Sleep(1000);
			}
			Client = new Client(GameHandler.NetworkHandler.GetLocalIpAddress().ToString(), 80, ProcessMessage);
		}

		public void Join(string server = "") {
			if (server == "") {
				server = GameHandler.NetworkHandler.GetLocalIpAddress().ToString();
			}

			IsOnline = true;

			Server = null;
			Client = new Client(server, 80, ProcessMessage);
		}

		public void ProcessMessage(string message) {
			try {
				ProcessMessage(StringToDictionary(message));
			} catch (Exception) {
				GameHandler.InfoHandler.Output("Could not process command \"" + message + "\"!", InfoHandler.OutputType.Warning);
			}
		}

		public void ProcessMessage(Dictionary<string, string> message) {
			try {
				if (message.ContainsKey("command")) {
					string command = message["command"];
					GameObject gameObject = null;
					if (message.ContainsKey("id")) {
						int id = int.Parse(message["id"]);
						if (GameHandler.InfoHandler.GlobalGameObjects.ContainsKey(id)) {
							gameObject = GameHandler.InfoHandler.GlobalGameObjects[int.Parse(message["id"])];
						}
					}

					if (IsHost) {
						// These commands should only be processed by the host and no one else
						// These are input commands and such from other clients
					} else {
						// These commands should only be processed by clients
						// These are position, Health etc. commands
						if (command == "sethealth" && gameObject != null && message.ContainsKey("value")) {
							SetHealth((LivingEntity)gameObject, int.Parse(message["value"]));
						} else if (command == "reloadroom" && gameObject != null) {
							ReloadRoom((Room)gameObject);
						} else if (command == "removetileatposition" && gameObject != null && message.ContainsKey("x") && message.ContainsKey("y")) {
							RemoveTileAtPosition((Room)gameObject, int.Parse(message["x"]), int.Parse(message["y"]));
						} else if (command == "shootprojectile" && gameObject != null) {
							ShootProjectile((WizzrobeEntity)gameObject);
						}
					}

					// These commands should be processed both by the host and client
					if (command == "start" && message.ContainsKey("seed") && message.ContainsKey("tutorialenabled")) {
						Start(int.Parse(message["seed"]), bool.Parse(message["tutorialenabled"]));
					} else if (command == "ready") {
						Ready();
					} else if (command == "syncid" && message.ContainsKey("id")) {
						SyncId(int.Parse(message["id"]));
					} else if (command == "setposition" && gameObject != null && message.ContainsKey("x") && message.ContainsKey("y")) {
						if (IsHost) {
							// On the host, only process movement of the other player
							if (gameObject is PlayerCharacter && ((PlayerCharacter)gameObject).CanControl == false) {
								SetPosition((Entity)gameObject, new Vector2(float.Parse(message["x"]), float.Parse(message["y"])), bool.Parse(message["interpolate"]));
							}
						} else {
							// On the client, accept any movement
							SetPosition((Entity)gameObject, new Vector2(float.Parse(message["x"]), float.Parse(message["y"])), bool.Parse(message["interpolate"]));
						}
					} else if (command == "shootarrow" && gameObject != null) {
						if (IsHost) {
							// On the host, only process shooting of the other player
							if (gameObject is PlayerCharacter && ((PlayerCharacter)gameObject).CanControl == false) {
								ShootArrow((PlayerCharacter)gameObject);
							}
						} else {
							// On the client, accept any shooting
							ShootArrow((PlayerCharacter)gameObject);
						}
					} else if (command == "throwbomb" && gameObject != null) {
						if (IsHost) {
							// On the host, only process throwing of the other player
							if (gameObject is PlayerCharacter && ((PlayerCharacter)gameObject).CanControl == false) {
								ThrowBomb((PlayerCharacter)gameObject);
							}
						} else {
							// On the client, accept any throwing
							ThrowBomb((PlayerCharacter)gameObject);
						}
					} else if (command == "swingsword" && gameObject != null) {
						if (IsHost) {
							// On the host, only process swinging of the other player
							if (gameObject is PlayerCharacter && ((PlayerCharacter)gameObject).CanControl == false) {
								SwingSword((PlayerCharacter)gameObject);
							}
						} else {
							// On the client, accept any swinging
							SwingSword((PlayerCharacter)gameObject);
						}
					} else if (command == "interact" && gameObject != null) {
						if (IsHost) {
							// On the host, only process interacting of the other player
							if (gameObject is PlayerCharacter && ((PlayerCharacter)gameObject).CanControl == false) {
								Interact((PlayerCharacter)gameObject);
							}
						} else {
							// On the client, accept any interacting
							Interact((PlayerCharacter)gameObject);
						}
					} else if (command == "setdeathcount" && gameObject != null && message.ContainsKey("value")) {
						SetDeathCount((PlayerCharacter)gameObject, int.Parse(message["value"]));
					} else if (command == "setdirection" && gameObject != null && message.ContainsKey("direction")) {
						if (IsHost) {
							// On the host, only process direction changes of the other player
							if (gameObject is PlayerCharacter && ((PlayerCharacter)gameObject).CanControl == false) {
								SetDirection((CharacterObject)gameObject, (CharacterObject.Direction)Enum.Parse(typeof(CharacterObject.Direction), message["direction"]));
							}
						} else {
							// On the client, accept any direction changes
							SetDirection((CharacterObject)gameObject, (CharacterObject.Direction)Enum.Parse(typeof(CharacterObject.Direction), message["direction"]));
						}
					} else if (command == "removeobject" && gameObject != null) {
						// FIX: Grass won't disappear otherwise, this is a violation of dumb clients (allowing them to remove objects on the host)
						RemoveObject(gameObject);
					} else if (command == "sendpushblock" && gameObject != null && message.ContainsKey("direction")) {
						PushBlock((PushableBlockEntity)gameObject, (PushableBlockEntity.Directions)Enum.Parse(typeof(PushableBlockEntity.Directions), message["direction"]));
					} else if (command == "chat" && gameObject != null && message.ContainsKey("text")) {
						Chat((PlayerCharacter)gameObject, message["text"]);
					}
				}
			} catch (Exception) {
				GameHandler.InfoHandler.Output("Could not process command \"" + DictionaryToString(message) + "\"!", InfoHandler.OutputType.Warning);
			}
		}

		public void Ready() {
			OtherReady = true;
		}

		public void ReloadRoom(Room room) {
			room.Reload();
		}

		public void RemoveObject(GameObject gameObject) {
			if (gameObject.Parent != null) {
				gameObject.Parent.RemoveChild(gameObject);
				if (gameObject is Entity) {
					Entity entity = gameObject as Entity;
					if (entity.ParentRoom != null) {
						entity.ParentRoom.entities.Remove(entity);
					}
				}
			}
		}

		public void RemoveTileAtPosition(Room room, int x, int y) {
			room.RemoveTileAtPositionNetworked(x, y);
		}

		/// <summary>
		/// Broadcasts a message to all other clients.
		/// </summary>
		/// <param name="message">The message to send.</param>
		/// <param name="sendToSelf">Whether to send the message to the client itself.</param>
		public void SendMessage(Dictionary<string, string> message, bool sendToSelf = false) {
			CheckStatus();

			if (Client != null) {
				// Add to outbox queue
				Client.BroadcastMessage(DictionaryToString(message));

				// If send to self, process immediately
				if (sendToSelf) {
					ProcessMessage(message);
				}
			}
		}

		public void SendReady() {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "ready"
				}
			};
			SendMessage(message);
		}

		public void SendReloadRoom(Room room) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "reloadroom"
				}, {
					"id", room.Id.ToString()
				}
			};
			SendMessage(message);
		}

		public void SendRemoveObject(GameObject gameObject) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "removeobject"
				}, {
					"id", gameObject.Id.ToString()
				}
			};
			SendMessage(message);
		}

		public void SendRemoveTileAtPosition(Room room, int x, int y) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "removetileatposition"
				}, {
					"id", room.Id.ToString()
				}, {
					"x", x.ToString()
				}, {
					"y", y.ToString()
				}
			};
			SendMessage(message);
		}

		/*public void SendSetChestState(ChestTile chest, ChestTile.States state) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "setcheststate"
				}, {
					"id", chest.Id.ToString()
				}, {
					"state", state.ToString()
				}
			};
			SendMessage(message);
		}*/

		public void SendSetDeathCount(PlayerCharacter character, int value) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "setdeathcount"
				}, {
					"id", character.Id.ToString()
				}, {
					"value", value.ToString()
				}
			};
			SendMessage(message);
		}

		public void SendSetDirection(CharacterObject character, CharacterObject.Direction direction) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "setdirection"
				}, {
					"id", character.Id.ToString()
				}, {
					"direction", direction.ToString()
				}
			};
			SendMessage(message);
		}

		public void SendPushBlock(PushableBlockEntity block, PushableBlockEntity.Directions direction) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "pushblock"
				}, {
					"id", block.Id.ToString()
				}, {
					"direction", direction.ToString()
				}
			};
			SendMessage(message);
		}

		public void SendSetHealth(LivingEntity entity, int value) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "sethealth"
				}, {
					"id", entity.Id.ToString()
				}, {
					"value", value.ToString()
				}
			};
			SendMessage(message);
		}

		/*public void SendSetLocked(KeyLockedDoor door, bool locked) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "setlocked"
				}, {
					"id", door.Id.ToString()
				}, {
					"locked", locked.ToString()
				}
			};
			SendMessage(message);
		}*/

		public void SendSetPosition(Entity entity, Vector2 position, bool interpolate = true) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "setposition"
				}, {
					"id", entity.Id.ToString()
				}, {
					"x", entity.Position.X.ToString()
				}, {
					"y", entity.Position.Y.ToString()
				}, {
					"interpolate", interpolate.ToString()
				}
			};
			SendMessage(message);
		}

		public void SendShootArrow(PlayerCharacter player) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "shootarrow"
				}, {
					"id", player.Id.ToString()
				}
			};
			SendMessage(message);
		}

		public void SendShootProjectile(WizzrobeEntity enemy) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "shootprojectile"
				}, {
					"id", enemy.Id.ToString()
				}
			};
			SendMessage(message);
		}

		public void SendStart(int seed, bool tutorialEnabled) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "start"
				}, {
					"seed", seed.ToString()
				}, {
					"tutorialenabled", tutorialEnabled.ToString()
				}
			};
			SendMessage(message, true);
		}

		public void SendSwingSword(PlayerCharacter character) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "swingsword"
				}, {
					"id", character.Id.ToString()
				}
			};
			SendMessage(message);
		}

		public void SendSyncId() {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "syncid"
				}, {
					"id", GameHandler.InfoHandler.NextId.ToString()
				}
			};
			SendMessage(message);
		}

		public void SendThrowBomb(PlayerCharacter player) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "throwbomb"
				}, {
					"id", player.Id.ToString()
				}
			};
			SendMessage(message);
		}

		/*public void SetChestState(ChestTile chest, ChestTile.States state) {
			chest.currentState = state;
		}*/

		public void SetDeathCount(PlayerCharacter player, int value) {
			player.deathCount = value;
		}

		public void SetDirection(CharacterObject character, CharacterObject.Direction direction) {
			character.currentDirection = direction;
		}

		public void PushBlock(PushableBlockEntity block, PushableBlockEntity.Directions direction) {
			block.PushDirectionNetworked(direction);
		}

		public void SetHealth(LivingEntity entity, int value) {
			entity.health = value;
		}

		/*public void SetLocked(KeyLockedDoor door, bool locked) {
			door.locked = locked;
		}*/

		public void SetPosition(Entity entity, Vector2 position, bool interpolate = true) {
			if (interpolate) {
				entity.RemotePosition = position;
			} else {
				entity.position = position;
			}
		}

		public void ShootArrow(PlayerCharacter player) {
			player.ShootArrowNetworked();
		}

		public void ShootProjectile(WizzrobeEntity enemy) {
			enemy.ShootProjectileNetworked();
		}

		public void Start(int seed, bool tutorialEnabled) {
			Thread t = new Thread(() => {
				if (IsOnline) {
					SendSyncId();
					while (remoteNextId == -1) {
						GameHandler.InfoHandler.Output("Waiting for remote id...");
						Thread.Sleep(500);
					}
					if (remoteNextId > GameHandler.InfoHandler.NextId) {
						GameHandler.InfoHandler.NextId = remoteNextId;
					}
					remoteNextId = -1;
				}
			((PlayingState)GameHandler.StateHandler.States["Play"]).Reset(seed, tutorialEnabled);
				SelfReady = true;
				SendReady();
			});
			t.IsBackground = true;
			t.Start();
		}

		public void Stop() {
			if (Server != null) {
				Server.Stop();
			}
			if (Client != null) {
				Client.Stop();
			}

			Server = null;
			Client = null;
			OtherReady = false;
			SelfReady = false;
			remoteNextId = -1;

			IsOnline = false;
			GameHandler.StateHandler.States["Menu"].Reset();
			GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["Menu"];
		}

		public void SwingSword(PlayerCharacter player) {
			player.SwingSwordNetworked();
		}

		public void SyncId(int id) {
			remoteNextId = id;
		}

		public void ThrowBomb(PlayerCharacter player) {
			player.ThrowBombNetworked();
		}

		private void CheckStatus() {
			if (IsOnline && ((Client != null && !Client.Started) || (Server != null && !Server.Started))) {
				Stop();
			}
		}

		private string DictionaryToString(Dictionary<string, string> message) {
			return string.Join(";", message.Select(x => x.Key + "=" + x.Value).ToArray());
		}

		private Dictionary<string, string> StringToDictionary(string message) {
			return message.Split(new[] {
				';'
			}, StringSplitOptions.RemoveEmptyEntries).Select(part => part.Split('=')).ToDictionary(split => split[0], split => split[1]);
		}

		public void SendInteract(PlayerCharacter player) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "interact"
				}, {
					"id", player.Id.ToString()
				}
			};
			SendMessage(message);
		}

		public void Interact(PlayerCharacter player) {
			player.InteractNetworked();
		}

		public void SendChat(PlayerCharacter player, string text) {
			Dictionary<string, string> message = new Dictionary<string, string> {
				{
					"command", "chat"
				}, {
					"id", player.Id.ToString()
				}, {
					"text", text
				}
			};
			SendMessage(message, true);
		}

		public void Chat(PlayerCharacter player, string text) {
			// Get HUD
			Hud hud = ((PlayingState)player.Parent.Parent).hud;
			Color color = Color.White;
			if ((IsHost && player.CanControl) || (!IsHost && !player.CanControl)) {
				color = Color.Blue;
			} else if ((!IsHost && player.CanControl) || (IsHost && !player.CanControl)) {
				color = Color.Red;
			}

			hud.AddChat(text, color);
		}
	}
}