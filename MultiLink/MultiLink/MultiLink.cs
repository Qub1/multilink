﻿using System;
using System.IO;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;
using MultiLink.Menu;
using MultiLink.States;
using MainMenu = MultiLink.Menu.MainMenu;

namespace MultiLink {
	/// <summary>
	/// This is the main type for your game
	/// </summary>
	public class MultiLink : GameHandler {
		public static MultiPlayer.MultiPlayer MultiPlayer = new MultiPlayer.MultiPlayer();
		public static PlayingState PlayingState;

		protected override void LoadContent() {
			base.LoadContent();

			// Create required directories
			Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MultiLink\\Saves");

			InitializeGraphics();

			LoadResources();

			// TODO: Add states, set current state, play music

			InitializeStates();

			StateHandler.CurrentState = StateHandler.States["Title"];
		}

		private void InitializeGraphics() {
			// Set up basic stuff
			IsMouseVisible = true;

			// Set resolution and screen size
			//GraphicsHandler.Resolution = new Point(1280, 720);
			GraphicsHandler.BaseResolution = new Point(1280, 720);
			GraphicsHandler.FullScreen = true;
		}

		private void InitializeStates() {
			StateHandler.AddState("Title", new TitleScreen());
			StateHandler.AddState("Menu", new MainMenu());
			StateHandler.AddState("HostOnline", new HostOnlineMenu());
			StateHandler.AddState("HostLocal", new HostLocalMenu());
			StateHandler.AddState("Waiting", new WaitingMenu());
			StateHandler.AddState("Join", new JoinMenu());
			StateHandler.AddState("Play", new PlayingState());
		}

		private void LoadResources() {
			AudioHandler.SongVolume = 0.5f;
			AudioHandler.SoundEffectVolume = 0.6f;

			// Initialize resources
			AssetHandler.SetId("24x25@overworld", "overworld");
			AssetHandler.SetId("20x13@dungeon", "dungeon");
			AssetHandler.SetId("4x4@hero", "hero");

			// Chests
			AssetHandler.SetId("2x1@chest", "chest");
			AssetHandler.SetId("2x1@chest_key", "keychest");

			// Items
			AssetHandler.SetId("heart", "heart");
			AssetHandler.SetId("heart_half", "halfheart");
			AssetHandler.SetId("heart_empty", "emptyheart");
			AssetHandler.SetId("key", "key");
			AssetHandler.SetId("4x1@ingame_arrow", "arrow");
			AssetHandler.SetId("ingame_bomb", "bomb");

			// Menu Icons
			AssetHandler.SetId("menu_arrow", "menu_arrow");
			AssetHandler.SetId("menu_bomb", "menu_bomb");
			AssetHandler.SetId("menu_bow", "menu_bow");
			AssetHandler.SetId("doodskop", "skull");

			// Player animations
			AssetHandler.SetId("2x4@link_animation_blue", "player_blue");
			AssetHandler.SetId("4x1@link_animation_use_blue", "player_use_blue");
			AssetHandler.SetId("8x1@link_animation_sword_blue", "player_sword_blue");
			AssetHandler.SetId("8x1@link_animation_sword_red", "player_sword_red");
			AssetHandler.SetId("2x4@link_animation_red", "player_red");
			AssetHandler.SetId("4x1@link_animation_use_red", "player_use_red");

			AssetHandler.SetId("7x1@bomb_explosion", "explosion");
			AssetHandler.SetId("2x1@grass_animation_remove", "grass_cut");
			AssetHandler.SetId("4x1@monster_animation_die", "monster_die");

			//Tiles Replacement
			AssetHandler.SetId("16x16@tilesheet_full", "tilesheet");

			// Pressure Plates
			AssetHandler.SetId("2x1@pressureplate_circle", "pressureplate_circle");
			AssetHandler.SetId("2x1@pressureplate_square", "pressureplate_square");
			AssetHandler.SetId("2x1@pressureplate_triangle", "pressureplate_triangle");

			// Retractable Pillars
			AssetHandler.SetId("2x1@rpcircle", "retractablepillar_circle");
			AssetHandler.SetId("2x1@rpsquare", "retractablepillar_square");
			AssetHandler.SetId("2x1@rptriangle", "retractablepillar_triangle");

			AssetHandler.SetId("EndScreenOverlay", "yourewiner");
			AssetHandler.SetId("Controls_online_overlay", "online_overlay");
			AssetHandler.SetId("Controls_localmp_overlay", "local_overlay");

			//menu theme
			AssetHandler.SetId("Music\\menumusic", "menumusic");

			//ingame music
			AssetHandler.SetId("Music\\gamemusic", "gamemusic");
		}

		private static void Main() {
			Application.ThreadException += Application_ThreadException;
			Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
			AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

			MultiLink game = new MultiLink();
			game.Run();
		}

		private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e) {
			try {
				var exception = e.Exception != null ? e.Exception.Message + e.Exception.StackTrace : string.Empty;
				ShowError(exception);
			} finally {
				Application.Exit();
			}
		}

		private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
			try {
				var exception = e.ExceptionObject is Exception ? ((Exception)e.ExceptionObject).Message + ((Exception)e.ExceptionObject).StackTrace : string.Empty;
				ShowError(exception);
			} finally {
				Application.Exit();
			}
		}

		private static void ShowError(string e) {
			MessageBox.Show("Oops! A fatal error occurred. Please send the following information to the developers:\r\n\r\n" + e,
								"Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
		}
	}
}