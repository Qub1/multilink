﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using MultiLibrary.Generation;
using MultiLibrary.Handlers;
using MultiLibrary.Objects;
using MultiLibrary.Types;
using MultiLink.Objects;
using MultiLink.Objects.Rooms;
using MultiLink.Objects.Tiles;

namespace MultiLink.Worlds {
	public sealed class World : WorldObject {
		public bool GameWon;
		private readonly TextureGameObject overlay;
		private float overlayTimer = 10000f;
		private readonly int seed;
		private readonly bool tutorial;

		public World(int seed, bool tutorial) {
			this.seed = seed;
			this.tutorial = tutorial;
			List<Room> rooms = GetGeneratedWorld(seed, 12, tutorial);

			foreach (Room room in rooms) {
				AddChild(room);
				room.redrawStaticRender = true;
			}

			Player.ParentRoom = rooms.First();
			Player.Position = rooms.First().Position + new Vector2((float)((rooms.First().EntrancePosition.X - 1) * Room.UnitSize.X + (Room.UnitSize.X - 1) * .5) * Tile.UnitSize.X, (float)((rooms.First().EntrancePosition.Y - 1) * Room.UnitSize.Y + (Room.UnitSize.Y - 1) * .5) * Tile.UnitSize.Y);
			Player.RespawnPosition = Player.Position;
			Player2.ParentRoom = rooms.First();
			Player2.Position = rooms.First().Position + new Vector2((float)((rooms.First().EntrancePosition.X - 1) * Room.UnitSize.X + (Room.UnitSize.X - 1) * .5) * Tile.UnitSize.X, (float)((rooms.First().EntrancePosition.Y - 1) * Room.UnitSize.Y + (Room.UnitSize.Y - 1) * .5) * Tile.UnitSize.Y) + new Vector2(Tile.UnitSize.X, 0);
			Player2.RespawnPosition = Player2.Position;

			rooms.First().UnBlackout();

			if (MultiLink.MultiPlayer.IsOnline) {
				overlay = new TextureGameObject(new Vector2(256, 100), new Vector2(768, 432));
				overlay.Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("online_overlay"));
				overlay.Gui = true;
				overlay.Layer = 1f;
				AddChild(overlay);
			} else {
				overlay = new TextureGameObject(new Vector2(256, 100), new Vector2(768, 432));
				overlay.Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("local_overlay"));
				overlay.Gui = true;
				overlay.Layer = 1f;
				AddChild(overlay);
			}
		}

		public override void Update() {
			overlayTimer -= GameHandler.InfoHandler.GameTime.ElapsedGameTime.Milliseconds;
			if (overlayTimer <= 0) {
				overlay.Alpha -= .025f;
			}
			if (overlay != null) {
				if (overlay.Alpha <= 0) {
					RemoveChild(overlay);
				}
			}

			if (!GameWon) {
				// Check whether the player transitions from room
				bool insideroom = false;
				Room currentRoom = Player.ParentRoom;
				Rectangle bb = new Rectangle((int)currentRoom.Position.X, (int)currentRoom.Position.Y, (int)(currentRoom.Size.X * Tile.UnitX), (int)(currentRoom.Size.Y * Tile.UnitY));
				if (!bb.Contains(new Point((int)Player.Position.X, (int)Player.Position.Y))) {
					// Change room
					foreach (Room room in Rooms) {
						bb = new Rectangle((int)room.Position.X, (int)room.Position.Y, (int)(room.Size.X * Tile.UnitSize.X), (int)(room.Size.Y * Tile.UnitSize.Y));
						if (bb.Contains(new Point((int)Player.Position.X, (int)Player.Position.Y))) {
							Player.ParentRoom = room;
							Player.RespawnPosition = Player.Position;
							room.UnBlackout();
							insideroom = true;
							break;
						}
					}
				} else {
					insideroom = true;
				}

				if (!insideroom) {
					Player.Damage(9001);
				}

				insideroom = false;
				currentRoom = Player2.ParentRoom;
				bb = new Rectangle((int)currentRoom.Position.X, (int)currentRoom.Position.Y, (int)(currentRoom.Size.X * Tile.UnitX), (int)(currentRoom.Size.Y * Tile.UnitY));

				if (!bb.Contains(new Point((int)Player2.Position.X, (int)Player2.Position.Y))) {
					// Change room
					foreach (Room room in Rooms) {
						bb = new Rectangle((int)room.Position.X, (int)room.Position.Y, (int)(room.Size.X * Tile.UnitSize.X), (int)(room.Size.Y * Tile.UnitSize.Y));
						if (bb.Contains(new Point((int)Player2.Position.X, (int)Player2.Position.Y))) {
							Player2.ParentRoom = room;
							Player2.RespawnPosition = Player2.Position;
							room.UnBlackout();
							insideroom = true;
							break;
						}
					}
				} else {
					insideroom = true;
				}

				if (!insideroom) {
					Player2.Damage(9001);
				}

				//Check if the rooms are onscreen, if not make them inactive and invisible
				//Vector2 oldOffsetScale = GameHandler.GraphicsHandler.Camera.OffsetScale;
				//GameHandler.GraphicsHandler.Camera.OffsetScale = Vector2.One;
				Rectangle screenbox = new Rectangle((int)(GameHandler.GraphicsHandler.Camera.CameraOffset.X), (int)(GameHandler.GraphicsHandler.Camera.CameraOffset.Y), (int)(GameHandler.GraphicsHandler.Resolution.X / Scale.X), (int)(GameHandler.GraphicsHandler.Resolution.Y / Scale.Y));
				//GameHandler.GraphicsHandler.Camera.OffsetScale = oldOffsetScale;
				foreach (Room room in Rooms) {
					Rectangle rbb = new Rectangle((int)room.Position.X, (int)room.Position.Y, (int)(room.Size.X * Tile.UnitSize.X), (int)(room.Size.Y * Tile.UnitSize.Y));
					// Check if none of the players are in the room before disabling it
					if (screenbox.Intersects(rbb) || Player.ParentRoom == room || Player2.ParentRoom == room) {
						room.Active = true;
						//room.Visible = true;
					} else {
						room.Active = false;
						//room.Visible = false;
					}
				}

				// Reset player
				if (GameHandler.InputHandler.OnKeyDown(Keys.F1)) {
					/*if (Player.ParentRoom == Player2.ParentRoom) {
						if (MultiLink.MultiPlayer.IsHost) {
						GameHandler.GraphicsHandler.Camera.Target = Player.Position;
						GameHandler.GraphicsHandler.Camera.SecondTarget = Player2.Position;

							MultiLink.MultiPlayer.SendReloadRoom(Player.ParentRoom);
					}
					}*/
					//if (MultiLink.MultiPlayer.IsOnline) {
//						MultiLink.MultiPlayer.SendStart(seed, tutorial);
					//} else {
						////MultiLink.MultiPlayer.Start(seed, tutorial);
						Player.ParentRoom.Reload();
						Player.Respawn();
						Player2.Respawn();
						MultiLink.MultiPlayer.SendReloadRoom(Player.ParentRoom);
					//}
				}

				base.Update();
			} else {
				if (GameHandler.InputHandler.KeyDown(Keys.Escape)) {
					GameHandler.StateHandler.CurrentState = GameHandler.StateHandler.States["Title"];
				}
			}

			// Update world scale
			if (UpdateScale && !MultiLink.MultiPlayer.IsOnline) {
				float scale = MathHelper.Clamp(500f / (Player.Position - Player2.Position).Length(), 0f, 1f);
				Scale = new Vector2(scale, scale);
				GameHandler.GraphicsHandler.Camera.OffsetScale = Scale;
			}
		}

		public bool UpdateScale = true;

		public void WinGame() {
			GameWon = true;
			TextureGameObject winscreen = new TextureGameObject(Vector2.Zero, new Vector2(800, 600));
			winscreen.Sprite = new Sprite(GameHandler.AssetHandler.GetTexture("yourewiner"));
			winscreen.Gui = true;
			winscreen.Layer = .99f;
			AddChild(winscreen);
		}

		public Vector2 WorldSize {
			get {
				Vector2 largestSize = Vector2.Zero;
				foreach (Room room in Rooms) {
					if (room.Position.X + room.Size.X * Tile.UnitSize.X > largestSize.X) {
						largestSize.X = room.Position.X + room.Size.X * Tile.UnitSize.X;
					}
					if (room.Position.Y + room.Size.Y * Tile.UnitSize.Y > largestSize.Y) {
						largestSize.Y = room.Position.Y + room.Size.Y * Tile.UnitSize.Y;
					}
				}
				return largestSize;
			}
		}

		public Vector2 SmallestRoomPosition {
			get {
				Vector2 smallestPosition = Rooms[0].Position;
				foreach (Room room in Rooms) {
					if (room.Position.X < smallestPosition.X) {
						smallestPosition.X = room.Position.X;
					}
					if (room.Position.Y < smallestPosition.Y) {
						smallestPosition.Y = room.Position.Y;
					}
				}
				return smallestPosition;
			}
		}

		private List<Room> GetGeneratedWorld(int newSeed, int roomAmount, bool newTutorial) {
			List<Room> rooms = new List<Room>();
			List<AbstractRoom> generatedRooms = DungeonGenerator.GenerateDungeon(newSeed, roomAmount, newTutorial);

			foreach (AbstractRoom generatedRoom in generatedRooms) {
				rooms.Add(new Room(new Vector2((generatedRoom.Position.X - 12) * Room.UnitSize.X * Tile.UnitX, (generatedRoom.Position.Y - 12) * Room.UnitSize.Y * Tile.UnitY), new Vector2(generatedRoom.Size.X * Room.UnitSize.X, generatedRoom.Size.Y * Room.UnitSize.Y)));
				rooms.Last().LoadFromFile(generatedRoom.Path);
				if (generatedRoom.Rotation > 0) {
					rooms.Last().Rotate(generatedRoom.Rotation);
					rooms.Last().rotations = generatedRoom.Rotation;
				}

				rooms.Last().EntranceOrientation = generatedRoom.EntryDirection.ToString();
				rooms.Last().ExitOrientation = generatedRoom.ExitDirection.ToString();

				rooms.Last().EntrancePosition = generatedRoom.EntryPosition;
				rooms.Last().ExitPosition = generatedRoom.ExitPosition;
			}
			return rooms;
		}
	}
}