﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MultiLibrary.Handlers {
	/// <summary>
	/// Handles all input.
	/// </summary>
	public class InputHandler {
		/// <summary>
		/// All available mouse buttons.
		/// </summary>
		public enum MouseButton {
			Left,
			Middle,
			Right
		}

		private KeyboardState currentKeyboardState;
		private MouseState currentMouseState;
		private KeyboardState previousKeyboardState;
		private MouseState previousMouseState;

		/// <summary>
		/// A list of keys that are down.
		/// </summary>
		public List<Keys> KeysDown => new List<Keys>(currentKeyboardState.GetPressedKeys());

		/// <summary>
		/// A list of mouse buttons that are down.
		/// </summary>
		public List<MouseButton> MouseButtonsDown {
			get {
				List<MouseButton> result = new List<MouseButton>();

				if (MouseButtonDown(MouseButton.Left)) {
					result.Add(MouseButton.Left);
				}
				if (MouseButtonDown(MouseButton.Middle)) {
					result.Add(MouseButton.Middle);
				}
				if (MouseButtonDown(MouseButton.Right)) {
					result.Add(MouseButton.Right);
				}

				return result;
			}
		}

		/// <summary>
		/// The current position of the mouse.
		/// </summary>
		public Vector2 MousePosition => (new Vector2(currentMouseState.X, currentMouseState.Y)  / GameHandler.GraphicsHandler.Scale) / new Vector2(GameHandler.GraphicsHandler.Resolution.X, GameHandler.GraphicsHandler.Resolution.Y) * new Vector2(GameHandler.GraphicsHandler.BaseResolution.X, GameHandler.GraphicsHandler.BaseResolution.Y); // We'll need to correct for any scaling, and thus we divide by the drawing scale

		/// <summary>
		/// Gets whether any of the keys are down.
		/// </summary>
		/// <param name="keys">The keys to check.</param>
		/// <returns>Whether any of the keys are down.</returns>
		public bool AnyKeyDown(List<Keys> keys) {
			return keys.Any(KeyDown);
		}

		/// <summary>
		/// Gets whether any of the keys are up.
		/// </summary>
		/// <param name="keys">The keys to check.</param>
		/// <returns>Whether any of the keys are up.</returns>
		public bool AnyKeyUp(List<Keys> keys) {
			return keys.Any(KeyUp);
		}

		/// <summary>
		/// Gets whether the key is down.
		/// </summary>
		/// <param name="key">The key to check.</param>
		/// <returns>Whether the key is down.</returns>
		public bool KeyDown(Keys key) {
			return currentKeyboardState.IsKeyDown(key);
		}

		/// <summary>
		/// Gets whether the key is up.
		/// </summary>
		/// <param name="key">The key to check.</param>
		/// <returns>Whether the key is up.</returns>
		public bool KeyUp(Keys key) {
			return currentKeyboardState.IsKeyUp(key);
		}

		/// <summary>
		/// Gets whether the MouseButton is down.
		/// </summary>
		/// <param name="button">The MouseButton to check.</param>
		/// <returns>Whether the MouseButton is down.</returns>
		public bool MouseButtonDown(MouseButton button) {
			switch (button) {
				case MouseButton.Left:
					return currentMouseState.LeftButton == ButtonState.Pressed;
				case MouseButton.Middle:
					return currentMouseState.MiddleButton == ButtonState.Pressed;
				default:
					return currentMouseState.RightButton == ButtonState.Pressed;
			}
		}

		/// <summary>
		/// Gets whether the MouseButton is up.
		/// </summary>
		/// <param name="button">The MouseButton to check.</param>
		/// <returns>Whether the MouseButton is up.</returns>
		public bool MouseButtonUp(MouseButton button) {
			switch (button) {
				case MouseButton.Left:
					return currentMouseState.LeftButton != ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Pressed;
				case MouseButton.Middle:
					return currentMouseState.MiddleButton != ButtonState.Pressed && previousMouseState.MiddleButton == ButtonState.Pressed;
				default:
					return currentMouseState.RightButton != ButtonState.Pressed && previousMouseState.RightButton == ButtonState.Pressed;
			}
		}

		/// <summary>
		/// Gets whether key was just pressed down.
		/// </summary>
		/// <returns>Whether a key was just pressed down.</returns>
		public bool OnAKeyDown() {
			return KeysDown.Any(OnKeyDown);
		}

		/// <summary>
		/// Gets whether a key is being held down.
		/// </summary>
		/// <returns>Whether a key is being held down.</returns>
		public bool OnAKeyHold() {
			return KeysDown.Any(key => KeyDown(key) && previousKeyboardState.IsKeyDown(key));
		}

		/// <summary>
		/// Gets whether a key was just released.
		/// </summary>
		/// <returns>Whether a key was just released.</returns>
		public bool OnAKeyUp() {
			return previousKeyboardState.GetPressedKeys().Any(key => currentKeyboardState.IsKeyUp(key));
		}

		/// <summary>
		/// Gets whether any of the keys was just pressed down.
		/// </summary>
		/// <param name="keys">The keys to check.</param>
		/// <returns>Whether any of the keys was just pressed down.</returns>
		public bool OnAnyKeyDown(List<Keys> keys) {
			return keys.Any(OnKeyDown);
		}

		/// <summary>
		/// Gets whether any of the keys is being held down.
		/// </summary>
		/// <param name="keys">The keys to check.</param>
		/// <returns>Whether any of the keys is being held down.</returns>
		public bool OnAnyKeyHold(List<Keys> keys) {
			return keys.Any(OnKeyHold);
		}

		/// <summary>
		/// Gets whether any of the keys was just released.
		/// </summary>
		/// <param name="keys">The keys to check.</param>
		/// <returns>Whether any of the keys was just released.</returns>
		public bool OnAnyKeyUp(List<Keys> keys) {
			return keys.Any(OnKeyUp);
		}

		/// <summary>
		/// Gets whether the key was just pressed down.
		/// </summary>
		/// <param name="key">The key to check.</param>
		/// <returns>Whether the key was just pressed down.</returns>
		public bool OnKeyDown(Keys key) {
			return KeyDown(key) && previousKeyboardState.IsKeyUp(key);
		}

		/// <summary>
		/// Gets whether the key is being held down.
		/// </summary>
		/// <param name="key">The key to check.</param>
		/// <returns>Whether the key is being held down.</returns>
		public bool OnKeyHold(Keys key) {
			return KeyDown(key) && previousKeyboardState.IsKeyDown(key);
		}

		/// <summary>
		/// Gets whether the key was just released.
		/// </summary>
		/// <param name="key">The key to check.</param>
		/// <returns>Whether the key was just released.</returns>
		public bool OnKeyUp(Keys key) {
			return KeyUp(key) && previousKeyboardState.IsKeyDown(key);
		}

		/// <summary>
		/// Gets whether the MouseButton was just pressed down.
		/// </summary>
		/// <param name="button">The MouseButton to check.</param>
		/// <returns>Whether the MouseButton was just pressed down.</returns>
		public bool OnMouseButtonDown(MouseButton button) {
			switch (button) {
				case MouseButton.Left:
					return MouseButtonDown(button) && previousMouseState.LeftButton != ButtonState.Pressed;
				case MouseButton.Middle:
					return MouseButtonDown(button) && previousMouseState.MiddleButton != ButtonState.Pressed;
				default:
					return MouseButtonDown(button) && previousMouseState.RightButton != ButtonState.Pressed;
			}
		}

		/// <summary>
		/// Gets whether the MouseButton is being held down.
		/// </summary>
		/// <param name="button">The MouseButton to check.</param>
		/// <returns>Whether the MouseButton is being held down.</returns>
		public bool OnMouseButtonHold(MouseButton button) {
			switch (button) {
				case MouseButton.Left:
					return MouseButtonDown(button) && previousMouseState.LeftButton == ButtonState.Pressed;
				case MouseButton.Middle:
					return MouseButtonDown(button) && previousMouseState.MiddleButton == ButtonState.Pressed;
				default:
					return MouseButtonDown(button) && previousMouseState.RightButton == ButtonState.Pressed;
			}
		}

		/// <summary>
		/// Gets whether the MouseButton was just released.
		/// </summary>
		/// <param name="button">The MouseButton to check.</param>
		/// <returns>Whether the MouseButton was just released.</returns>
		public bool OnMouseButtonUp(MouseButton button) {
			switch (button) {
				case MouseButton.Left:
					return !MouseButtonDown(button) && previousMouseState.LeftButton == ButtonState.Pressed;
				case MouseButton.Middle:
					return !MouseButtonDown(button) && previousMouseState.MiddleButton == ButtonState.Pressed;
				default:
					return !MouseButtonDown(button) && previousMouseState.RightButton == ButtonState.Pressed;
			}
		}

		public void Update() {
			previousMouseState = currentMouseState;
			previousKeyboardState = currentKeyboardState;
			currentMouseState = Mouse.GetState();
			currentKeyboardState = Keyboard.GetState();
		}
	}
}