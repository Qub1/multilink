﻿using System.Collections.Generic;
using MultiLibrary.Types;

namespace MultiLibrary.Handlers {
	/// <summary>
	/// Handles different game states.
	/// </summary>
	public class StateHandler {
		private State currentState;
		private readonly Dictionary<string, State> states = new Dictionary<string, State>();

		/// <summary>
		/// The state that is currently active. Set to null to disable.
		/// </summary>
		public State CurrentState {
			get {
				return currentState;
			}
			set {
				// Only set the state if it has been loaded.
				if (States.ContainsValue(value)) {
					currentState = value;
				} else {
					throw new KeyNotFoundException("Could not find game state: " + value);
				}
			}
		}

		/// <summary>
		/// A list of all loaded states.
		/// </summary>
		public Dictionary<string, State> States => new Dictionary<string, State>(states);

		/// <summary>
		/// Adds a state to the StateHandler.
		/// </summary>
		/// <param name="id">The ID of the state to add.</param>
		/// <param name="state">The state to add.</param>
		public void AddState(string id, State state) {
			states.Add(id, state);
		}

		public void Draw() {
			if (CurrentState != null && CurrentState.Visible) {
				CurrentState.Draw();
			}
		}

		/// <summary>
		/// Removes a state from the StateHandler.
		/// </summary>
		/// <param name="id">The ID of the state to remove.</param>
		public void RemoveState(string id) {
			states.Remove(id);
		}

		public void Update() {
			if (CurrentState != null && CurrentState.Active) {
				CurrentState.Update();
			}
		}
	}
}