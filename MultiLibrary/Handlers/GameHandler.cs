using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace MultiLibrary.Handlers {
	/// <summary>
	/// Contains all objects that allow manipulation of aspects of the game.
	/// </summary>
	public class GameHandler : Game {
		/// <summary>
		/// Used for loading and retrieving assets.
		/// </summary>
		public static AssetHandler AssetHandler;

		/// <summary>
		/// Used for playing and managing audio.
		/// </summary>
		public static AudioHandler AudioHandler = new AudioHandler();

		/// <summary>
		/// Used for displaying and manipulating graphics.
		/// </summary>
		public static GraphicsHandler GraphicsHandler;

		/// <summary>
		/// Used to change information about the game, such as window title.
		/// </summary>
		public static InfoHandler InfoHandler;

		/// <summary>
		/// Used for handling input.
		/// </summary>
		public static InputHandler InputHandler = new InputHandler();

		/// <summary>
		/// Handles connections, port forwarding and network interaction.
		/// </summary>
		public static NetworkHandler NetworkHandler = new NetworkHandler();

		/// <summary>
		/// Used to switch between game states.
		/// </summary>
		public static StateHandler StateHandler = new StateHandler();

		/// <summary>
		/// Creates a new GameHandler.
		/// </summary>
		public GameHandler() {
			Content.RootDirectory = "Content";

			AssetHandler = new AssetHandler(Content);
			GraphicsHandler = new GraphicsHandler(new GraphicsDeviceManager(this));
			InfoHandler = new InfoHandler(Window);
		}

		protected override void Draw(GameTime gameTime) {
			base.Draw(gameTime);

			try {
				// Draw state
				GraphicsHandler.BeginDraw();
				StateHandler.Draw();
				GraphicsHandler.EndDraw();
			} catch (Exception e) {
				InfoHandler.Output("An error occurred while drawing! Error: \r\n" + e, InfoHandler.OutputType.Warning);
			}
		}

		/// <summary>
		/// Prepare graphics handler
		/// </summary>
		protected override void LoadContent() {
			base.LoadContent();

			GraphicsHandler.LoadContent();
		}

		protected override void Update(GameTime gameTime) {
			base.Update(gameTime);

			try {
				// Calculate TPS
				InfoHandler.Begin();

				// Update game time
				InfoHandler.GameTime = gameTime;

				// Update input
				InputHandler.Update();

				// Check fullscreen
				if (InputHandler.OnKeyDown(Keys.F11)) {
					GraphicsHandler.FullScreen = !GraphicsHandler.FullScreen;
				}

				// Update state
				StateHandler.Update();

				// Update ticks
				InfoHandler.End();
			} catch (Exception e) {
				InfoHandler.Output("An error occurred while updating! Error: \r\n" + e, InfoHandler.OutputType.Warning);
			}
		}
	}
}