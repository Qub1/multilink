﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using NATUPNPLib;

namespace MultiLibrary.Handlers {
	public class NetworkHandler {
		/// <summary>
		/// A list of incomming messages.
		/// </summary>
		public List<string> Inbox = new List<string>();

		/// <summary>
		/// Retrieves the external IP address of the machine.
		/// </summary>
		/// <returns>The external IP address in the form of xxx.xxx.xxx.xxx</returns>
		public IPAddress GetExternalIpAddress() {
			try {
				string url = "http://whatismyipaddress.com/";
				string ipRegex = @"\d*\.\d*\.\d*\.\d*";

				WebClient client = new WebClient();
				client.Headers.Add("user-agent", "Mozilla/5.0");
				string response = client.DownloadString(url);

				Regex r = new Regex(ipRegex);
				Match m = r.Match(response);
				if (m.Success) {
					return IPAddress.Parse(m.Value);
				}
				return GetLocalIpAddress();
			} catch (Exception) {
				return GetLocalIpAddress();
			}
		}

		/// <summary>
		/// Retrieves the local IP address of the machine within the network.
		/// </summary>
		/// <returns>The local IP address in the form of xxx.xxx.xxx.xxx</returns>
		public IPAddress GetLocalIpAddress() {
			IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (IPAddress ip in host.AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork)) {
				return ip;
			}
			throw new Exception("Local IP Address Not Found!");
		}

		/// <summary>
		/// Opens a port to this computer using UPnP.
		/// </summary>
		/// <param name="externalPort">The external port to open.</param>
		/// <param name="internalPort">The internal port to open.</param>
		/// <param name="description">The description of the program opening the port.</param>
		/// <param name="protocol">The protocol to use.</param>
		public void OpenPort(int externalPort, int internalPort, string description, string protocol = "TCP") {
			try {
				UPnPNAT upnpnat = new UPnPNAT();
				IStaticPortMappingCollection mappings = upnpnat.StaticPortMappingCollection;
				if (mappings != null) {
					try {
						mappings.Remove(externalPort, protocol);
					} catch (Exception e) {
						GameHandler.InfoHandler.Output("Error removing mapping of uPnP external port \"" + externalPort + "\" internal port \"" + internalPort + "\" protocol \"" + protocol + "\". Error:\n\r" + e, InfoHandler.OutputType.Warning);
					}
					mappings.Add(externalPort, protocol, internalPort, GetLocalIpAddress().ToString(), true, description);
				}
			} catch (Exception e) {
				GameHandler.InfoHandler.Output("Error mapping uPnP external port \"" + externalPort + "\" internal port \"" + internalPort + "\" protocol \"" + protocol + "\". Error:\n\r" + e, InfoHandler.OutputType.Warning);
			}
		}
	}
}