﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MultiLibrary.Helpers;

namespace MultiLibrary.Handlers {
	/// <summary>
	/// Handles graphics-related operations.
	/// </summary>
	public class GraphicsHandler {
		public Camera Camera;
		private DateTime drawStart = DateTime.Now;
		private int frames;
		public readonly GraphicsDeviceManager graphicsDeviceManager;
		private Point resolution = new Point(1920, 1080);
		private Vector2 scale;
		private Matrix matrix;
		private Rectangle screenbox;
		private SpriteBatch spriteBatch;
		public Point BaseResolution = new Point(1920, 1080);
		private bool drawing = false;

		/// <summary>
		/// The amount of frames per second.
		/// </summary>
		public int Fps {
			get;
			private set;
		}

		/// <summary>
		/// Switch the game between windowed and fullscreen.
		/// </summary>
		public bool FullScreen {
			get {
				return graphicsDeviceManager.IsFullScreen;
			}
			set {
				Vector2 newScale = new Vector2(ScreenSize.X / (float)Resolution.X, ScreenSize.Y / (float)Resolution.Y);
				float finalScale = 1F;

				if (value) {
					finalScale = newScale.X;
					if (Math.Abs(1 - newScale.Y) < Math.Abs(1 - newScale.X)) {
						finalScale = newScale.Y;
					}
				} else {
					if (newScale.X < 1f || newScale.Y < 1f) {
						finalScale = Math.Min(newScale.X, newScale.Y);
					}
				}

				BufferSize = new Point((int)(finalScale * Resolution.X), (int)(finalScale * Resolution.Y));
				graphicsDeviceManager.IsFullScreen = value;
				graphicsDeviceManager.ApplyChanges();
				Scale = new Vector2(Viewport.Width / (float)Resolution.X, Viewport.Height / (float)Resolution.Y);
			}
		}

		public Point BufferSize {
			get {
				return new Point(graphicsDeviceManager.PreferredBackBufferWidth, graphicsDeviceManager.PreferredBackBufferHeight);
			}
			set {
				graphicsDeviceManager.PreferredBackBufferWidth = value.X;
				graphicsDeviceManager.PreferredBackBufferHeight = value.Y;
				graphicsDeviceManager.ApplyChanges();
			}
		}

		/// <summary>
		/// The resolution at which to draw the game.
		/// </summary>
		public Point Resolution {
			get {
				return resolution;
			}
			set {
				resolution = value;

				// Refresh fullscreen
				FullScreen = FullScreen;
			}
		}

		/// <summary>
		/// The scale at which everything is drawn.
		/// </summary>
		public Vector2 Scale {
			get {
				return scale;
			}
			set {
				scale = value;

				matrix = Matrix.CreateScale(Scale.X, Scale.Y, 1);
			}
		}

		/// <summary>
		/// The actual size of the screen in pixels.
		/// </summary>
		public Point ScreenSize => new Point(GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height);

		/// <summary>
		/// The current visible portion of the game.
		/// </summary>
		public Rectangle Viewport {
			get {
				return new Rectangle(graphicsDeviceManager.GraphicsDevice.Viewport.X, graphicsDeviceManager.GraphicsDevice.Viewport.Y, graphicsDeviceManager.GraphicsDevice.Viewport.Width, graphicsDeviceManager.GraphicsDevice.Viewport.Height);
			}
			set {
				graphicsDeviceManager.GraphicsDevice.Viewport = new Viewport(value.X, value.Y, value.Width, value.Height);
			}
		}

		/// <summary>
		/// Creates a new GraphicsHandler.
		/// </summary>
		/// <param name="graphicsDeviceManager">The GraphicsDeviceManager to use.</param>
		public GraphicsHandler(GraphicsDeviceManager graphicsDeviceManager) {
			this.graphicsDeviceManager = graphicsDeviceManager;
		}

		/// <summary>
		/// Begins drawing
		/// </summary>
		public void BeginDraw() {
			if (!drawing) {
				drawing = true;
				// Clear screen
				Clear(Color.Black);

				screenbox = new Rectangle((int)Camera.CameraOffset.X, (int)Camera.CameraOffset.Y, resolution.X, resolution.Y);
				screenbox.Inflate(50, 50);
				// Calculate FPS
				if ((DateTime.Now - drawStart).TotalMilliseconds >= 1000) {
					Fps = frames;
					drawStart = DateTime.Now;
					frames = 0;
				}

				spriteBatch.Begin(SpriteSortMode.FrontToBack, null, SamplerState.PointClamp, null, null, null, matrix);
			}
		}

		public void BeginRenderTargetDraw() {
			// Clear screen
			spriteBatch.End();

			Clear(Color.Transparent);

			screenbox = new Rectangle((int)Camera.CameraOffset.X, (int)Camera.CameraOffset.Y, resolution.X, resolution.Y);
			screenbox.Inflate(50, 50);

			spriteBatch.Begin(SpriteSortMode.Texture, null, SamplerState.PointClamp, null, null);
		}

		/// <summary>
		/// Stops 2D drawing.
		/// </summary>
		public void EndDraw() {
			if (drawing) {
				spriteBatch.End();

				frames++;
				drawing = false;
			}
		}

		public void SetRenderTarget(RenderTarget2D rt) {
			graphicsDeviceManager.GraphicsDevice.SetRenderTarget(rt);
		}

		public void EndRenderTargetDraw() {
			spriteBatch.End();

			graphicsDeviceManager.GraphicsDevice.SetRenderTarget(null);

			BeginDraw();
		}


		/// <summary>
		/// Clears the screen to a certain color.
		/// </summary>
		/// <param name="color"></param>
		public void Clear(Color color) {
			graphicsDeviceManager.GraphicsDevice.Clear(color);
		}

		/// <summary>
		/// Draw2Ds some text on the screen.
		/// </summary>
		/// <param name="position">The position to draw the text at.</param>
		/// <param name="text">The text to draw.</param>
		/// <param name="font">The font of the text to draw.</param>
		/// <param name="color">The color of the text to draw.</param>
		/// <param name="effects">Effects to apply to the texture.</param>
		public void DrawText(Vector2 position, float rotation, Vector2 size, string text, SpriteFont font, Color? color = null, SpriteEffects effects = SpriteEffects.None, float layer = 0f, bool GUI = false) {
			Vector2 pos = position;
			if (!GUI)
				pos = position - Camera.CameraOffset;

			if (GUI) {
				pos = new Vector2(pos.X / BaseResolution.X * Resolution.X, pos.Y / BaseResolution.Y * Resolution.Y);
				size = new Vector2(size.X / BaseResolution.X * Resolution.X, size.Y / BaseResolution.Y * Resolution.Y);
			}

			spriteBatch.DrawString(font, text, pos, color ?? Color.White, rotation, Vector2.Zero, new Vector2(size.X / font.MeasureString(text).X, size.Y / font.MeasureString(text).Y), SpriteEffects.None, layer);
		}

		/// <summary>
		/// Draw2Ds a rectangle on the screen.
		/// </summary>
		/// <param name="position">The position to draw the rectangle at.</param>
		/// <param name="size">The size of the rectangle.</param>
		/// <param name="color">The color of the rectangle.</param>
		public void DrawRectangle(Vector2 position, Vector2 size, Color color, float layer = 0f) {
			DrawTexture(position, 0F, size, GetColoredPixel(color), null, null, SpriteEffects.None, layer);
		}

		/// <summary>
		/// Draw2Ds a box on the screen.
		/// </summary>
		/// <param name="position">The position to draw the box at.</param>
		/// <param name="size">The size of the box.</param>
		/// <param name="borderSize">The size of the border.</param>
		/// <param name="color">The color of the box.</param>
		/// <param name="borderColor">The color of the border of the box.</param>
		public void DrawSquare(Vector2 position, Vector2 size, float borderSize, Color color, Color borderColor) {
			// Top line
			DrawRectangle(position, new Vector2(size.X, borderSize), borderColor);

			// Bottom line
			DrawRectangle(new Vector2(position.X, position.Y + size.Y - borderSize), new Vector2(size.X, borderSize), borderColor);

			// Left line
			DrawRectangle(new Vector2(position.X, position.Y + borderSize), new Vector2(borderSize, size.Y - 2 * borderSize), borderColor);

			// Right line
			DrawRectangle(new Vector2(position.X + size.X - borderSize, position.Y + borderSize), new Vector2(borderSize, size.Y - 2 * borderSize), borderColor);

			// Center
			DrawRectangle(new Vector2(position.X + borderSize, position.Y + borderSize), new Vector2(size.X - 2 * borderSize, size.Y - 2 * borderSize), color);
		}

		/// <summary>
		/// Draw2Ds a texture on the screen.
		/// </summary>
		/// <param name="position">The position to draw the texture at.</param>
		/// <param name="rotation">How much to rotate the drawn texture.</param>
		/// <param name="size">The size of the texture.</param>
		/// <param name="texture">The texture to draw.</param>
		/// <param name="section">The part of the texture to draw.</param>
		/// <param name="tint">The tint to apply to the texture.</param>
		/// <param name="effects">Effects to apply to the texture.</param>
		/// <param name="layer">The layer of the texture.</param>
		/// <param name="gui">Whether to draw as a GUI element (on top of everything, fixed in place).</param>
		public void DrawTexture(Vector2 position, float rotation, Vector2 size, Texture2D texture, Rectangle? source = null, Color? tint = null, SpriteEffects effects = SpriteEffects.None, float layer = 0f, bool GUI = false) {
			//if (!screenbox.Contains(new Point((int)position.X, (int)position.Y))&&!GUI)
			//    return;

			if (source == null) {
				source = new Rectangle(0, 0, texture.Width, texture.Height);
			}

			Vector2 pos = position;
			if (!GUI)
				pos = position - Camera.CameraOffset;

			if (GUI) {
				pos = new Vector2(pos.X / BaseResolution.X * Resolution.X, pos.Y / BaseResolution.Y * Resolution.Y);
				size = new Vector2(size.X / BaseResolution.X * Resolution.X, size.Y / BaseResolution.Y * Resolution.Y);
			}

			spriteBatch.Draw(texture, pos, source ?? null, tint ?? Color.White, rotation, Vector2.Zero, new Vector2(size.X / source.Value.Width, size.Y / source.Value.Height), effects, layer);

		}

		/// <summary>
		/// Gets a 1x1 texture of the given color.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <returns>A colored 1x1 texture.</returns>
		public Texture2D GetColoredPixel(Color color) {
			return GetColoredTexture(new[] {
				color
			}, new Point(1, 1));
		}

		/// <summary>
		/// Gets a texture of the given colors.
		/// </summary>
		/// <param name="colorData">The colors.</param>
		/// <param name="size">The size of the texture.</param>
		/// <returns>A colored texture.</returns>
		public Texture2D GetColoredTexture(Color[] colorData, Point size) {
			Texture2D result = GetEmptyTexture(size);

			// Set color
			result.SetData(colorData);

			return result;
		}

		/// <summary>
		/// Gets an empty texture.
		/// </summary>
		/// <param name="size">The size of the texture.</param>
		/// <returns>An empty texture.</returns>
		public Texture2D GetEmptyTexture(Point size) {
			return new Texture2D(graphicsDeviceManager.GraphicsDevice, size.X, size.Y);
		}

		/// <summary>
		/// Gets the color data of a texture as a 2D grid, where the first dimension is the X coordinate in the texture and the
		/// second dimension is the Y coordinate.
		/// </summary>
		/// <param name="texture">The texture to use.</param>
		/// <returns>A two dimensional array containing the color data of the texture.</returns>
		public Color[,] GetGridColorData(Texture2D texture) {
			// Get original data
			Color[] data = new Color[texture.Width * texture.Height];
			texture.GetData(data);

			// Create new two dimensional array
			Color[,] gridData = new Color[texture.Width, texture.Height];

			// Order original data
			for (int x = 0; x < texture.Width; x++) {
				for (int y = 0; y < texture.Height; y++) {
					gridData[x, y] = data[x + y * texture.Width];
				}
			}

			return gridData;
		}

		/// <summary>
		/// Must be executed upon loading content.
		/// </summary>
		public void LoadContent() {
			spriteBatch = new SpriteBatch(graphicsDeviceManager.GraphicsDevice);
			FullScreen = false;

			Camera = new Camera();
		}

		/*
		/// <summary>
		/// Split the texture into a grid of evenly sized cells.
		/// </summary>
		/// <param name="texture">The texture to use.</param>
		/// <param name="dimensions">The rows and columns in the texture.</param>
		/// <param name="borderSize">The size of cell borders.</param>
		/// <returns>A two dimensional array containing the cells in the texture.</returns>
		public Texture2D[,] GetTextureCells(Texture2D texture, Point dimensions, float borderSize = 0F) {
			// Get texture color data
			Color[] data = new Color[texture.Width * texture.Height];
			texture.GetData(data);

			// Calculate cellsize
			Vector2 cellSize = new Vector2((float)texture.Width / dimensions.X, (float)texture.Height / dimensions.Y);

			// Loop the rows and columns
			Texture2D[,] result = new Texture2D[dimensions.X, dimensions.Y];
			for (int column = 0; column < dimensions.X; column++) {
				for (int row = 0; row < dimensions.Y; row++) {
					// Get current cell data
					result[column, row] = new Texture2D(texture.GraphicsDevice, (int)cellSize.X, (int)cellSize.Y);

					// Loop over color data of cell
					Color[] cellData = new Color[(int)(cellSize.X * cellSize.Y)];
					int index = 0;
					Vector2 currentPosition = new Vector2(column * cellSize.X + (column + 1) * borderSize, row * cellSize.Y + (row + 1) * borderSize);

					// Y must be read first, since the resulting one dimensional array exists of the rows appended to each other
					for (int y = (int)currentPosition.Y; y < (int)currentPosition.Y + cellSize.Y; y++) {
						for (int x = (int)currentPosition.X; x < (int)currentPosition.X + cellSize.X; x++) {
							cellData[index] = data[x + y * texture.Width];
							index++;
						}
					}

					result[column, row].SetData(cellData);
				}
			}

			return result;
		}

		/// <summary>
		/// Gets a section of the texture.
		/// </summary>
		/// <param name="texture">The texture to use.</param>
		/// <param name="section">The section to get.</param>
		/// <returns>The section of the texture.</returns>
		public Texture2D GetTextureSection(Texture2D texture, Rectangle section) {
			// Get texture color data
			Color[] data = new Color[texture.Width * texture.Height];
			texture.GetData(data);

			Texture2D result = new Texture2D(texture.GraphicsDevice, section.Width, section.Height);
			Color[] sectionData = new Color[section.Width * section.Height];
			int index = 0;
			for (int y = section.Y; y < section.Y + section.Height; y++) {
				for (int x = section.X; x < section.X + section.Width; x++) {
					sectionData[index] = data[x + y * texture.Width];
					index++;
				}
			}

			result.SetData(sectionData);

			return result;
		}
        */
	}
}