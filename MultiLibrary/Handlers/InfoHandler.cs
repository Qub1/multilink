﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using MultiLibrary.Objects;

namespace MultiLibrary.Handlers {
	/// <summary>
	/// Handles game information.
	/// </summary>
	public class InfoHandler {
		/// <summary>
		/// Latest GameTime.
		/// </summary>
		public GameTime GameTime;

		public Dictionary<int, GameObject> GlobalGameObjects = new Dictionary<int, GameObject>();
		public int NextId;

		/// <summary>
		/// Used to generate random numbers.
		/// </summary>
		public Random Random = new Random();

		private readonly GameWindow gameWindow;
		private int ticks;
		private DateTime tickStart = DateTime.Now;

		/// <summary>
		/// The window title.
		/// </summary>
		public string Title {
			get {
				return gameWindow.Title;
			}
			set {
				gameWindow.Title = value;
			}
		}

		/// <summary>
		/// The amount of ticks per second.
		/// </summary>
		public int Tps {
			get;
			private set;
		}

		/// <summary>
		/// Creates a new InfoHandler.
		/// </summary>
		/// <param name="gameWindow">The game window.</param>
		public InfoHandler(GameWindow gameWindow) {
			this.gameWindow = gameWindow;

			Thread t = new Thread(HandleOutput) {
				IsBackground = true
			};
			t.Start();
		}

		public void Begin() {
			// Update TPS
			if ((DateTime.Now - tickStart).TotalMilliseconds >= 1000) {
				Tps = ticks;
				tickStart = DateTime.Now;
				ticks = 0;
			}
		}

		public void End() {
			// Update TPS
			ticks++;
		}

		/// <summary>
		/// Adds a GameObject and sets its ID.
		/// </summary>
		/// <param name="gameObject">The GameObject to add.</param>
		internal void AddGameObject(GameObject gameObject) {
			if (!GlobalGameObjects.ContainsValue(gameObject)) {
				gameObject.Id = NextId;
				GlobalGameObjects.Add(NextId, gameObject);
				NextId++;
			}
		}

		/// <summary>
		/// Removes a GameObject.
		/// </summary>
		/// <param name="gameObject">The GameObject to remove.</param>
		internal void RemoveGameObject(GameObject gameObject) {
			int key = 0;
			foreach (KeyValuePair<int, GameObject> item in GlobalGameObjects.Where(item => item.Value == gameObject).ToList()) {
				key = item.Key;
				break;
			}
			GlobalGameObjects.Remove(key);
		}

		private List<string> output = new List<string>(); 

		public enum OutputType {
			Info,
			Warning,
			Error
		}

		public void Output(string text, OutputType type = OutputType.Info) {
			string message = "";
			switch (type) {
				case OutputType.Info:
					message += "[INFORMATION] ";
					break;
				case OutputType.Warning:
					message += "[WARNING]     ";
					break;
				case OutputType.Error:
					message += "[ERROR]       ";
					break;
			}
			message += text;

			lock(output) {
				output.Add(message);
			}
		}

		private void HandleOutput() {
			while (true) {
				while (output.Count > 0) {
					string message = "[" + DateTime.Now.ToString() + "] " + output[0];
					Debug.Print(message);
					output.RemoveAt(0);
				}
			}
		}
	}
}