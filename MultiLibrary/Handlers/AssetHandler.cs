﻿using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace MultiLibrary.Handlers {
	/// <summary>
	/// Handles loading and retrieving of game assets.
	/// </summary>
	public class AssetHandler {
		private readonly ContentManager contentManager;
		private readonly Dictionary<string, string> ids = new Dictionary<string, string>();
		private readonly Dictionary<string, Song> songs = new Dictionary<string, Song>();
		private readonly Dictionary<string, SoundEffect> soundEffects = new Dictionary<string, SoundEffect>();
		private readonly Dictionary<string, SpriteFont> spriteFonts = new Dictionary<string, SpriteFont>();
		private readonly Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();

		/// <summary>
		/// Creates a new AssetManager.
		/// </summary>
		/// <param name="contentManager">The ContentManager to use.</param>
		public AssetHandler(ContentManager contentManager) {
			this.contentManager = contentManager;
		}

		/// <summary>
		/// Gets the amount of files in the specified content directory.
		/// </summary>
		/// <param name="path">The path of the directory.</param>
		/// <returns>The amount of files.</returns>
		public int CountFiles(string path) {
			return Directory.GetFiles(contentManager.RootDirectory + "/" + path).Length;
		}

		/// <summary>
		/// Gets a Song. If the Song has been loaded before, that instance will be used, otherwise a new instance will be loaded.
		/// </summary>
		/// <param name="name">The name of the Song.</param>
		/// <returns>The Song.</returns>
		public Song GetSong(string name) {
			// Check if an ID exists
			if (ids.ContainsKey(name)) {
				name = ids[name];
			}

			// Load if it hasn't been loaded yet
			if (!songs.ContainsKey(name)) {
				songs[name] = contentManager.Load<Song>(name);
			}

			return songs[name];
		}

		/// <summary>
		/// Gets a SoundEffect. If the SoundEffect has been loaded before, that instance will be used, otherwise a new instance
		/// will be loaded.
		/// </summary>
		/// <param name="name">The name of the SoundEffect.</param>
		/// <returns>The SoundEffect.</returns>
		public SoundEffect GetSoundEffect(string name) {
			// Check if an ID exists
			if (ids.ContainsKey(name)) {
				name = ids[name];
			}

			// Load if it hasn't been loaded yet
			if (!soundEffects.ContainsKey(name)) {
				soundEffects[name] = contentManager.Load<SoundEffect>(name);
				soundEffects[name].Name = name;
			}

			return soundEffects[name];
		}

		/// <summary>
		/// Gets a SpriteFont. If the SpriteFont has been loaded before, that instance will be used, otherwise a new instance will
		/// be loaded.
		/// </summary>
		/// <param name="name">The name of the SpriteFont.</param>
		/// <returns>The SpriteFont.</returns>
		public SpriteFont GetSpriteFont(string name) {
			// Check if an ID exists
			if (ids.ContainsKey(name)) {
				name = ids[name];
			}

			// Load if it hasn't been loaded yet
			if (!spriteFonts.ContainsKey(name)) {
				spriteFonts[name] = contentManager.Load<SpriteFont>(name);
			}

			return spriteFonts[name];
		}

		/// <summary>
		/// Gets a texture. If the texture has been loaded before, that instance will be used, otherwise a new instance will be
		/// loaded.
		/// </summary>
		/// <param name="name">The name of the texture.</param>
		/// <returns>The texture.</returns>
		public Texture2D GetTexture(string name) {
			// Check if an ID exists
			if (ids.ContainsKey(name)) {
				name = ids[name];
			}

			// Load if it hasn't been loaded yet
			if (!textures.ContainsKey(name)) {
				textures[name] = contentManager.Load<Texture2D>(name);
				textures[name].Name = name;
			}

			return textures[name];
		}

		/// <summary>
		/// Gets the contents of a resource file.
		/// </summary>
		/// <param name="path">The path of the file, relative to the game.</param>
		/// <returns>The file's contents.</returns>
		public string ReadFile(string path) {
			StreamReader reader = new StreamReader(contentManager.RootDirectory + "/" + path);
			string result = reader.ReadToEnd();
			reader.Close();

			return result;
		}

		/// <summary>
		/// Assigns an ID to the specified resource, so that it may be loaded quicker later on.
		/// </summary>
		/// <param name="name">The name of the resource.</param>
		/// <param name="id">The ID to assign.</param>
		public void SetId(string name, string id) {
			ids[id] = name;
		}

		/// <summary>
		/// Writes text to a file.
		/// </summary>
		/// <param name="path">The path of the file, relative to the game.</param>
		/// <param name="text">The contents to write.</param>
		/// <param name="append">Whether to append to the end or overwrite.</param>
		public void WriteFile(string path, string text, bool append) {
			StreamWriter writer = new StreamWriter(contentManager.RootDirectory + "/" + path, append);
			writer.Write(text);
			writer.Close();
		}
	}
}