﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MultiLibrary.Handlers;

namespace MultiLibrary.Types {
	/// <summary>
	/// A representation of a texture, part of a spritesheet or animation.
	/// </summary>
	public class Sprite {
		/// <summary>
		/// Whether the Sprite is animated. When animated, the index is automatically incremented across the dimensions of the
		/// texture.
		/// </summary>
		public bool Animated;

		/// <summary>
		/// The amount of time per animation frame.
		/// </summary>
		public TimeSpan AnimationFrameTime = new TimeSpan();

		/// <summary>
		/// The amount of cells used for the animation from the start index.
		/// </summary>
		public int AnimationLength = 1;

		/// <summary>
		/// The index to start animation from.
		/// </summary>
		public Point AnimationStartIndex = new Point(0, 0);

		/// <summary>
		/// The size of grid lines used in the image (if any). Used when dividing the image into cells.
		/// </summary>
		public float CellBorderSize = 0F;

		/// <summary>
		/// Whether the animation should loop all cells instead of just one single pass.
		/// </summary>
		public bool LoopAnimation = true;

		private DateTime animationLastFrame = DateTime.Now;
		private Point dimensions = new Point(1, 1);
		private Texture2D texture = GameHandler.GraphicsHandler.GetColoredPixel(Color.Transparent);

		/// <summary>
		/// The size of one single cell in the Texture.
		/// </summary>
		public Vector2 CellSize => new Vector2((float)Texture.Width / Dimensions.X, (float)Texture.Height / Dimensions.Y);

		/// <summary>
		/// The current cell of the Texture.
		/// </summary>
		public Texture2D Current {
			get;
			private set;
		}

		/// <summary>
		/// The index of the current cell of the Texture to use.
		/// The amount of cells in rows and columns are defined by the Dimensions.
		/// </summary>
		public Point CurrentIndex {
			get;
			set;
		} = new Point(0, 0);

		/// <summary>
		/// The amount of images contained in the Texture, horizontally and vertically.
		/// Used for spritesheets and animations.
		/// </summary>
		public Point Dimensions {
			get {
				return dimensions;
			}
			set {
				dimensions = value;

				CurrentIndex = new Point(0, 0);
				AnimationStartIndex = new Point(0, 0);
			}
		}

		public Rectangle Source {
			get {
				Vector2 cs = CellSize;
				return new Rectangle((int)(CurrentIndex.X * cs.X), (int)(CurrentIndex.Y * cs.Y), (int)cs.X, (int)cs.Y);
			}
		}

		/// <summary>
		/// The texture used in this sprite.
		/// </summary>
		public Texture2D Texture {
			get {
				return texture;
			}
			set {
				texture = value;

				// Check if the texture has dimensions
				if (Texture.Name.Contains("@")) {
					string[] splitName = Texture.Name.Split('@');
					string[] splitDimensions = splitName[0].Split('x');

					// Set dimensions
					Dimensions = new Point(int.Parse(splitDimensions[0]), int.Parse(splitDimensions[1]));
				} else {
					Dimensions = new Point(1, 1);
				}
			}
		}

		/// <summary>
		/// Creates a new Sprite.
		/// </summary>
		/// <param name="texture">The texture used in the sprite.</param>
		public Sprite(Texture2D texture = null) {
			Texture = texture ?? GameHandler.GraphicsHandler.GetEmptyTexture(new Point(1, 1));
		}

		public void Update() {
			if (Animated) {
				Point nextIndex = CurrentIndex;

				if (DateTime.Now - animationLastFrame >= AnimationFrameTime) {
					// Increase X
					nextIndex = new Point(CurrentIndex.X + 1, CurrentIndex.Y);

					// Check if X is out of bounds
					if (nextIndex.X > Dimensions.X - 1) {
						nextIndex = new Point(0, nextIndex.Y + 1);
					}

					animationLastFrame = DateTime.Now;
				}

				// Reset animation if it is out of bounds
				int index = nextIndex.Y * Dimensions.X + nextIndex.X;
				int startIndex = AnimationStartIndex.Y * Dimensions.X + AnimationStartIndex.X;
				int endIndex = startIndex + AnimationLength - 1;
				if (index < startIndex || index > endIndex) {
					// Check if looping
					if (LoopAnimation) {
						// Reset animation
						nextIndex = AnimationStartIndex;
					} else {
						// Stop animation
						nextIndex = CurrentIndex;
						Animated = false;
					}
				}

				CurrentIndex = nextIndex;
			}
		}
	}
}