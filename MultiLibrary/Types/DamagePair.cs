﻿using MultiLibrary.Objects;

namespace MultiLibrary.Types {
	public class DamagePair {
		public int Damage;
		public GameObject Object;

		public DamagePair(GameObject o, int d) {
			Object = o;
			Damage = d;
		}
	}
}