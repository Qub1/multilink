﻿using Microsoft.Xna.Framework;

namespace MultiLibrary.Generation {
	public class AbstractRoom {
		public char EntryDirection,
					ExitDirection;

		public Point EntryPosition,
					 ExitPosition;

		public string Path;
		public Point Position;
		public int Rotation;
		public Point Size;
		public string Type;

		public AbstractRoom(Point size, Point entryPosition, char entryDirection, Point exitPosition, char exitDirection, string path) {
			Size = size;
			EntryPosition = entryPosition;
			EntryDirection = entryDirection;
			ExitPosition = exitPosition;
			ExitDirection = exitDirection;
			Path = path;
			Rotation = 0;
		}

		public char RotateDirectionRight(char oldDirection) {
			char newDirection = '0';

			if (oldDirection == 'n') {
				newDirection = 'e';
			} else if (oldDirection == 'e') {
				newDirection = 's';
			} else if (oldDirection == 's') {
				newDirection = 'w';
			} else if (oldDirection == 'w') {
				newDirection = 'n';
			}

			return newDirection;
		}

		public Point RotatePositionRight(Point size, Point position) {
			Point newPosition = new Point(0, 0);
			Point oldPosition = position;
			Point newSize = size;
			Point oldSize = new Point(size.Y, size.X);

			if (oldPosition.X == 1) {
				newPosition.Y = 1;
			} else if (oldPosition.X != 1 && oldPosition.X != oldSize.X) {
				newPosition.Y = oldPosition.X;
			} else if (oldPosition.X == oldSize.X) {
				newPosition.Y = newSize.Y;
			}

			if (oldPosition.Y == 1) {
				newPosition.X = newSize.X;
			} else if (oldPosition.Y != 1 && oldPosition.Y != oldSize.Y) {
				newPosition.X = newSize.X - (oldPosition.Y - 1);
			} else if (oldPosition.Y == oldSize.Y) {
				newPosition.X = 1;
			}

			return newPosition;
		}

		// Rotate the room for lining up exit and entry
		public void RotateRight() {
			// Flip Size
			Size = new Point(Size.Y, Size.X);

			// Rotate Exit and Entry Direction
			EntryDirection = RotateDirectionRight(EntryDirection);
			ExitDirection = RotateDirectionRight(ExitDirection);

			// Rotate Exit and Entry Position
			EntryPosition = RotatePositionRight(Size, EntryPosition);
			ExitPosition = RotatePositionRight(Size, ExitPosition);
			if (Rotation < 3) {
				Rotation += 1;
			} else {
				Rotation = 0;
			}
		}
	}
}