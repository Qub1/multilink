﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Xna.Framework;

namespace MultiLibrary.Generation {
	public class DungeonGenerator {
		private static List<AbstractRoom> coreRooms,
										  entryRooms,
										  exitRooms,
										  tutorialRooms;

		private static List<AbstractRoom> generatedRooms;
		private static bool generating;

		private static int maxRooms,
						   roomNumber;

		private static Random random;
		private static bool[,] roomUnitGrid;

		public static List<AbstractRoom> GenerateDungeon(int seed, int rooms, bool tutorial) {
            random = new Random(seed);

            string projectDirectory = Application.StartupPath;
			string puzzleDirectory = Path.Combine(projectDirectory, @"Content\Rooms");

			// Generating loop to restart if it fails
			generating = true;
			while (generating) {
				generating = false;
                
				// Adding all rooms to lists
				coreRooms = new List<AbstractRoom>();
				coreRooms = CreateListOfAllRooms(Path.Combine(puzzleDirectory, @"Puzzles"));
				entryRooms = new List<AbstractRoom>();
				entryRooms = CreateListOfAllRooms(Path.Combine(puzzleDirectory, @"Entry"));
				exitRooms = new List<AbstractRoom>();
				exitRooms = CreateListOfAllRooms(Path.Combine(puzzleDirectory, @"Exit"));
				tutorialRooms = new List<AbstractRoom>();
				tutorialRooms = CreateListOfAllRooms(Path.Combine(puzzleDirectory, @"Tutorial"));

				// Initial Values
				roomNumber = 1;
				maxRooms = rooms;

				// Initialize list for generated rooms
				generatedRooms = new List<AbstractRoom>();

				// Initialize grid for collision checks
				roomUnitGrid = new bool[50, 50];

				// Adding the rooms in sequence
				AddEntryRoom();
				if (tutorial) {
					AddTutorialRooms();
				}
				while (roomNumber < maxRooms) {
					AddCoreRoom();
				}
				AddExitRoom();
			}
			// Return the list of rooms
			return generatedRooms;
		}


		private static void AddCoreRoom() {
			// Choose a random room from the list
			int listNumber = random.Next(0, coreRooms.Count);

			TryAddRoom(coreRooms, listNumber, "core");
			coreRooms.RemoveAt(listNumber);
		}

		private static void AddEntryRoom() {
			TryAddRoom(entryRooms, 0, "entry");
		}

		private static void AddExitRoom() {
			TryAddRoom(exitRooms, 0, "exit");
		}

        private static void AddTutorialRooms() {
            for (var i = 0; i < tutorialRooms.Count; i++) {
                TryAddRoom(tutorialRooms, i, "tutorial");
            }
        }

        private static void TryAddRoom(List<AbstractRoom> roomList, int roomListNumber, string type) {
            Point newRoomPosition;
            if (generatedRooms.Count() == 0) {
                for (var i = random.Next(0, 4); i < 3; i++) {
                    roomList.ElementAt(roomListNumber).RotateRight();
                }
                newRoomPosition = new Point(roomUnitGrid.GetLength(0) / 2, roomUnitGrid.GetLength(1) / 2);
            } else {
                Rotate(generatedRooms.Last(), roomList.ElementAt(roomListNumber));
                newRoomPosition = GetNewPosition(generatedRooms.Last(), roomList.ElementAt(roomListNumber));
            }
            if (!PositionCheck(newRoomPosition, roomList.ElementAt(roomListNumber))) {
                generating = true;
            } else {
                AddRoom(newRoomPosition, type, roomList.ElementAt(roomListNumber));
            }
            roomNumber += 1;
        }

        private static void AddRoom(Point position, string type, AbstractRoom room) {
            // Adding the room to the generated rooms list
            generatedRooms.Add(room);
            generatedRooms.Last().Position = position;
            generatedRooms.Last().Type = type;

            // Adding the room to the grid for collision checking
            for (var x = 0; x < room.Size.X; x++) {
                for (var y = 0; y < room.Size.Y; y++) {
                    roomUnitGrid[position.X + x, position.Y + y] = true;
                }
            }
        }


        // Add all rooms at a certain path to a list
        private static List<AbstractRoom> CreateListOfAllRooms(string path) {
			List<AbstractRoom> rooms = new List<AbstractRoom>();

			string[] paths = Directory.GetFiles(path, "*.txt");

			// Creating a room for each file
			foreach (string roomPath in paths) {
				char[] splitChars = {
					','
				};

				string[] roomSize = File.ReadLines(roomPath).First().Split(splitChars);
				Point size = new Point(int.Parse(roomSize.First()), int.Parse(roomSize.Skip(1).Take(1).First()));

				Point entryPosition;
				char entryDirection;
				if (File.ReadLines(roomPath).Skip(1).Take(1).First() != "") {
					string[] roomEntry = File.ReadLines(roomPath).Skip(1).Take(1).First().Split(splitChars);
					entryPosition = new Point(int.Parse(roomEntry.First()), int.Parse(roomEntry.Skip(1).Take(1).First()));
					entryDirection = char.Parse(roomEntry.Skip(2).Take(1).First());
				} else {
					entryPosition = new Point(0, 0);
					entryDirection = '0';
				}

				Point exitPosition;
				char exitDirection;
				if (File.ReadLines(roomPath).Skip(2).Take(1).First() != "") {
					string[] roomExit = File.ReadLines(roomPath).Skip(2).Take(1).First().Split(splitChars);
					exitPosition = new Point(int.Parse(roomExit.First()), int.Parse(roomExit.Skip(1).Take(1).First()));
					exitDirection = char.Parse(roomExit.Skip(2).Take(1).First());
				} else {
					exitPosition = new Point(0, 0);
					exitDirection = '0';
				}

				rooms.Add(new AbstractRoom(size, entryPosition, entryDirection, exitPosition, exitDirection, roomPath));
			}
			return rooms;
		}

		// Gets the new position from the exit of the last room and the entry position of the new room
		private static Point GetNewPosition(AbstractRoom lastRoom, AbstractRoom newRoom) {
			Point newRoomPosition = new Point(lastRoom.Position.X + (lastRoom.ExitPosition.X - 1) - (newRoom.EntryPosition.X - 1), lastRoom.Position.Y + (lastRoom.ExitPosition.Y - 1) - (newRoom.EntryPosition.Y - 1));
			if (lastRoom.ExitDirection == 'n') {
				newRoomPosition.Y -= 1;
			}
			if (lastRoom.ExitDirection == 'e') {
				newRoomPosition.X += 1;
			}
			if (lastRoom.ExitDirection == 's') {
				newRoomPosition.Y += 1;
			}
			if (lastRoom.ExitDirection == 'w') {
				newRoomPosition.X -= 1;
			}

			return newRoomPosition;
		}

		// Check if a position in the grid is free
		private static bool PositionCheck(Point position, AbstractRoom room) {
			for (var x = 0; x < room.Size.X; x++) {
				for (var y = 0; y < room.Size.Y; y++) {
					if (position.X + x >= 0 && position.X + x < roomUnitGrid.GetLength(0) && position.Y + y >= 0 && position.Y + y < roomUnitGrid.GetLength(1)) {
						if (roomUnitGrid[position.X + x, position.Y + y]) {
							return false;
						}
					} else {
						return false;
					}
				}
			}
			return true;
		}

		// Rotates the new room untill it lines up with the last room
		private static void Rotate(AbstractRoom lastRoom, AbstractRoom newRoom) {
			while ((lastRoom.ExitDirection != 'n' || newRoom.EntryDirection != 's') && 
                   (lastRoom.ExitDirection != 'e' || newRoom.EntryDirection != 'w') && 
                   (lastRoom.ExitDirection != 's' || newRoom.EntryDirection != 'n') && 
                   (lastRoom.ExitDirection != 'w' || newRoom.EntryDirection != 'e')) {
				newRoom.RotateRight();
			}
		}
	}
}