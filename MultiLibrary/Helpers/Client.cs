﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using MultiLibrary.Handlers;

namespace MultiLibrary.Helpers {
	public class Client {
		private TcpClient client;
		private Action<string> messageHandler;
		private readonly List<string> outbox = new List<string>();
		private StreamReader reader;
		//private string dataLeft = "";
		private bool stop;
		private NetworkStream stream;
		private StreamWriter writer;

		public bool Started {
			get;
			private set;
		}

		public Client(string server, int port, Action<string> messageHandler) {
			Start(server, port, messageHandler);
		}

		/// <summary>
		/// Broadcasts the message to all others on the server.
		/// </summary>
		/// <param name="message"></param>
		public void BroadcastMessage(string message) {
			lock(outbox) {
				outbox.Add(message);
			}
		}

		public void Start(string server, int port, Action<string> messageProcessor) {
			if (!Started) {
				stop = false;
				GameHandler.InfoHandler.Output("[CLIENT] Starting thread...");
				Thread t = new Thread(() => StartClient(server, port, messageProcessor)) {
					IsBackground = true
				};
				t.Start();
				GameHandler.InfoHandler.Output("[CLIENT] Thread started!");
				Started = true;
			}
		}

		public void Stop() {
			GameHandler.InfoHandler.Output("[CLIENT] Stopping client...");
			stop = true;
			Started = false;
			client.Close();
		}

		private void StartClient(string server, int port, Action<string> messageProcessor) {
			messageHandler = messageProcessor;

			// Connect
			try {
				GameHandler.InfoHandler.Output("[CLIENT] Connecting to server at IP \"" + server + "\" and port \"" + port + "\"...");
				client = new TcpClient(server, port);
				GameHandler.InfoHandler.Output("[CLIENT] Connected to server at IP \"" + server + "\" and port \"" + port + "\"!");
			} catch (Exception e) {
				GameHandler.InfoHandler.Output("[CLIENT] Client couldn't start! Error:\r\n" + e, InfoHandler.OutputType.Error);
				return;
			}

			stream = client.GetStream();
			reader = new StreamReader(stream);
			writer = new StreamWriter(stream);

			Thread t = new Thread(StartClientReader) {
				IsBackground = true
			};
			t.Start();

			Thread t2 = new Thread(StartClientWriter) {
				IsBackground = true
			};
			t2.Start();
		}

		private void StartClientReader() {
			while (!stop) {
				try {
					// Receive data
					string data = reader.ReadLine();
					GameHandler.InfoHandler.Output("[CLIENT] Received data \"" + data + "\"!");
					messageHandler(data);
				} catch (IOException e) {
					GameHandler.InfoHandler.Output("[CLIENT] Error:\r\n" + e, InfoHandler.OutputType.Error);

					Stop();
				}
			}

			StopClient();
		}

		private void StartClientWriter() {
			while (!stop) {
				try {
					// Send data
					if (outbox.Count > 0) {
						string message = outbox[0];
						GameHandler.InfoHandler.Output("[CLIENT] Sending data \"" + message + "\" to server...");
						writer.WriteLine(message);
						writer.Flush();
						stream.Flush();
						outbox.RemoveAt(0);
						GameHandler.InfoHandler.Output("[CLIENT] Sent data \"" + message + "\" to server!");
					}
				} catch (IOException e) {
					GameHandler.InfoHandler.Output("[CLIENT] Error:\r\n" + e, InfoHandler.OutputType.Error);

					Stop();
				}
			}

			StopClient();
		}

		private void StopClient() {
			stream.Flush();
			writer.Flush();
			reader.Close();

			GameHandler.InfoHandler.Output("[CLIENT] Client stopped!");
		}
	}
}