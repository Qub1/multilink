﻿using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;

namespace MultiLibrary.Helpers {
	/// <summary>
	/// A camera to control the 2D view
	/// </summary>
	public class Camera {
		/// <summary>
		/// The target that the camera will be centered on
		/// </summary>
		public Vector2 Target = Vector2.Zero;
		public Vector2 SecondTarget = Vector2.Zero;
		public Vector2 OffsetScale = Vector2.One;

		/// <summary>
		/// The position of the camera
		/// </summary>
		public Vector2 CameraOffset {
			get {
				if (UseOffset) {
					Vector2 target1 = Target * OffsetScale;
					Vector2 target2 = SecondTarget * OffsetScale;

					Vector2 result = -(new Vector2(GameHandler.GraphicsHandler.Resolution.X, GameHandler.GraphicsHandler.Resolution.Y) / 2f) + (.5f * (target1 + target2));

					return result;
				}
				return Vector2.Zero;
			}
		}

		public bool UseOffset = true;

		public void CenterTarget() {
			Target = new Vector2(GameHandler.GraphicsHandler.Resolution.X / 2f, GameHandler.GraphicsHandler.Resolution.Y / 2f);
			SecondTarget = new Vector2(GameHandler.GraphicsHandler.Resolution.X / 2f, GameHandler.GraphicsHandler.Resolution.Y / 2f);
		}
	}
}