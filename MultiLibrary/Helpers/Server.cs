﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using MultiLibrary.Handlers;

namespace MultiLibrary.Helpers {
	public class Server {
		public bool AcceptingConnections;
		public List<ClientHelper> Clients = new List<ClientHelper>();
		private TcpListener server;
		private bool stop;

		public bool Started {
			get;
			private set;
		}

		public Server(int port) {
			Start(port);
		}

		public void Start(int port) {
			if (!Started) {
				stop = false;
				GameHandler.InfoHandler.Output("[SERVER] Starting thread...");
				Thread t = new Thread(() => StartServer(port)) {
					IsBackground = true
				};
				t.Start();
				GameHandler.InfoHandler.Output("[SERVER] Thread started");
				Started = true;
			}
		}

		public void Stop() {
			GameHandler.InfoHandler.Output("[SERVER] Stopping server...");
			stop = true;
			Started = false;
		}

		private void StartServer(int port) {
			// Open port
			GameHandler.InfoHandler.Output("[SERVER] Opening port \"" + port + "\"...");
			GameHandler.NetworkHandler.OpenPort(port, port, "MultiLink");
			GameHandler.InfoHandler.Output("[SERVER] Opened port \"" + port + "\"!");

			// Start listener
			try {
				GameHandler.InfoHandler.Output("[SERVER] Starting on port \"" + port + "\"...");
				server = new TcpListener(GameHandler.NetworkHandler.GetLocalIpAddress(), port);
				server.Start();
				GameHandler.InfoHandler.Output("[SERVER] Running on port \"" + port + "\"!");
			} catch (IOException e) {
				GameHandler.InfoHandler.Output("[SERVER] Server couldn't start! Error:\r\n" + e, InfoHandler.OutputType.Error);
				return;
			}

			AcceptingConnections = true;
			string waitMessage = "[SERVER] Waiting for a connection...";

			GameHandler.InfoHandler.Output(waitMessage);
			while (!stop) {
				// Check if there are pending connections to accept
				//if (server.Pending()) {
				TcpClient client = server.AcceptTcpClient();
				Clients.Add(new ClientHelper(this, client));
				GameHandler.InfoHandler.Output("[SERVER] Client connected!");
				GameHandler.InfoHandler.Output(waitMessage);
				//}
			}

			foreach (ClientHelper client in Clients) {
				client.Stop();
			}

			server.Stop();
			GameHandler.InfoHandler.Output("[SERVER] Server stopped!");
		}
	}

	public class ClientHelper {
		public StreamReader reader;
		public Server server;
		public NetworkStream stream;
		public StreamWriter writer;
		private bool stop;
		//private string dataLeft = "";
		public ClientHelper(Server server, TcpClient client) {
			this.server = server;
			stream = client.GetStream();
			reader = new StreamReader(stream);
			writer = new StreamWriter(stream);

			GameHandler.InfoHandler.Output("[SERVER CONNECTION] Starting client thread...");
			Thread t = new Thread(ReceiveData) {
				IsBackground = true
			};
			t.Start();
			GameHandler.InfoHandler.Output("[SERVER CONNECTION] Started client thread!");
		}

		public void Stop() {
			GameHandler.InfoHandler.Output("[SERVER CONNECTION] Closing connection...");
			stop = true;
		}

		private void ReceiveData() {
			while (!stop) {
				try {
					string message = reader.ReadLine();
					GameHandler.InfoHandler.Output("[SERVER CONNECTION] Received data \"" + message + "\"!");

					// Broadcast data to other Clients
					GameHandler.InfoHandler.Output("[SERVER CONNECTION] Broadcasting data \"" + message + "\" to all clients...");
					foreach (ClientHelper currentClient in server.Clients.Where(currentClient => currentClient != this)) {
						currentClient.writer.WriteLine(message);
						currentClient.writer.Flush();
						currentClient.stream.Flush();
					}
					GameHandler.InfoHandler.Output("[SERVER CONNECTION] Broadcasted data \"" + message + "\" to all clients!");
				} catch (IOException e) {
					GameHandler.InfoHandler.Output("[SERVER CONNECTION] Error:\r\n" + e, InfoHandler.OutputType.Error);

					server.Stop();
					Stop();
				}
			}

			stream.Flush();
			writer.Flush();
			reader.Close();

			GameHandler.InfoHandler.Output("[SERVER CONNECTION] Connection closed!");
		}
	}
}