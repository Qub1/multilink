﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace MultiLibrary.Objects.TwoDimensional {
	/// <summary>
	/// A three dimensional object.
	/// </summary>
	public class GameObject2D : GameObject {
		public Vector2 BoundingBoxOffset = Vector2.Zero;
		public Vector2 BoundingBoxSizeOffset = Vector2.Zero;

		/// <summary>
		/// The center position of the GameObject2D, relative to its parent's position.
		/// </summary>
		public Vector2 Position;

		/// <summary>
		/// The size of the GameObject2D, where X = Width, Y = Height.
		/// </summary>
		public Vector2 Size;

		/// <summary>
		/// The velocity of the GameObject2D, relative to its parent's velocity.
		/// </summary>
		public Vector2 Velocity = Vector2.Zero;

		private List<GameObject2D> children = new List<GameObject2D>();

		/// <summary>
		/// The boundingbox of the object.
		/// </summary>
		public virtual Rectangle BoundingBox => new Rectangle((int)(Position.X + BoundingBoxOffset.X), (int)(Position.Y + BoundingBoxOffset.Y), (int)(Size.X + BoundingBoxSizeOffset.X), (int)(Size.Y + BoundingBoxSizeOffset.Y));

		/// <summary>
		/// A list of child GameObjects.
		/// </summary>
		public List<GameObject2D> Children => children.Where(gameObject => gameObject.Parent != null).ToList(); // Only get GameObjects with a parent

		/// <summary>
		/// A recursive list of child GameObjects.
		/// </summary>
		public List<GameObject2D> GlobalChildren {
			get {
				List<GameObject2D> result = new List<GameObject2D>();

                for (int i = 0; i < children.Count; i++) {
                    // Only get GameObjects with a parent
                    if (children[i].Parent == null) {
                        continue;
                    }

                    result.Add(children[i]);
                    result.AddRange(children[i].GlobalChildren);
                }
                /*
				foreach (GameObject2D gameObject in Children) {
					result.Add(gameObject);
					result.AddRange(gameObject.GlobalChildren);
				}*/

                return result;
			}
		}

		/// <summary>
		/// The center position of the GameObject2D in global 2D space.
		/// </summary>
		public Vector2 GlobalPosition {
			get {
				if (Parent == null) {
					return Position;
				}
				return Position + Parent.GlobalPosition;
			}
		}

		/// <summary>
		/// The velocity of the GameObject2D in global 2D space.
		/// </summary>
		public Vector2 GlobalVelocity {
			get {
				if (Parent == null) {
					return Velocity;
				}
				return Velocity + Parent.GlobalVelocity;
			}
		}

		/// <summary>
		/// The parent GameObject2D that this is a child of.
		/// </summary>
		public GameObject2D Parent {
			get;
			private set;
		}

		/// <summary>
		/// Creates a new GameObject2D.
		/// </summary>
		/// <param name="position">The position.</param>
		/// <param name="size">The size.</param>
		public GameObject2D(Vector2 position, Vector2 size) {
			Position = position;
			Size = size;
		}

		public override void Update() {
			// Move
			Position += Velocity;

            // Update child GameObjects
			foreach (GameObject2D gameObject in Children) {
				if (gameObject.Active) {
					gameObject.Update();
				}
			}

			Cleanup();
		}

		public override void Draw() {

            foreach (GameObject2D gameObject in children) {
				if (gameObject.Visible) {
					gameObject.Draw();
				}
			}

			Cleanup();
		}

		/// <summary>
		/// Resets the object to its default state.
		/// </summary>
		public override void Reset() {
			foreach (GameObject2D gameObject in Children) {
				gameObject.Reset();
			}
		}

		/// <summary>
		/// Adds a child GameObject.
		/// </summary>
		/// <param name="gameObject">The GameObject2D to add.</param>
		public void AddChild(GameObject2D gameObject) {
            if (gameObject == null) {
                return;
            }
			children.Add(gameObject);
			gameObject.Parent = this;
		}

		public void ClearChildren() {
			children = new List<GameObject2D>();
		}

		/// <summary>
		/// Removes a child GameObject.
		/// </summary>
		/// <param name="gameObject">The GameObject2D to remove.</param>
		public void RemoveChild(GameObject2D gameObject) {
			gameObject.Parent = null;
			children.Remove(gameObject);
		}

		/// <summary>
		/// Removes orphaned GameObjects.
		/// This needs to be done at the end of an update, as to avoid changing the size of the list whilst manipulating it.
		/// </summary>
		private void Cleanup() {
			List<GameObject2D> clean = new List<GameObject2D>(Children);

			while (clean.Count > 0) {
				if (clean[0].Parent == null) {
					children.Remove(clean[0]);
				}
				clean.Remove(clean[0]);
			}
		}
	}
}