﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MultiLibrary.Handlers;

namespace MultiLibrary.Objects {
	/// <summary>
	/// Used for displaying text on screen.
	/// </summary>
	public class TextGameObject : TextureGameObject {
		/// <summary>
		/// The color of the text.
		/// </summary>
		public Color Color = Color.White;

		private SpriteFont font;
		private string text;

		/// <summary>
		/// The font.
		/// </summary>
		public SpriteFont Font {
			get {
				return font;
			}
			set {
				font = value;

				Size = TextSize;
			}
		}

		/// <summary>
		/// The string text.
		/// </summary>
		public string Text {
			get {
				return text;
			}
			set {
				text = value;

				Size = TextSize;
			}
		}

		/// <summary>
		/// The actual text size.
		/// </summary>
		public Vector2 TextSize => Font.MeasureString(Text);

		/// <summary>
		/// Creates a new TextEntity.
		/// </summary>
		public TextGameObject(Vector2 position, SpriteFont font, string text = "") : base(position, Vector2.Zero) {
			this.font = font;
			Text = text;
			Gui = true;
			Layer = 0f;
		}

		public override void Draw() {
			GameHandler.GraphicsHandler.DrawText(GlobalPosition, 0F, Size, Text, Font, Color, SpriteEffects.None, Layer, Gui);
			base.Draw();
		}
	}
}