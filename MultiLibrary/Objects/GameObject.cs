﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Microsoft.Xna.Framework;
using MultiLibrary.Handlers;

namespace MultiLibrary.Objects {
	/// <summary>
	/// A three dimensional object.
	/// </summary>
	public class GameObject {
		/// <summary>
		/// Whether the GameObject is updated.
		/// </summary>
		public bool Active {
			get {
				return active;
			}
			set {
				if (Active != value) {
					active = value;

					NotifyPropertyChanged("Active");
				}
			}
		}

		private bool active = true;

		public Vector2 BoundingBoxOffset = Vector2.Zero;
		public Vector2 BoundingBoxSizeOffset = Vector2.Zero;
		public bool Gui = false;
		public int Id = -1;
		public Vector2 position;
		public PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// The size of the GameObject2D, where X = Width, Y = Height.
		/// </summary>
		public Vector2 Size;

		/// <summary>
		/// Whether the GameObject is drawn.
		/// </summary>
		public bool Visible = true;

		private List<GameObject> children = new List<GameObject>();
		private Vector2 velocity = Vector2.Zero;

		/// <summary>
		/// The boundingbox of the object.
		/// </summary>
		public virtual Rectangle BoundingBox => new Rectangle((int)(Position.X + BoundingBoxOffset.X), (int)(Position.Y + BoundingBoxOffset.Y), (int)(Size.X + BoundingBoxSizeOffset.X), (int)(Size.Y + BoundingBoxSizeOffset.Y));

		/// <summary>
		/// A list of child GameObjects.
		/// </summary>
		public List<GameObject> Children => children.Where(gameObject => gameObject.Parent != null).ToList(); // Only get GameObjects with a parent

		/// <summary>
		/// A recursive list of child GameObjects.
		/// </summary>
		public List<GameObject> GlobalChildren {
			get {
				List<GameObject> result = new List<GameObject>();

				foreach (GameObject gameObject in Children) {
					result.Add(gameObject);
					result.AddRange(gameObject.GlobalChildren);
				}

				return result;
			}
		}

		/// <summary>
		/// The center position of the GameObject2D in global 2D space.
		/// </summary>
		public Vector2 GlobalPosition {
			get {
				if (Parent == null) {
					return Position;
				}
				return Position + Parent.GlobalPosition;
			}
		}

		/// <summary>
		/// The velocity of the GameObject2D in global 2D space.
		/// </summary>
		public Vector2 GlobalVelocity {
			get {
				if (Parent == null) {
					return Velocity;
				}
				return Velocity + Parent.GlobalVelocity;
			}
		}

		/// <summary>
		/// The parent GameObject2D that this is a child of.
		/// </summary>
		public GameObject Parent {
			get;
			private set;
		}

		public Vector2 Scale = Vector2.One;

		public Vector2 GlobalScale {
			get {
				if (Parent == null) {
					return Scale;
				} else {
					return Scale * Parent.GlobalScale;
				}
			}
		}

		/// <summary>
		/// The center position of the GameObject2D, relative to its parent's position.
		/// </summary>
		public Vector2 Position {
			get {
				return position;
			}
			set {
				if (Position != value) {
					position = value;

					NotifyPropertyChanged("Position");
				}
			}
		}

		/// <summary>
		/// The velocity of the GameObject2D, relative to its parent's velocity.
		/// </summary>
		public Vector2 Velocity {
			get {
				return velocity;
			}
			set {
				if (Velocity != value) {
					velocity = value;

					if (Velocity.Length() > HighestVelocity.Length()) {
						HighestVelocity = Velocity;
					}

					NotifyPropertyChanged("Velocity");
				}
			}
		}

		public Vector2 HighestVelocity {
			get;
			private set;
		} = Vector2.Zero;

		/// <summary>
		/// Creates a new GameObject2D.
		/// </summary>
		/// <param name="position">The position.</param>
		/// <param name="size">The size.</param>
		public GameObject(Vector2? position = null, Vector2? size = null) {
			Position = position ?? Vector2.Zero;
			Size = size ?? Vector2.Zero;
		}

		/// <summary>
		/// Adds a child GameObject.
		/// </summary>
		/// <param name="gameObject">The GameObject2D to add.</param>
		public virtual void AddChild(GameObject gameObject) {
			if (gameObject == null) {
				return;
			}
			children.Add(gameObject);
			gameObject.Parent = this;
			GameHandler.InfoHandler.AddGameObject(gameObject);
		}

		public void ClearChildren() {
			children = new List<GameObject>();
		}

		public virtual void Draw() {
			foreach (GameObject gameObject in children.Where(gameObject => gameObject.Visible)) {
				gameObject.Draw();
			}

			Cleanup();
		}

		/// <summary>
		/// Removes a child GameObject.
		/// </summary>
		/// <param name="gameObject">The GameObject2D to remove.</param>
		public virtual void RemoveChild(GameObject gameObject) {
			if (gameObject == null) {
				return;
			}
			gameObject.Parent = null;
			children.Remove(gameObject);
			GameHandler.InfoHandler.RemoveGameObject(gameObject);
		}

		/// <summary>
		/// Resets the object to its default state.
		/// </summary>
		public virtual void Reset() {
			foreach (GameObject gameObject in Children) {
				gameObject.Reset();
			}
		}

		public virtual void Update() {
			// Move
			Position += Velocity;

			// Update child GameObjects
			foreach (GameObject gameObject in Children.Where(gameObject => gameObject.Active)) {
				gameObject.Update();
			}

			Cleanup();
		}

		/// <summary>
		/// Removes orphaned GameObjects.
		/// This needs to be done at the end of an update, as to avoid changing the size of the list whilst manipulating it.
		/// </summary>
		private void Cleanup() {
			List<GameObject> clean = new List<GameObject>(Children);

			while (clean.Count > 0) {
				if (clean[0].Parent == null) {
					children.Remove(clean[0]);
				}
				clean.Remove(clean[0]);
			}
		}

		private void NotifyPropertyChanged(string info) {
			if (PropertyChanged != null) {
				PropertyChanged(this, new PropertyChangedEventArgs(info));
			}
		}
	}
}