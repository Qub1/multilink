﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MultiLibrary.Handlers;
using MultiLibrary.Types;

namespace MultiLibrary.Objects {
	/// <summary>
	/// A GameObject with a texture.
	/// </summary>
	public class TextureGameObject : GameObject {
		/// <summary>
		/// Transparancy of the sprite
		/// </summary>
		public float Alpha = 1f;

		/// <summary>
		/// Layer to draw the sprite on
		/// </summary>
		public float Layer = 0f;

		public Vector2 RenderOffset = Vector2.Zero;
		public Vector2 RenderSizeOffset = Vector2.Zero;

		/// <summary>
		/// Rotation of the sprite
		/// </summary>
		public float Rotation = 0f;

		/// <summary>
		/// The Sprite of the GameObject.
		/// </summary>
		public Sprite Sprite = new Sprite();

		/// <summary>
		/// Creates a new TextureGameObject2D.
		/// </summary>
		/// <param name="position">The position.</param>
		/// <param name="size">The size.</param>
		public TextureGameObject(Vector2 position, Vector2 size) : base(position, size) {
		}

		public override void Draw() {
			GameHandler.GraphicsHandler.DrawTexture((GlobalPosition + RenderOffset) * GlobalScale, Rotation, (Size + RenderSizeOffset) * GlobalScale, Sprite.Texture, Sprite.Source, Color.White * Alpha, SpriteEffects.None, Layer, Gui);

			base.Draw();
		}

		public override void Update() {
			// Animate sprite
			Sprite.Update();

			base.Update();
		}

		public void StaticRenderDraw() {
			GameHandler.GraphicsHandler.DrawTexture(Position + RenderOffset, Rotation, Size + RenderSizeOffset, Sprite.Texture, Sprite.Source, Color.White);
		}
	}
}