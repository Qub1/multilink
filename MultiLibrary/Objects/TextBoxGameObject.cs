﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MultiLibrary.Handlers;

namespace MultiLibrary.Objects {
	public sealed class TextBoxGameObject : TextureGameObject {
		public bool AcceptsDigits;
		public bool AcceptsLetters;
		public bool HasFocus;
		public int MaxLength = 18;
		public TextGameObject Text;
		private bool startTimer;
		private TimeSpan timer;

		/// <summary>
		/// Creates a new TextBox.
		/// </summary>
		/// <param name="position">The position.</param>
		/// <param name="size">The size of the TextBox.</param>
		/// <param name="font">The SpriteFont to use for the text contents.</param>
		/// <param name="text">The text to place in the box.</param>
		/// <param name="acceptsDigits">Whether this InputBox accepts digits as input.</param>
		/// <param name="acceptsLetters">Whether this InputBox accepts letters as input.</param>
		public TextBoxGameObject(Vector2 position, Vector2 size, SpriteFont font, string text = "", bool acceptsDigits = true, bool acceptsLetters = true) : base(position, size) {
			AcceptsDigits = acceptsDigits;
			AcceptsLetters = acceptsLetters;

			Gui = true;

			Text = new TextGameObject(new Vector2(10F, 2F), font, text) {
				Layer = 1f,
				Gui = true
			};
			AddChild(Text);
		}

		/// <summary>
		/// Clears the text and reverts the box to its default state.
		/// </summary>
		public override void Reset() {
			HasFocus = false;

			base.Reset();
		}

		public override void Update() {
			// Don't process keypresses if this InputBox doesn't have focus
			foreach (Keys key in GameHandler.InputHandler.KeysDown) {
				if (HasFocus) {
					if (GameHandler.InputHandler.OnKeyDown(key) && Text.Text.Length < MaxLength) {
						bool digit = AcceptsDigits && key >= Keys.D0 && key <= Keys.D9;
						bool letter = AcceptsLetters && ((key >= Keys.A && key <= Keys.Z) || key == Keys.Space);

						if (digit || letter) {
							// Key is allowed, add to text
							Text.Text += (char)key;
						} else if (key == Keys.OemPeriod) {
							Text.Text += ".";
						}
					}
					if (key == Keys.Back && Text.Text.Length > 0) {
						if (GameHandler.InputHandler.OnKeyDown(key)) {
							// Start timer
							Text.Text = Text.Text.Substring(0, Text.Text.Length - 1);
							timer = new TimeSpan();
							startTimer = true;
						} else if (GameHandler.InputHandler.KeyDown(key) && startTimer) {
							timer += GameHandler.InfoHandler.GameTime.ElapsedGameTime;

							if (timer.Milliseconds >= 500) {
								// Key is backspace; remove last input
								Text.Text = Text.Text.Substring(0, Text.Text.Length - 1);
							}
						}
					}
				}

				if (GameHandler.InputHandler.OnKeyUp(key) && key == Keys.Back) {
					// Reset timer
					timer = new TimeSpan();
					startTimer = false;
				}
			}

			Text.Size = Text.TextSize.X > Size.X - 20F ? new Vector2(Size.X - 20F, Text.TextSize.Y) : Text.TextSize;
			Text.Position = (Size - Text.Size) / 2;

			base.Update();
		}
	}
}